<%--
  Created by IntelliJ IDEA.
  User: Shovon
  Date: 10/25/12
  Time: 7:24 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title><decorator:title/></title>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Admin</title>
    <link rel="stylesheet" type="text/css" href="<c:url value="/Styles/admin_css/theme.css" />" media="all"/>
    <link rel="stylesheet" type="text/css" href="<c:url value="/Styles/admin_css/style.css" />" media="all"/>
    <script>
        var StyleFile = "theme" + document.cookie.charAt(6) + ".css";
        document.writeln('<link rel="stylesheet" type="text/css" href="<c:url value="/Styles/admin_css/" />' + StyleFile + '">');
    </script>
    <!--[if IE]>
    <link rel="stylesheet" type="text/css" href="<c:url value="/Styles/admin_css/ie-sucks.css" />" />
    <![endif]-->
    <decorator:head/>
</head>

<body>
<div id="container">
    <div id="header">
        <h2>Admin Panel</h2>


    </div>

    <div id="wrapper">
        <div id="content">

            <decorator:body/>

        </div>
        <jsp:include page="/AdminView/SideBar.jsp"/>

    </div>
    <div id="footer">
        <div id="credits">
            &copy; hutbuzar.com <a href="#"></a>
        </div>
        <div id="styleswitcher">
            <ul>
                <li><a href="javascript: document.cookie='theme=4'; window.location.reload();" title="Default"
                       id="defswitch">d</a></li>
                <li><a href="javascript: document.cookie='theme=1'; window.location.reload();" title="Blue"
                       id="blueswitch">b</a></li>
                <li><a href="javascript: document.cookie='theme=2'; window.location.reload();" title="Green"
                       id="greenswitch">g</a></li>
                <li><a href="javascript: document.cookie='theme=3'; window.location.reload();" title="Brown"
                       id="brownswitch">b</a></li>
                <li><a href="javascript: document.cookie='theme='; window.location.reload();" title="Mix"
                       id="mixswitch">m</a></li>
            </ul>
        </div>
        <br/>

    </div>
</div>
</body>
</html>

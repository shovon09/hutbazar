<%--
  Created by IntelliJ IDEA.
  User: Shovon
  Date: 10/22/12
  Time: 8:56 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Welcome To Hutbazar</title>
    <link rel="shortcut icon"
          href="./Images/favicon.ico"/>
</head>
<body>

<div id="content">

    <!-- Content Slider (primary add) -->
    <%--<%@include file="/CustomerView/PrimaryAdView.jsp" %>--%>
    <jsp:include page="/CustomerView/PrimaryAdView.jsp"/>

    <!-- End Content Slider -->

    <!-- Secondary Add -->
    <%--<%@include file="/CustomerView/SecondaryAdView.jsp" %>--%>
    <jsp:include page="/CustomerView/SecondaryAdView.jsp"/>
    <!-- End  Secondary Add -->

</div>

</body>
</html>
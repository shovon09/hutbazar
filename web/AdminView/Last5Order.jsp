<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="hutbazar.model.admin.*" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="hutbazar.model.admin.dao.*" %>
<%@ page import="hutbazar.model.admin.domain.*" %>
<%--
  Created by IntelliJ IDEA.
  UserEntity: Shovon
  Date: 10/19/12
  Time: 11:52 PM
  To change this template use File | Settings | File Templates.
--%>

<%
    HttpSession usersession = request.getSession(true);
    List<OrderListEntity> orderList = LastFiveOrderService.gerLastFiveOrders ((String) usersession.getAttribute("userStoreId"));
    pageContext.setAttribute("orderList", orderList);
%>

<div id="infobox">
    <h3>Last 5 Orders</h3>
    <table>
        <thead>
        <tr>
            <td>Order_number</td>
            <td>Total Product Bought</td>
            <td>Order Date</td>
            <td>Status</td>
            <td>Details</td>

        </tr>
        </thead>
        <tbody>

        <c:forEach var="orderList" items="${orderList}">
            <tr class="cart_title">

                <td>${orderList.order_number}</td>
                <td>${orderList.totalNumberOfProducts}</td>
                <td>${orderList.order_date}</td>
                <td>${orderList.order_status}</td>
                <form name="orderDetails" action="<c:url value="/admin/orderDetails"/>" method="post">
                    <input type="hidden" name="orderIDToGetDetails" value="${orderList.order_id}">
                    <td><input type="submit"  value="Details"/> </td>


                </form>
            </tr>

        </c:forEach>



        </tbody>
    </table>

</div>

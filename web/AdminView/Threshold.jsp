<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="hutbazar.model.admin.dao.ThresholdService" %>
<%@ page import="hutbazar.model.admin.domain.ThresholdEntity" %>
<%@ page import="java.util.List" %>
<%--
  Created by IntelliJ IDEA.
  UserEntity: Shovon
  Date: 10/19/12
  Time: 11:52 PM
  To change this template use File | Settings | File Templates.
--%>

<%
    HttpSession usersession = request.getSession(true);
    List<ThresholdEntity> threshold = ThresholdService.getProducts((String) usersession.getAttribute("userStoreId"));
    pageContext.setAttribute("threshold", threshold);
%>

<div id="infobox">
    <h3>Products that are close or under its Threshold Level</h3>
    <table>
        <thead>
        <tr>
            <td>Product Name</td>
            <td>Quantity Available in stock</td>
            <td>Threshold</td>
            <td>Add more</td>


        </tr>
        </thead>
        <tbody>

        <c:forEach var="threshold" items="${threshold}">
            <tr class="cart_title">

                <td>${threshold.productName}</td>
                <td>${threshold.stock}</td>
                <td>${threshold.threshold}</td>
                <td><a href="<c:url value="/admin/addExistingProductForm?productId=${threshold.productId}"/>">Add more</a></td>

                    <%--  <td>${orderList.order_status}</td>
                    <form name="orderDetails" action="<c:url value="/admin/orderDetails"/>" method="post">
                        <input type="hidden" name="orderIDToGetDetails" value="${orderList.order_id}">
                        <td><input type="submit"  value="Details"/> </td>


                    </form>--%>
            </tr>

        </c:forEach>


        </tbody>
    </table>

</div>

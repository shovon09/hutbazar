<%--
  Created by IntelliJ IDEA.
  User: Shovon
  Date: 11/8/12
  Time: 2:26 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title></title>
</head>
<body>
<div id="sidebar">
    <ul>
        <%--<li><h3><a href="#" class="house">Dashboard</a></h3>--%>
        <%--<ul>--%>
        <%--<li><a href="#" class="report">Sales Report</a></li>--%>
        <%--<li><a href="#" class="search">Search Product</a></li>--%>
        <%--</ul>--%>
        <%--</li>--%>
        <%--<li><h3><a href="#" class="folder_table">Orders</a></h3>--%>
        <%--<ul>--%>
        <%--<li><a href="#" class="addorder">New order</a></li>--%>
        <%--<li><a href="#" class="shipping">Shipments</a></li>--%>
        <%--<li><a href="#" class="invoices">Invoices</a></li>--%>
        <%--</ul>--%>
        <%--</li>--%>

        <li><h3><a href="<c:url value="/admin/dashboard"/>" class="house">Dashboard</a></h3>
            <ul>
                <li><a href="<c:url value="/admin/salesReport"/>" class="report">Overall Sales Report</a></li>
                <li><a href="<c:url value="/admin/productWiseSalesReport"/>" class="report">Product Wise Sales Report</a></li>
                <li><a href="<c:url value="/admin/monthWiseSalesReport"/>" class="report">Month Wise Sales Report</a></li>
                <li><a href="<c:url value="/admin/customerWiseSalesReport"/>" class="report">Customer Wise Sales Report</a></li>


            </ul>
        </li>
        <li><h3><a href="<c:url value="/admin/showOrders"/>" class="folder_table">Orders</a></h3>
            <ul>
                <li><a href="<c:url value="/admin/newOrders"/>" class="addorder">New order</a></li>
                <%-- <li><a href="<c:url value="/admin/newOrders"/> " class="shipping">Shipments</a></li>--%>

            </ul>
        </li>
        <li><h3><a href="<c:url value="/admin/manageProduct?storeId=${sessionScope.storeId}"/>"
                   class="manage">Manage</a></h3>
            <ul>
                <li><a href="<c:url value="/admin/manageProduct?storeId=${sessionScope.storeId}"/>" class="cart">Products</a>
                </li>
                <li><a href="<c:url value="/admin/showCompareForm?storeId=${sessionScope.storeId}"/>" class="folder">Compare
                    Product</a></li>
                <li><a href="<c:url value="/admin/manageCategory?storeId=${sessionScope.storeId}"/>" class="folder">Product
                    categories</a></li>

            </ul>
        </li>
        <li><h3><a href="#" class="userEntity">Customer Panel</a></h3>
            <ul>
                <li><a href="<c:url value="/admin/customerWishList"/>" class="useradd">Customer Wish List</a></li>
                <li><a href="<c:url value="/admin/customerSupportList"/>" class="group">Customer Support</a></li>
                <%--<li><a href="<c:url value="/admin/customerFeedBackList"/>" class="group">Customer Feedback</a></li>--%>

            </ul>
        </li>
        <li><h3><a href="<c:url value="/admin/logout"/>" class="userEntity">Sign Out</a></h3>

        </li>
    </ul>
</div>
</body>
</html>
<%--
  Created by IntelliJ IDEA.
  User: shihab
  Date: 11/12/12
  Time: 11:16 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Details</title>
</head>
<body>
<%--
  Created by IntelliJ IDEA.
  User: shihab
  Date: 11/12/12
  Time: 9:02 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Customer Wise Sales Report</title>
</head>
<body>

<h3>Products Bought</h3>
<table>
    <thead>

    <tr class="cart_title">
        <td>Product Name</td>
        <td>Quantity</td>
    </tr>



    </thead>
    <tbody>

    <c:forEach var="productGraph" items="${productGraph}">

        <tr class="cart_title">
            <td>${productGraph.productName}</td>
            <td>${productGraph.quantity}</td>
        </tr>



    </c:forEach>




    </tbody>



</table>

<img src="<c:url value="/admin/customerProductsGraph?customerId=${customerId}"/>">
<img src="<c:url value="/admin/customerProductsProfitGraph?customerId=${customerId}"/>">
<img src="<c:url value="/admin/customerProductsSalesGraph?customerId=${customerId}"/>">

</body>
</html>

</body>
</html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="hutbazar.model.admin.dao.RightNowServices" %>
<%@ page import="hutbazar.model.admin.domain.RightNowEntity" %>
<%@ page import="java.util.List" %>
<%--
  Created by IntelliJ IDEA.
  UserEntity: Shovon
  Date: 10/19/12
  Time: 11:52 PM
  To change this template use File | Settings | File Templates.
--%>

<%
    HttpSession usersession = request.getSession(true);
    List<RightNowEntity> rightNow = RightNowServices.getRightNow((String) usersession.getAttribute("userStoreId"));
    pageContext.setAttribute("rightNow", rightNow);
%>

<div id="rightnow">
    <h3 class="reallynow">
        <span>Right Now</span>

        <br/>
    </h3>
    <c:forEach var="rightNow" items="${rightNow}">
        <p class="youhave">You have ${rightNow.totalOrders} new Orders, ${rightNow.totalUsers} new users and , today you
            made ${rightNow.totalSales} taka and a total of <strong>${rightNow.totalProfit} taka Profit</strong>
        </p>
    </c:forEach>
</div>

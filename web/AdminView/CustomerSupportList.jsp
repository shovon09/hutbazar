<%--
  Created by IntelliJ IDEA.
  User: Shovon
  Date: 11/13/12
  Time: 10:27 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title></title>
</head>
<body>
<div id="rightnow">
    <h3 class="reallynow">
        <span>Customer Support List</span>
        <br/>
    </h3>

    <p class="youhave">You have total ${supportList.size()} Customer Support Request </p>
</div>

<h3>Customer Support Request </h3>
<table>
    <thead>
    <tr>
        <th>Category ID</th>
        <th>Product ID</th>
        <th>Comment</th>
        <th>email</th>
        <th>Send Feedback</th>

    </tr>
    </thead>
    <tbody>
    <c:forEach var="supportList" items="${supportList}">
        <tr>
            <td>${supportList.categoryId}</td>
            <td>${supportList.productId}</td>
            <td>${supportList.comment}</td>
            <td>${supportList.email}</td>
            <td><a href="<c:url value="/admin/mailForm?email=${supportList.email}"/>">Send Mail</a></td>


        </tr>
    </c:forEach>


    </tbody>
</table>


</body>
</html>
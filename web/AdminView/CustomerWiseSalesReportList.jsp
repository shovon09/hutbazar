<%--
  Created by IntelliJ IDEA.
  User: shihab
  Date: 11/12/12
  Time: 9:02 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Customer Wise Sales Report</title>
</head>
<body>

<h3>Customers</h3>
<table>
    <thead>
    <tr>
        <td>Customer Name</td>
        <td>Total Quantity Bought by this customer</td>
        <td>Total Distinct Products bought by this customer</td>
        <td>Total Investment in this customer</td>
        <td>Total Sales by this customer</td>
        <td>Total Profit from this customer</td>


    </tr>
    </thead>
    <tbody>



    <c:forEach var="customers" items="${customers}">
        <tr class="cart_title">

            <td> <a href= "<c:url value="/admin/customerWiseSalesReportDetails?customerId=${customers.customerId}&quantity=${customers.quantity}&totalProducts=${customers.totalProducts}&totalInvestment=${customers.totalInvestment}&totalSales=${customers.totalSales}&totalProfit=${customers.totalProfit}" />"> ${customers.customerName} </a>  </td>
            <td>${customers.quantity}</td>
            <td>${customers.totalProducts}</td>
            <td>${customers.totalInvestment} Taka</td>
            <td>${customers.totalSales} Taka</td>
            <td>${customers.totalProfit} Taka</td>




        </tr>

    </c:forEach>

    </tbody>
</table>








</body>
</html>
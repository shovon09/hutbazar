<%--
  Created by IntelliJ IDEA.
  User: shihab
  Date: 11/9/12
  Time: 6:44 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Order Details</title>
    <link rel="shortcut icon"
          href="./Images/favicon.ico" />
</head>
<body>

<h3>Order Details</h3>
<div id="infowrap">

    <div id="infobox">

        <table >
            <thead>
            <tr>
                <td>Product Name</td>
                <td>Store Name</td>
                <td>Quantity Bought</td>
                <td>Unit Price</td>
                <td>Total</td>
            </tr>
            </thead>
            <tbody>

            <c:forEach var="orderDetails" items="${orderDetails}">
                <tr class="cart_title">
                    <td>${orderDetails.productName}</td>
                    <td>${orderDetails.storeName}</td>
                    <td>${orderDetails.productQuantity}</td>
                    <td>${orderDetails.productUnitPrice}</td>
                    <td>${orderDetails.sales}</td>

            </c:forEach>

            </tbody>


        </table>

         <h3>Other Information</h3>

        <table>



            <tr >
                <td>Total Amount </td>
                <td>${totalSales} Taka</td>
            </tr>


            <tr class="cart_title">
                <td>Shipment Charge </td>
                <td>${shipmentCharge} Taka</td>
            </tr>


            <tr class="cart_title">
                <td>Total Paid Amount [with Shipment Charge] </td>
                <td>${totalAfterShipment} Taka</td>
            </tr>





            <tr class="cart_title">
                <td>Order Date</td>
                <td>${orderDate}</td>
            </tr>


            <tr class="cart_title">
                <td>Delivary  Date</td>
                <td>${delivaryDate}</td>
            </tr>


            <tr class="cart_title">
                <td>Deliary Address </td>
                <td>${delivaryAddress}</td>
            </tr>


            <tr class="cart_title">
                <td>Order Status </td>
                <td>${orderStatus}</td>
            </tr>


            <tr class="cart_title">
                <td>Total Investment In this Order </td>
                <td>${totalInvestment} Taka</td>
            </tr>


            <tr class="cart_title">
                <td>Total Profit in this Order</td>
                <td>${totalProfit} Taka</td>
            </tr>





        </table>


    </div>
    <img src="<c:url value="/admin/orderProductsGraph?orderIdToGetDetails=${sessionScope.orderId}"/>"/>
</div>


</body>
</html>
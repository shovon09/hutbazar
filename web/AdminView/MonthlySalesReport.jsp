<%--
  Created by IntelliJ IDEA.
  User: shihab
  Date: 11/12/12
  Time: 9:02 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Month Wise Sales Report</title>
</head>
<body>

<h3>Month Wise Sales Report</h3>
<table>
    <thead>
    <tr>
        <td>Month</td>
        <td>Total Investment In this Month</td>
        <td>Total Sales in this Month</td>
        <td>Total Profit in this Month</td>
        <td>Total Quantity Sold</td>
        <td>Number of Products Sold</td>


    </tr>
    </thead>
    <tbody>


    <c:forEach var="monthlySalesReport" items="${monthlySalesReport}">
        <tr class="cart_title">


            <td>${monthlySalesReport.month}</td>
            <td>${monthlySalesReport.totalInvestment} Taka</td>
            <td>${monthlySalesReport.totalSales} Taka</td>
            <td>${monthlySalesReport.totalProfit} Taka</td>
            <td>${monthlySalesReport.quantity}</td>
            <td>${monthlySalesReport.products}</td>


        </tr>

    </c:forEach>

    </tbody>
</table>

<img src="<c:url value="/admin/monthVsProfitChart"/>" />
</body>
</html>
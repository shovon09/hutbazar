<%--
  Created by IntelliJ IDEA.
  User: Shovon
  Date: 11/10/12
  Time: 12:39 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%--
  Created by IntelliJ IDEA.
  User: Shovon
  Date: 11/9/12
  Time: 12:02 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
</head>
<body>
<div id="rightnow">
    <h3 class="reallynow">
        <span>Manage Products</span>
        <a href="<c:url value="/admin/addCategoryForm"/>" class="add">Add New Category</a>
        <br/>
    </h3>

    <p class="youhave">You have ${totalCategory} Category</p>
</div>

<h3>Available Categories</h3>
<table>
    <thead>
    <tr>
        <th>Category Id</th>
        <th>Category Name</th>


    </tr>
    </thead>
    <tbody>
    <c:forEach var="category" items="${category}">
        <tr>
            <td> ${category.categoryId}</td>
            <td>${category.categoryName}</td>

        </tr>
    </c:forEach>


    </tbody>
</table>


</body>
</html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="hutbazar.model.customer.dao.CustomerService" %>
<%@ page import="hutbazar.model.customer.domain.StoreEntity" %>
<%@ page import="java.util.List" %>
<%--
  Created by IntelliJ IDEA.
  UserEntity: Shovon
  Date: 10/19/12
  Time: 11:52 PM
  To change this template use File | Settings | File Templates.
--%>

<%
    List<StoreEntity> storeEntities = CustomerService.getStores();
    pageContext.setAttribute("sotres", storeEntities);
%>

<div class="box categories">
    <h2>Availabe Stores<span></span></h2>

    <div class="box-content">

        <ul>
            <c:forEach var="store" items="${sotres}">

                <li><a href="storeCatagory?store_id=${store.store_id}">${store.store_name}</a></li>

            </c:forEach>
        </ul>
    </div>
</div>
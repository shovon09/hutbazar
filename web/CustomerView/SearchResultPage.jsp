<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  UserEntity: Shovon
  Date: 11/7/12
  Time: 6:22 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
</head>
<body>

<c:choose>
    <c:when test="${!searchResult.isEmpty()}">
        <table id="ver-minimalist" summary="Most Favorite Movies">
            <thead>
            <tr>
                <th scope="col">Product Id</th>
                <th scope="col">Product Name</th>
                <th scope="col">Category Id</th>
                <th scope="col">Selling Price</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach var="searchResult" items="${searchResult}">
                <tr>
                    <td>
                        <a href="productDetails?product_id=${searchResult.productId}&category_id=${searchResult.categoryId}">${searchResult.productId}</a>
                    </td>
                    <td>${searchResult.productName}</td>
                    <td>
                        <a href="listProduct?catId=${searchResult.categoryId}&store_id=${searchResult. storeId}">${searchResult.categoryId}</a>
                    </td>
                    <td>${searchResult.sellingPrice}</td>


                </tr>
            </c:forEach>
            </tbody>
        </table>
    </c:when>
    <c:otherwise>
        <div id="content">
            Sorry No match Found.

        </div>

    </c:otherwise>
</c:choose>


</body>
</html>
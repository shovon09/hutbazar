<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  UserEntity: Shovon
  Date: 10/23/12
  Time: 1:29 PM
  To change this template use File | Settings | File Templates.
--%>
<%--<%@ page import="hutbazar.model.customer>--%>
<%--<%@ page import="java.util.List" %>--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--<%  List<ProductCategoryEntity> productCategoryEntities = (List<ProductCategoryEntity>) request.getAttribute("categories");%>--%>
<html>
<head>
    <title>Category Details</title>
</head>
<body>

<div class="left_content">
    <div class="crumb_nav">
        <a href="storeCatagory?store_id=${storeId}">Category</a> &gt;&gt; products
    </div>
    <div class="title"><span class="title_icon"></span>Products
    </div>

    <div class="new_products">


        <%--<jsp:useBean id="categories" scope="request" type="java.util.List"/>--%>
        <c:forEach var="product" items="${products}">

            <div class="new_prod_box">
                <a href="productDetails?product_id=${product.productId}&category_id=${product.categoryId}">${product.productName}</a>

                <div class="new_prod_bg">
                    <span class="new_icon"><img src="" alt="" title=""/></span>
                    <a href="productDetails?product_id=${product.productId}&category_id=${product.categoryId}"><img
                            src="${product.image}" alt="" title="" class="thumb" border="0" height="100"
                            width="120"/></a>
                </div>
            </div>

        </c:forEach>


    </div>


    <div class="clear"></div>
</div>
<!--end of left content-->

</body>
</html>
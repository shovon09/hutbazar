<%--
  Created by IntelliJ IDEA.
  User: Shovon
  Date: 10/26/12
  Time: 12:03 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
</head>
<body>
<div class="left_content">

    <div class="crumb_nav">
        <a href="listProduct?catId=${categoryId}&store_id=${product.storeId}">All products</a> &gt;&gt; product details
    </div>
    <div class="title"><span class="title_icon"></span>${product.productName}</div>

    <div class="feat_prod_box_details">

        <div class="prod_img"><a href="#"><img src="${product.image}" alt="" title="" border="0" height="150"
                                               width="150"/></a>
            <br/><br/>
            <a href="#" rel="lightbox"><img src="Styles/css/images/zoom.gif" alt="" title="zoom"
                                            border="0"/></a>
        </div>

        <div class="prod_det_box">
            <div class="box_top"></div>
            <div class="box_center">
                <div class="prod_title">Details</div>
                <p class="details">
                    ${product.description}
                </p>

                <div class="price"><strong>PRICE:</strong> <span class="red">${inventory.sellingPrice} BDT</span></div>
                <div class="price"><strong>Stock available : ${inventory.stock} </strong></div>
                <div class="price">

                    <strong>

                        <form method="post" action="addToCart">

                            <input type="hidden" name="categoryId" value="${categoryId}"/>
                            <input type="hidden" name="storeId" value="${product.storeId}"/>
                            <input type="hidden" name="productId" value="${product.productId}"/>
                            <input type="hidden" name="productName" value="${product.productName}"/>
                            <input type="hidden" name="image" value="${product.image}"/>
                            <input type="hidden" size="3" name="quantity" value="1"/>
                            <input type="hidden" name="unitPrice" value="${inventory.sellingPrice}"/>
                            <input type="hidden" name="avgBoughtPrice" value="${inventory.avgBoughtPrice}"/>
                            <input type="hidden" name="promotion" value="${product.discount}"/>
                            <input type="hidden" name="promotionFrom" value="${product.promotionFrom}"/>
                            <input type="hidden" name="promotionEnd" value="${product.promotionEnd}"/>
                            <input type="hidden" name="promotionActive" value="${product.promotionActive}"/>
                            <input type="hidden" name="userId" value="${sessionScope.userId}"/>

                            <input type="image" src="Styles/css/images/addtocart.png">
                        </form>
                    </strong>
                    <%--<><a href="#" class="more"><img src="Styles/css/images/addtocart.png" alt="" title="" border="0"/></a>--%>

                </div>
                <div class="clear"></div>

                <div class="box_bottom"></div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
    <div id="demo" class="demolayout">

        <ul id="demo-nav" class="demolayout">
            <li><a class="active" href="#tab1">More details</a></li>

        </ul>

        <div class="tabs-container">

            <div style="display: block;" class="tab" id="tab1">
                <p class="more_details">${product.description} </p>
                <ul class="list">

                    <li>Product ID : ${product.productId}</li>
                    <li>Product Name : ${product.productName}</li>
                    <li>Selling price : ${inventory.sellingPrice} BDT</li>
                    <li>Date Added : ${product.dateAdded}</li>
                    <li>Promotion : ${product.discount} %</li>
                    <li>Promotion From : ${product.promotionFrom}</li>
                    <li>Promotion End : ${product.promotionEnd}</li>


                </ul>

            </div>


        </div>


    </div>
</div>
</body>
</html>
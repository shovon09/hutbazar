<%--
  Created by IntelliJ IDEA.
  User: shihab
  Date: 10/28/12
  Time: 7:45 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Register</title>
    <link rel="shortcut icon"
          href="./Images/favicon.ico"/>
    <script src="http://code.jquery.com/jquery-latest.js"></script>
    <script type="text/javascript" src="http://jzaefferer.github.com/jquery-validation/jquery.validate.js"></script>


    <script>

        $(document).ready(
                function () {
                    $("#register").validate({
                    });
                }
        );


    </script>
</head>
<body>
<div id="content">
    <div class="feat_prod_box_details">

        <div class="contact_form">

            <div class="form_subtitle">Create new account</div>

            <form name="register" id="register" action="registeruser" method="post">

                <div class="form_row">
                    <label class="contact"><strong>Choose an Username:</strong></label>
                    <input type="text" name="username" size="48" class="required"/>
                </div>


                <div class="form_row">
                    <label class="contact"><strong>Choose A Password:</strong></label>
                    <input type="password" name="password" size="48" class="required password"/>
                </div>

                <div class="form_row">
                    <label class="contact"><strong>Email:</strong></label>
                    <input type="text" name="email" size="48" class="required email"/>
                </div>


                <div class="form_row">
                    <label class="contact"><strong>Contact No:</strong></label>
                    <input type="text" name="contactNumber" class="contact_input"/>
                </div>

                <div class="form_row">
                    <label class="contact"><strong>Address:</strong></label>
                    <input type="text" name="address" class="contact_input"/>
                </div>

                <div class="form_row">
                    <label class="contact"><strong>Division:</strong></label>
                    <select name="division">
                        <option value="Dhaka">Dhaka</option>
                        <option value="Chittagong">Chittagong</option>
                        <option value="Sylhet">Sylhet</option>
                        <option value="Rajshahi">Rajshahi</option>
                        <option value="Rangpur">Rangpur</option>
                        <option value="Khulna">Khulna</option>
                        <option value="Barishal">Barishal</option>
                    </select>


                </div>


                <div class="form_row">
                    <input type="submit" class="register" value="register"/>
                </div>
            </form>
        </div>
    </div>
</div>


</body>
</html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: shihab
  Date: 11/1/12
  Time: 2:32 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
</head>
<body>
<div id="content">

<div class="feat_prod_box_details">

    <div class="contact_form">

        <div class="form_subtitle">

            <c:choose>
                <c:when test="${success==null}">
                    Your Account Information.
                </c:when>
                <c:otherwise>
                    Your Profile Updated
                </c:otherwise>
            </c:choose>


        </div>


        <form name="userAccount" action="userAccount" method="post">


            <div class="form_row">


                <c:choose>


                    <c:when test="${sessionScope.isUpdate!=null}">
                        <label class="contact"><strong>New Name:</strong></label>
                        <input type="text" name="userNewName" value="${sessionScope.userName}" class="contact_input"/>
                    </c:when>

                    <c:otherwise>
                        <label class="contact"><strong>Your Name:</strong></label>
                        <input type="text" readonly="readonly" name="userName" value="${sessionScope.userName}"
                               class="contact_input"/>
                    </c:otherwise>

                </c:choose>

            </div>


            <div class="form_row">


                <c:choose>

                    <c:when test="${sessionScope.isUpdate!=null}">
                        <label class="contact"><strong>New Email:</strong></label>
                        <input type="text" name="userNewEmail" value="${sessionScope.userEmail}" class="contact_input"/>
                    </c:when>

                    <c:otherwise>
                        <label class="contact"><strong>Your Email:</strong></label>
                        <input type="text" readonly="readonly" name="userEmail" value="${sessionScope.userEmail}"
                               class="contact_input"/>
                    </c:otherwise>

                </c:choose>

            </div>


            <div class="form_row">


                <c:choose>

                    <c:when test="${sessionScope.isUpdate!=null}">
                        <label class="contact"><strong>New Contact Number:</strong></label>
                        <input type="text" name="userNewContactNumber" value="${sessionScope.userContactNumber}"
                               class="contact_input"/>
                    </c:when>

                    <c:otherwise>
                        <label class="contact"><strong>Your Contact Number:</strong></label>
                        <input type="text" readonly="readonly" name="userContactNumber"
                               value="${sessionScope.userContactNumber}" class="contact_input"/>
                    </c:otherwise>

                </c:choose>

            </div>


            <div class="form_row">


                <c:choose>

                    <c:when test="${sessionScope.isUpdate!=null}">
                        <label class="contact"><strong>Your New Address:</strong></label>
                        <input type="text" name="userNewAddress" value="${sessionScope.userAddress}"
                               class="contact_input"/>
                    </c:when>

                    <c:otherwise>
                        <label class="contact"><strong>Your Address:</strong></label>
                        <input type="text" readonly="readonly" name="userAddress" value="${sessionScope.userAddress}"
                               class="contact_input"/>
                    </c:otherwise>

                </c:choose>

            </div>


            <div class="form_row">


                <c:choose>

                    <c:when test="${sessionScope.isUpdate!=null}">

                        <div class="form_row">
                            <label class="contact"><strong>Your New Division:</strong></label>
                            <select name="userNewDivision">
                                <option value="Dhaka">Dhaka</option>
                                <option value="Chittagong">Chittagong</option>
                                <option value="Sylhet">Sylhet</option>
                                <option value="Rajshahi">Rajshahi</option>
                                <option value="Rangpur">Rangpur</option>
                                <option value="Khulna">Khulna</option>
                                <option value="Barishal">Barishal</option>
                            </select>


                        </div>
                    </c:when>

                    <c:otherwise>
                        <label class="contact"><strong>Your Division:</strong></label>
                        <input type="text" readonly="readonly" name="userDivision" value="${sessionScope.userDivision}"
                               class="contact_input"/>
                    </c:otherwise>

                </c:choose>

            </div>

    </div>

</div>

<c:choose>

    <c:when test="${sessionScope.isUpdate!=null}">


        <div class="feat_prod_box_details">

            <div class="contact_form">


                <div class="form_subtitle">
                    Change Your Password
                </div>


                <div class="form_row">

                    <label class="contact"><strong>New Password</strong></label>

                    <input type="password" name="userNewPassword" class="contact_input"/>


                </div>

                <div class="form_row">

                    <label class="contact"><strong>Confirm New Password</strong></label>


                    <input type="password" name="userNewConfirmPassword" class="contact_input"/>


                </div>


            </div>


        </div>


    </c:when>


</c:choose>


<div class="form_row">
    <input type="submit" class="register" value="Update"/>
</div>

</form>


</div>

</body>
</html>
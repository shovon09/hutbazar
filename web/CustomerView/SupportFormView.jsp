v<%--
  Created by IntelliJ IDEA.
  User: shihab
  Date: 10/30/12
  Time: 12:39 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Support</title>
</head>
<body>

<div id="content">

    <div class="feat_prod_box_details">

        <div class="contact_form">


            <div class="form_subtitle">

                <c:choose>
                    <c:when test="${success==null}">
                        Please Write to us for any support for a specific product
                    </c:when>
                    <c:otherwise>
                        Sent.Another one?
                    </c:otherwise>
                </c:choose>

            </div>


            <form name="store" action="support" method="post">

                <div class="form_row">

                    <label class="contact"><strong>Store Name:</strong></label>

                    <select name="storeId">

                        <c:forEach var="store" items="${stores}">

                            <c:choose>
                                <c:when test="${sessionScope.selectedStoreId==store.store_id}">
                                    <option selected="selected" value=${store.store_id}> ${store.store_name}</option>
                                </c:when>
                                <c:otherwise>
                                    <option value=${store.store_id}> ${store.store_name}</option>
                                </c:otherwise>
                            </c:choose>

                        </c:forEach>

                    </select>

                </div>


                <div class="form_row">
                    <input type="hidden" name="state" value="1"/>
                    <input type="submit" class="register" value="categories"/>
                </div>


            </form>


        </div>

    </div>


    <c:choose>
        <c:when test="${sessionScope.selectedStoreId!=null}">

            <div class="feat_prod_box_details">

                <div class="contact_form">


                    <div class="form_subtitle">
                        Please Choose A Category
                    </div>


                    <form name="category" action="support" method="post">

                        <div class="form_row">

                            <label class="contact"><strong>Category Name:</strong></label>

                            <select name="categoryId">

                                <c:forEach var="category" items="${category}">

                                    <c:choose>
                                        <c:when test="${sessionScope.selectedCategoryId==category.categoryID}">
                                            <option selected="selected"
                                                    value=${category.categoryID}> ${category.categoryName}</option>
                                        </c:when>
                                        <c:otherwise>
                                            <option value=${category.categoryID}> ${category.categoryName}</option>
                                        </c:otherwise>
                                    </c:choose>

                                </c:forEach>

                            </select>

                        </div>


                        <div class="form_row">
                            <input type="hidden" name="state2" value="2"/>
                            <input type="submit" class="register" value="Products"/>
                        </div>


                    </form>

                </div>


            </div>

        </c:when>

    </c:choose>


    <c:choose>
    <c:when test="${sessionScope.selectedCategoryId!=null}">

    <div class="feat_prod_box_details">

        <div class="contact_form">


            <div class="form_subtitle">
                Please Choose the product
            </div>


            <form name="product" action="support" method="post">

                <div class="form_row">

                    <label class="contact"><strong>Product Name:</strong></label>

                    <select name="productId">

                        <c:forEach var="product" items="${product}">

                            <option value=${product.productId}> ${product.productName}</option>

                        </c:forEach>

                    </select>

                </div>

                <div class="form_row">
                    <label class="contact"><strong>Your Problem/Comment:</strong></label>
                    <textarea rows="10 " cols="47" name="userComment"></textarea>
                </div>


                <div class="form_row">
                    <input type="hidden" name="state3" value="3"/>
                    <input type="submit" class="register" value="send"/>
                </div>


            </form>

        </div>

    </div>

</div>


</c:when>

</c:choose>


</body>
</html>
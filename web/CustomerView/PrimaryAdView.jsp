<%--
  Created by IntelliJ IDEA.
  User: Shovon
  Date: 10/19/12
  Time: 11:14 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ page import="hutbazar.model.customer.dao.CustomerService" %>
<%@ page import="hutbazar.model.customer.domain.StoreEntity" %>
<%@ page import="java.util.List" %>
<%
    List<StoreEntity> storeEntities = CustomerService.getPrimaryStores();
    pageContext.setAttribute("sotres", storeEntities);
%>

<div id="slider" class="box">
    <div id="slider-holder">
        <ul>
            <c:forEach var="store" items="${sotres}">

                <li><a href="storeCatagory?store_id=${store.store_id}"><img src="${store.image}" alt="" width="843"
                                                                            height="403"/></a></li>

            </c:forEach>

        </ul>
    </div>
    <div id="slider-nav">
        <c:forEach var="store" items="${sotres}">

            <a href="#">${store.store_id}</a>

        </c:forEach>

    </div>
</div>
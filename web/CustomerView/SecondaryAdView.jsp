<%--
  Created by IntelliJ IDEA.
  User: Shovon
  Date: 10/19/12
  Time: 11:46 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ page import="hutbazar.model.customer.dao.CustomerService" %>
<%@ page import="hutbazar.model.customer.domain.StoreEntity" %>
<%@ page import="java.util.List" %>
<%
    List<StoreEntity> storeEntities = CustomerService.getSecondaryStores();
    pageContext.setAttribute("sotres", storeEntities);
%>
<div class="products">
    <div class="cl">&nbsp;</div>
    <ul>
        <c:forEach var="store" items="${sotres}">
            <li>
                <a href="storeCatagory?store_id=${store.store_id}"><img src="${store.image}" alt="" height="290"
                                                                        width="200"/> </a>

                <div class="product-info">
                    <h3>${store.store_name}</h3>

                    <div class="product-desc">
                        <h4>${store.store_name}</h4>

                        <p>${store.description}<br/></p>
                        <strong class="price"></strong>
                    </div>
                </div>
            </li>
        </c:forEach>

        <%--<li>--%>
        <%--<img src="./Styles/css/images/big1.jpg" alt="" />--%>
        <%--<div class="product-info">--%>
        <%--<h3>Woolen Jacket</h3>--%>
        <%--<div class="product-desc">--%>
        <%--<h4>Mens's Jacket</h4>--%>
        <%--<p>Warm and Nice<br /></p>--%>
        <%--</div>--%>
        <%--</div>--%>
        <%--</li>--%>
        <%--<li class="last">--%>
        <%--<img src="./Styles/css/images/big1.jpg" alt="" />--%>
        <%--<div class="product-info">--%>
        <%--<h3>Leather Jacket</h3>--%>
        <%--<div class="product-desc">--%>
        <%--<h4>Leather Jacket</h4>--%>
        <%--<p>Stylish and Warm<br /></p>--%>
        <%--</div>--%>
        <%--</div>--%>
        <%--</li>--%>
    </ul>
    <div class="cl">&nbsp;</div>
</div>
<%--
  Created by IntelliJ IDEA.
  User: Shovon
  Date: 11/12/12
  Time: 8:13 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>Product With Promotion</title>
</head>
<body>
<table id="ver-minimalist" summary="Most Favorite Movies">
    <thead>
    <tr>
        <th scope="col">Product Name</th>
        <th scope="col">Promotion</th>
        <th scope="col">Promotion Start</th>
        <th scope="col">Promotion End</th>


    </tr>
    </thead>
    <tbody>
    <c:forEach var="product" items="${product}">
        <tr>
            <td>
                <a href="productDetails?product_id=${product.productId}&category_id=${product.categoryId}"> ${product.productName} </a>
            </td>
            <td>${product.discount}</td>
            <td>${product.promotionFrom}</td>
            <td>${product.promotionEnd}</td>


        </tr>
    </c:forEach>
    </tbody>
</table>
</body>
</html>
<%--
  Created by IntelliJ IDEA.
  User: Shovon
  Date: 10/31/12
  Time: 6:36 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
</head>
<body>
<%--
  Created by IntelliJ IDEA.
  UserEntity: Shovon
  Date: 10/30/12
  Time: 12:04 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title></title>
</head>
<body>
<div class="left_content">
    <div class="title"><span class="title_icon"><img src="" alt="" title=""/></span>Product List</div>

    <div class="feat_prod_box_details">

        <table class="cart_table">
            <tr class="cart_title">
                <td>Product Id</td>
                <td>Product Name</td>
                <td>Quantity</td>
                <td>Unit Price(BDT)</td>
                <td>Total(BDT)</td>

            </tr>


            <c:forEach var="cart" items="${carts}">

                <tr>
                    <td>${cart.productId}</td>
                    <td>${cart.productName}</td>
                    <td>${cart.quantity}</td>
                    <td>${cart.unitPrice}</td>
                    <td>${cart.total}</td>

                </tr>

            </c:forEach>


            <tr>
                <td colspan="4" class="cart_total"><span class="red">TOTAL: </span></td>
                <td> ${sumTotal} BDT</td>
            </tr>


        </table>

        <div class="feat_prod_box_details">
            <div class="contact_form">
                <div class="form_subtitle">Please fill the following field :</div>
                <form name="" action="checkOut" method="post">

                    <div class="form_row">
                        <label class="contact"><strong>Division :</strong></label>
                        <select name="deliveryDivision">
                            <c:forEach var="division" items="${divisions}">
                                <option value="${division.charge},${division.division}">${division.division}</option>
                                <%--<input name="division" type="hidden" value="${division.division}"/>--%>
                            </c:forEach>
                        </select>
                    </div>
                    <div class="form_row">
                        <label class="contact"><strong>Full Address :</strong></label>
                        <textarea rows="4" cols="20" name="address"></textarea>
                    </div>
                    <div class="form_row">
                        <label class="contact"><strong>Contact Number:</strong></label>
                        <input type="text" name="contactNumber" value="+880"/>
                    </div>
                    <div class="form_row">
                        <label class="contact"><strong>Bank :</strong></label>
                        <select name="bank">
                            <option value="dutchBangla">Dutch Bangla Bank Ltd</option>
                            <option value="dutchBangla">Prime Bank</option>
                            <option value="dutchBangla">Brac Bank</option>
                            <option value="dutchBangla">Islami Bank</option>
                        </select>
                    </div>
                    <div class="form_row">
                        <label class="contact"><strong>Account Number :</strong></label>
                        <input type="password" name="password"/>
                    </div>

                    <div class="form_row">
                        <input type="submit" class="register" value="Check Out"/>
                    </div>
                </form>
            </div>
        </div>

    </div>


    <div class="clear"></div>
</div>
<!--end of left content-->
</body>
</html>
</body>
</html>
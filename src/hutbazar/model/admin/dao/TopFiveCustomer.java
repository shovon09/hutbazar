package hutbazar.model.admin.dao;

import hutbazar.model.admin.domain.TopFiveCustomerEntity;
import hutbazar.util.JdbcConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: shihab
 * Date: 11/13/12
 * Time: 4:16 AM
 * To change this template use File | Settings | File Templates.
 */
public class TopFiveCustomer
{
    public static List<TopFiveCustomerEntity> getTopFiveCustomer(String storeId)
    {
        System.out.print("product list eikhane dhukse :)");
        Connection connection = JdbcConnection.getConnection();
        List<TopFiveCustomerEntity> products = new ArrayList<TopFiveCustomerEntity>();

        String sql = null;
        PreparedStatement statement = null;

        try
        {
            sql = "SELECT customer_name, customer_id, SUM( quantity ) AS quantity FROM ordertable  NATURAL JOIN customer_info WHERE  `store_id` =\"" +storeId+ "\" GROUP BY customer_name\n" +
                    "ORDER BY quantity DESC \n" +
                    "LIMIT 0 , 5 " ;

            System.out.println("product list sql"+sql);
            statement = connection.prepareStatement(sql);
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next())
            {
                System.out.println("product list detail query hoise");

                String customer_id = resultSet.getString("customer_id");
                String customer_name = resultSet.getString("customer_name");
                String quantity = String.valueOf(resultSet.getInt("quantity"));


                products.add(new TopFiveCustomerEntity(customer_id, customer_name, quantity));

            }
            JdbcConnection.close(statement);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            JdbcConnection.close(statement);
        }

        return products;
    }
}



package hutbazar.model.admin.dao;

import hutbazar.model.admin.domain.FeedBackEntity;
import hutbazar.util.JdbcConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Shovon
 * Date: 11/13/12
 * Time: 12:53 PM
 * To change this template use File | Settings | File Templates.
 */
public class GetCustomerFeedBack {
    public static List<FeedBackEntity> getFeedBack(String stID) {

        Connection connection = JdbcConnection.getConnection();
        List<FeedBackEntity> feedBackEntities = new ArrayList<FeedBackEntity>();
        String sql = null;

        PreparedStatement statement = null;
        try {
            sql = "SELECT * FROM `support` WHERE `store_id`  =  ? AND read = 0";
            statement = connection.prepareStatement(sql);
            statement.setString(1, stID);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                String queryId = resultSet.getString("queryid");
                String customerquery = resultSet.getString("customerquery");
                String read = resultSet.getString("read");
                String name = resultSet.getString("name");
                String email = resultSet.getString("email");
                feedBackEntities.add(new FeedBackEntity(queryId, customerquery, read, name, email));
            }

            JdbcConnection.close(statement);
        } catch (SQLException e) {
            e.printStackTrace();
            JdbcConnection.close(statement);
        }

        return feedBackEntities;


    }
}

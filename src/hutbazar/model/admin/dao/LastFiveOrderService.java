package hutbazar.model.admin.dao;

import hutbazar.util.JdbcConnection;
import hutbazar.model.admin.domain.OrderListEntity;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: shihab
 * Date: 11/10/12
 * Time: 12:40 AM
 * To change this template use File | Settings | File Templates.
 */
public class LastFiveOrderService
{
    public static List<OrderListEntity> gerLastFiveOrders(String storeId)
    {
    Connection connection = JdbcConnection.getConnection();
    List<OrderListEntity> orderList = new ArrayList<OrderListEntity>();

    String sql = null;
    PreparedStatement statement = null;
    try
    {
        sql = "SELECT order_id,COUNT( DISTINCT product_id ) AS total_products, SUM( quantity ) AS total_quantity,SUM(total_selling_price) AS total_sales,SUM(avg_bought_price*quantity) AS total_investment,SUM(profit) AS total_profit,order_date, delivary_date,order_status  FROM ordertable WHERE `store_id`= " + "?"+"GROUP BY order_id ORDER BY order_date DESC LIMIT 0,5";
        statement = connection.prepareStatement(sql);
        statement.setString(1,storeId);
        System.out.println(sql);
        ResultSet resultSet = statement.executeQuery();

        int order_number=1;
        while (resultSet.next())
            {
                System.out.println("query hoise");
                String order_id = resultSet.getString("order_id");
                String totalNumberOfProducts = String.valueOf(resultSet.getInt("total_products"));
                String totalQuantity = String.valueOf(resultSet.getInt("total_quantity"));
                String totalSales = resultSet.getString("total_sales");
                String totalInvestment = resultSet.getString("total_investment");
                String totalProfit= resultSet.getString("total_profit");
                String order_date = resultSet.getDate("order_date").toString();
                String delivary_date = resultSet.getDate("delivary_date").toString();
                String order_status = resultSet.getString("order_status");

                //System.out.println("Order Number" + order_number);
                System.out.println("Order id" + order_id);
                System.out.println("Order products" + totalNumberOfProducts);
                System.out.println("Order quantity" + totalQuantity);
                System.out.println("Order Date" + order_date);
                System.out.println("Delivary Date" + delivary_date);


                orderList.add(new OrderListEntity(Integer.toString(order_number), order_id,totalNumberOfProducts,totalQuantity,totalInvestment,totalSales,totalProfit,order_date,delivary_date,order_status));
                order_number++;
            }


        }
    catch (SQLException e1)
    {
            e1.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
    }

        JdbcConnection.close(statement);
        return orderList;
    }





}


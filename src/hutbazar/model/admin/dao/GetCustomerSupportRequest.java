package hutbazar.model.admin.dao;

import hutbazar.model.admin.domain.SupportEntity;
import hutbazar.util.JdbcConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Shovon
 * Date: 11/13/12
 * Time: 10:18 AM
 * To change this template use File | Settings | File Templates.
 */
public class GetCustomerSupportRequest {
    public static List<SupportEntity> getSupportList(String stID) {

        Connection connection = JdbcConnection.getConnection();
        List<SupportEntity> supportEntities = new ArrayList<SupportEntity>();
        String sql = null;

        PreparedStatement statement = null;
        try {
            sql = "SELECT * FROM `support` WHERE `store_id`= ? AND `read` = 0";

            statement = connection.prepareStatement(sql);
            statement.setString(1, stID);
            System.out.println(sql);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                String storeId = resultSet.getString("store_id");
                String categoryId = resultSet.getString("category_id");
                String productId = resultSet.getString("product_id");
                String comment = resultSet.getString("comment");
                String read = resultSet.getString("read");
                String email = resultSet.getString("email");
                supportEntities.add(new SupportEntity(storeId, categoryId, productId, comment, read, email));

            }

            JdbcConnection.close(statement);
        } catch (SQLException e) {
            e.printStackTrace();
            JdbcConnection.close(statement);
        }

        return supportEntities;


    }
}

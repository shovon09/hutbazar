package hutbazar.model.admin.dao;

import hutbazar.model.admin.domain.ProductSalesListEntity;
import hutbazar.util.JdbcConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: shihab
 * Date: 11/12/12
 * Time: 8:27 PM
 * To change this template use File | Settings | File Templates.
 */
public class ProductListService
{
    public static List <ProductSalesListEntity> getProductList(String storeId)
    {
        System.out.print("product list eikhane dhukse :)");
        Connection connection = JdbcConnection.getConnection();
        List<ProductSalesListEntity> products = new ArrayList<ProductSalesListEntity>();

        String sql = null;
        PreparedStatement statement = null;
        try
        {
            sql = "SELECT product_name, product_id,SUM( quantity ) AS total_quantity, SUM( total_selling_price ) AS total_sales, SUM( avg_bought_price * quantity ) AS total_investment, SUM( profit ) AS total_profit\n" +
                    "FROM ordertable\n" +
                    "NATURAL JOIN product_info\n" +
                    "WHERE  `store_id` =\"" +storeId+ "\"" +
                    "GROUP BY product_id";
            System.out.println("product list sql"+sql);
            statement = connection.prepareStatement(sql);
           ResultSet resultSet = statement.executeQuery();

            while (resultSet.next())
            {
                System.out.println("product list query hoise");

                String product_id = resultSet.getString("product_id");
                String product_name = resultSet.getString("product_name");
                String totalQuantity = String.valueOf(resultSet.getInt("total_quantity"));
                String totalSales = resultSet.getString("total_sales");
                String totalInvestment = resultSet.getString("total_investment");
                String totalProfit= resultSet.getString("total_profit");

                //System.out.println("Order Number" + order_number);
                System.out.println("Product id" + product_id);
           /*     System.out.println("Order products" + totalNumberOfProducts);
                System.out.println("Order quantity" + totalQuantity);
                System.out.println("Order Date" + order_date);
                System.out.println("Delivary Date" + delivary_date);*/

                products.add(new ProductSalesListEntity(product_id,product_name,totalQuantity, totalSales, totalInvestment, totalProfit));

            }
            JdbcConnection.close(statement);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            JdbcConnection.close(statement);
        }

        return products;
    }


    public static List <ProductSalesListEntity> getTopFiveProductList(String storeId)
    {
        System.out.print("product list eikhane dhukse :)");
        Connection connection = JdbcConnection.getConnection();
        List<ProductSalesListEntity> products = new ArrayList<ProductSalesListEntity>();

        String sql = null;
        PreparedStatement statement = null;
        try
        {
            sql = "SELECT product_name,product_id, SUM( quantity ) AS quantity, SUM( total_selling_price ) AS total_sales, SUM( avg_bought_price * quantity ) AS total_investment, SUM( profit ) AS total_profit FROM ordertable  NATURAL JOIN product_info WHERE  `store_id` =\"" +storeId+ "\" GROUP BY product_name ORDER BY quantity DESC LIMIT 0,5";
        //    System.out.println("product list sql"+sql);
            statement = connection.prepareStatement(sql);
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next())
            {
                System.out.println("product list query hoise");

                String product_id = resultSet.getString("product_id");
                String product_name = resultSet.getString("product_name");
                String totalQuantity = String.valueOf(resultSet.getInt("quantity"));
                String totalSales = resultSet.getString("total_sales");
                String totalInvestment = resultSet.getString("total_investment");
                String totalProfit= resultSet.getString("total_profit");

                //System.out.println("Order Number" + order_number);
                System.out.println("Product id" + product_id);
                /*     System.out.println("Order products" + totalNumberOfProducts);
             System.out.println("Order quantity" + totalQuantity);
             System.out.println("Order Date" + order_date);
             System.out.println("Delivary Date" + delivary_date);*/

                products.add(new ProductSalesListEntity(product_id,product_name,totalQuantity, totalSales, totalInvestment, totalProfit));

            }
            JdbcConnection.close(statement);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            JdbcConnection.close(statement);
        }

        return products;
    }


    public static List <ProductSalesListEntity> getProductListDetails(String productId)
    {
        System.out.print("product list eikhane dhukse :)");
        Connection connection = JdbcConnection.getConnection();
        List<ProductSalesListEntity> products = new ArrayList<ProductSalesListEntity>();

        String sql = null;
        PreparedStatement statement = null;
        try
        {
            sql = "SELECT product_name, product_id,SUM( quantity ) AS total_quantity, SUM( total_selling_price ) AS total_sales, SUM( avg_bought_price * quantity ) AS total_investment, SUM( profit ) AS total_profit\n" +
                    "FROM ordertable\n" +
                    "NATURAL JOIN product_info\n" +
                    "WHERE  `product_id` =\"" +productId+ "\"" +
                    "GROUP BY product_id";
            System.out.println("product list sql"+sql);
            statement = connection.prepareStatement(sql);
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next())
            {
                System.out.println("product list detail query hoise");

                String product_id = resultSet.getString("product_id");
                String product_name = resultSet.getString("product_name");
                String totalQuantity = String.valueOf(resultSet.getInt("total_quantity"));
                String totalSales = resultSet.getString("total_sales");
                String totalInvestment = resultSet.getString("total_investment");
                String totalProfit= resultSet.getString("total_profit");


                System.out.println("Product id details" + product_id);


                products.add(new ProductSalesListEntity(product_id,product_name,totalQuantity, totalSales, totalInvestment, totalProfit));

            }
            JdbcConnection.close(statement);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            JdbcConnection.close(statement);
        }

        return products;
    }
}

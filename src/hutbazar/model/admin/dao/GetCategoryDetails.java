package hutbazar.model.admin.dao;

import hutbazar.model.admin.domain.CategoryEntity;
import hutbazar.util.JdbcConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Shovon
 * Date: 11/10/12
 * Time: 12:45 AM
 * To change this template use File | Settings | File Templates.
 */
public class GetCategoryDetails {
    public static List<CategoryEntity> getCategory(String stId) {
        List<CategoryEntity> categoryEntities = new ArrayList<CategoryEntity>();
        Connection connection = JdbcConnection.getConnection();
        String sql = null;
        PreparedStatement statement = null;
        try {
            sql = "SELECT * FROM category WHERE store_id= ?";
            statement = connection.prepareStatement(sql);
            statement.setString(1, stId);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                String categoryId = resultSet.getString("category_id");
                String categoryName = resultSet.getString("category_name");
                String image = resultSet.getString("image");
                String storeId = resultSet.getString("store_id");
                categoryEntities.add(new CategoryEntity(categoryId, categoryName, image, storeId));
            }

            JdbcConnection.close(statement);
        } catch (SQLException e) {
            e.printStackTrace();
            JdbcConnection.close(statement);
        }
        return categoryEntities;
    }

    public static String getTotalCategory(String storeId) {
        Connection connection = JdbcConnection.getConnection();
        String sql = null;
        String totalCategory = null;
        PreparedStatement statement = null;
        try {
            sql = "SELECT COUNT(DISTINCT `category_id`) as total FROM category WHERE store_id = ?";
            statement = connection.prepareStatement(sql);
            statement.setString(1, storeId);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                totalCategory = resultSet.getString("total");
            }

            JdbcConnection.close(statement);
        } catch (SQLException e) {
            e.printStackTrace();
            JdbcConnection.close(statement);
        }
        return totalCategory;
    }
}

package hutbazar.model.admin.dao;

import hutbazar.util.JdbcConnection;
import hutbazar.model.admin.domain.OrderDetailsEntity;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: shihab
 * Date: 11/9/12
 * Time: 6:14 PM
 * To change this template use File | Settings | File Templates.
 */
public class OrderDetailsService
{
    public static List<OrderDetailsEntity> getOrderDetails(String orderID)
    {
        Connection connection = JdbcConnection.getConnection();
        List<OrderDetailsEntity> orderDetails = new ArrayList<OrderDetailsEntity>();
        System.out.print(orderID);
        String sql = null;
        PreparedStatement statement = null;

        try
        {
            sql = "SELECT order_id AS orderId, store_name AS storeName, product_name AS productName, profit, avg_bought_price, total_selling_price, quantity AS productQuantity, unit_price AS productUnitPrice, shipment_charge AS shipmentCharge, order_date AS orderDate, delivary_date AS delivaryDate, delivery_address AS delivaryAddress, order_status AS orderStatus\n" +
                    "FROM product_info\n" +
                    "JOIN store_info\n" +
                    "JOIN ordertable\n" +
                    "WHERE product_info.store_id = store_info.store_id\n" +
                    "AND product_info.product_id = ordertable.product_id\n" +
                    "AND ordertable.order_id = \"" +orderID+ "\" "   ;

            statement = connection.prepareStatement(sql);



            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next())
            {

           //   System.out.print(resultSet.toString());
                String orderId = resultSet.getString("orderId");
                System.out.println("OrderID of the selected order is " + orderId);
                String storeName = resultSet.getString("storeName");
                String productName = resultSet.getString("productName");
                String productQuantity = resultSet.getString("productQuantity");
                String productUnitPrice = resultSet.getString("productUnitPrice");
                String shipmentCharge = resultSet.getString("shipmentCharge");
                String orderDate = resultSet.getString("orderDate");
                String delivaryDate = resultSet.getString("delivaryDate");
                String delivaryAddress = resultSet.getString("delivaryAddress");
                String orderStatus = resultSet.getString("orderStatus");
                String profit = resultSet.getString("profit");
                String investment = resultSet.getString("avg_bought_price");

                String sales = resultSet.getString("total_selling_price");



                orderDetails.add(new OrderDetailsEntity( orderId,storeName, productName, productQuantity, productUnitPrice, shipmentCharge, orderDate, delivaryDate, delivaryAddress, orderStatus,sales,investment,profit));

            }
            JdbcConnection.close(statement);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            JdbcConnection.close(statement);
        }

        return orderDetails;
    }
}

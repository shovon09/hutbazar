package hutbazar.model.admin.dao;

import hutbazar.model.admin.domain.NotSoldProductEntity;
import hutbazar.util.JdbcConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: shihab
 * Date: 11/9/12
 * Time: 11:55 PM
 * To change this template use File | Settings | File Templates.
 */
public class NotSoldProductService {
    public static List<NotSoldProductEntity> getNotSoldProducts(String storeId) {

        Connection connection = JdbcConnection.getConnection();
        List<NotSoldProductEntity> notSoldProducts = new ArrayList<NotSoldProductEntity>();
        String sql = null;
        PreparedStatement statement = null;

        try {
            sql = "SELECT product_name FROM inventory WHERE number_of_sell =\"" + "0" + "\" AND store_id=\"" + storeId + "\"";
            System.out.print(sql);
            statement = connection.prepareStatement(sql);
            statement.executeQuery(sql);
            ResultSet resultSet = statement.executeQuery();


            while (resultSet.next()) {

                notSoldProducts.add(new NotSoldProductEntity(resultSet.getString("product_name")));


            }
        } catch (SQLException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return notSoldProducts;
    }
}

package hutbazar.model.admin.dao;

import hutbazar.model.admin.domain.MonthlySalesReportEntity;
import hutbazar.util.JdbcConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: shihab
 * Date: 11/13/12
 * Time: 10:48 AM
 * To change this template use File | Settings | File Templates.
 */
public class MonthlySalesReportService
{
    public static List<MonthlySalesReportEntity> getMonthySalesReport(String storeId)
    {

        Connection connection = JdbcConnection.getConnection();
        List<MonthlySalesReportEntity> monthlyReport = new ArrayList<MonthlySalesReportEntity>();

        String sql = null;
        PreparedStatement statement = null;
        try
        {
            sql = "SELECT MONTHNAME( order_date ) AS \n" +
                    "MONTH , store_id, SUM( quantity ) AS quantity, SUM( total_selling_price ) AS total_sales, SUM( quantity * avg_bought_price ) AS total_investment, SUM( profit ) AS total_profit, COUNT( DISTINCT product_id ) AS product_quantity\n" +
                    "FROM ordertable\n" +
                    "WHERE STORE_id =\""+storeId+ "\"\n" +
                    "GROUP BY MONTH \n" +
                    "ORDER BY order_date ASC ";
            System.out.println(sql);
            statement = connection.prepareStatement(sql);
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next())
            {
                System.out.println("query hoise");
                String isDelivered;
                String quantity = resultSet.getString("quantity");
                String products = resultSet.getString("product_quantity");
                String totalSales = resultSet.getString("total_sales");
                String totalInvestment = resultSet.getString("total_investment");
                String totalProfit = resultSet.getString("total_profit");
                String month = resultSet.getString("month");

                monthlyReport.add(new MonthlySalesReportEntity(month,quantity, totalSales, totalInvestment,totalProfit, products));

            }
            JdbcConnection.close(statement);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            JdbcConnection.close(statement);
        }

        return monthlyReport;


    }


}

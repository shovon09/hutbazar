package hutbazar.model.admin.dao;

import hutbazar.model.admin.domain.WishListEntity;
import hutbazar.util.JdbcConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Shovon
 * Date: 11/13/12
 * Time: 4:30 AM
 * To change this template use File | Settings | File Templates.
 */
public class GetCustomerWishList {


    public static List<WishListEntity> getWishList(String stID) {

        Connection connection = JdbcConnection.getConnection();
        List<WishListEntity> wishLists = new ArrayList<WishListEntity>();
        String sql = null;

        PreparedStatement statement = null;
        try {
            sql = "SELECT * FROM `wishlist` WHERE `storeid`  = ?";
            statement = connection.prepareStatement(sql);
            statement.setString(1,stID);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                String productName = resultSet.getString("product_name");
                String userName = resultSet.getString("username");
                String userMail = resultSet.getString("useremail");
                String description = resultSet.getString("description");
                String storeId = resultSet.getString("storeid");
                String read = resultSet.getString("read");

                wishLists.add(new WishListEntity(productName, userName, userMail, description, storeId, read));

            }

            JdbcConnection.close(statement);
        } catch (SQLException e) {
            e.printStackTrace();
            JdbcConnection.close(statement);
        }

        return wishLists;


    }
}

package hutbazar.model.admin.dao;

import hutbazar.model.admin.domain.ProductGraphEntity;
import hutbazar.util.JdbcConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: shihab
 * Date: 11/13/12
 * Time: 1:11 PM
 * To change this template use File | Settings | File Templates.
 */
public class ProductGraphService
{
    public static List<ProductGraphEntity> getProducts(String storeId,String customer_id )
    {
        Connection connection = JdbcConnection.getConnection();
        List<ProductGraphEntity> productGraph = new ArrayList<ProductGraphEntity>();

        String sql = null;
        PreparedStatement statement = null;
        try
        {
            //sql = "SELECT order_id,COUNT( DISTINCT product_id ) AS total_products, SUM( quantity ) AS total_quantity,SUM(total_selling_price) AS total_sales,SUM(avg_bought_price*quantity) AS total_investment,SUM(profit) AS total_profit,order_date, delivary_date,order_status  FROM ordertable WHERE `store_id`= " + "?"+"GROUP BY order_id";
            //  System.out.println(sql);
            sql="SELECT product_name, SUM( quantity ) AS quantity FROM ordertable NATURAL JOIN product_info WHERE store_id =\"" +storeId + "\" AND customer_id =\""+customer_id+"\" GROUP BY product_id";
            statement = connection.prepareStatement(sql);
            System.out.println(sql);
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next())
            {

                String productName = resultSet.getString("product_name");
                String quantity =   resultSet.getString("quantity");
                productGraph.add(new ProductGraphEntity(productName,quantity));
            }
            JdbcConnection.close(statement);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            JdbcConnection.close(statement);
        }

        return productGraph;

    }
}

package hutbazar.model.admin.dao;

import hutbazar.model.admin.domain.CustomerSalesReportEntity;
import hutbazar.util.JdbcConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: shihab
 * Date: 11/13/12
 * Time: 11:37 AM
 * To change this template use File | Settings | File Templates.
 */
public class CustomerWiseSalesReport
{
    public static List<CustomerSalesReportEntity> getCustomer(String storeId)
    {
        Connection connection = JdbcConnection.getConnection();
        List<CustomerSalesReportEntity> customers = new ArrayList<CustomerSalesReportEntity>();

        String sql = null;
        PreparedStatement statement = null;
        try
        {
            sql = "SELECT customer_id, customer_name, store_id, SUM( quantity ) AS quantity, SUM( total_selling_price ) AS total_sales, SUM( quantity * avg_bought_price ) AS total_investment, SUM( profit ) AS total_profit, COUNT( DISTINCT product_id ) AS product_quantity\n" +
                    "FROM ordertable\n" +
                    "NATURAL JOIN customer_info WHERE STORE_id =\""+storeId+ "\" GROUP BY customer_id ";

            statement = connection.prepareStatement(sql);
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next())
            {

                String isDelivered;
                String customerId = resultSet.getString("customer_id");
                String customerName = resultSet.getString("customer_name");
                String quantity = resultSet.getString("quantity");
                String numberOfProducts = resultSet.getString("product_quantity");
                String totalSales = resultSet.getString("total_sales");
                String totalInvestment = resultSet.getString("total_investment");
                String totalProfit = resultSet.getString("total_profit");


                customers.add(new CustomerSalesReportEntity(customerId, customerName,quantity,numberOfProducts, totalSales, totalInvestment, totalProfit));

            }
            JdbcConnection.close(statement);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            JdbcConnection.close(statement);
        }

        return customers;

    }
}

package hutbazar.model.admin.dao;

import hutbazar.model.admin.domain.CustomerSalesDetailsEntity;
import hutbazar.model.admin.domain.TopFiveCustomerEntity;
import hutbazar.util.JdbcConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Shovon
 * Date: 11/14/12
 * Time: 12:31 AM
 * To change this template use File | Settings | File Templates.
 */
public class CustomerSalesDetails
{
    public static CustomerSalesDetailsEntity getCustomerSell(String storeId,String customerId)
    {
        System.out.print("product list eikhane dhukse :)");
        Connection connection = JdbcConnection.getConnection();
        CustomerSalesDetailsEntity customerSell = null;

        String sql = null;
        PreparedStatement statement = null;

        try
        {
            sql ="select customer_id,customer_name,sum(profit)as totalProfit,sum(avg_bought_price*quantity) as totalInvestment,sum(total_selling_price)as totalSales\n" +
                    "from ordertable natural join customer_info\n" +
                    "WHERE store_id= ?" + " AND customer_id =?" +
                    "group by customer_id";



            System.out.println("product customer customer list sql"+sql);
            statement = connection.prepareStatement(sql);
            statement.setString(1,storeId);
            statement.setString(2,customerId);
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next())
            {
                System.out.println("product list detail query hoise");

                String cId = resultSet.getString("customer_id");
                String customerName = resultSet.getString("customer_name");
                String totalInvestment = resultSet.getString("totalInvestment");
                String totalSales = resultSet.getString("totalSales");
                String totalProfit = resultSet.getString("totalProfit");



                customerSell =     new CustomerSalesDetailsEntity(cId,customerName, totalInvestment,totalSales,totalProfit);

            }
            JdbcConnection.close(statement);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            JdbcConnection.close(statement);
        }

        return customerSell;
    }
}

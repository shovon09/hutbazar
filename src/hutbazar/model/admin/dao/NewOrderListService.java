package hutbazar.model.admin.dao;

import hutbazar.model.admin.domain.OrderListEntity;
import hutbazar.util.JdbcConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: shihab
 * Date: 11/9/12
 * Time: 8:20 PM
 * To change this template use File | Settings | File Templates.
 */
public class NewOrderListService {
    public static List<OrderListEntity> getNewOrderList(String storeId) {
        System.out.print("eikhane dhukse :)");
        Connection connection = JdbcConnection.getConnection();
        List<OrderListEntity> orderList = new ArrayList<OrderListEntity>();

        String sql = null;
        PreparedStatement statement = null;
        try {
            sql = "SELECT order_id,COUNT( DISTINCT product_id ) AS total_products, SUM( quantity ) AS total_quantity,SUM(total_selling_price) AS total_sales,SUM(avg_bought_price*quantity) AS total_investment,SUM(profit) AS total_profit,order_date, delivary_date,order_status  FROM ordertable WHERE `store_id`=" + "?" + "AND `order_status`=" + "?" + "GROUP BY order_id";
            System.out.println(sql);
            statement = connection.prepareStatement(sql);
            statement.setString(1, storeId);
            statement.setString(2, "pending");

            ResultSet resultSet = statement.executeQuery();
            int order_number = 1;
            while (resultSet.next()) {
                System.out.println("query hoise");
                String isDelivered;
                String order_id = resultSet.getString("order_id");
                String totalNumberOfProducts = String.valueOf(resultSet.getInt("total_products"));
                String totalQuantity = String.valueOf(resultSet.getInt("total_quantity"));
                String totalSales = resultSet.getString("total_sales");
                String totalInvestment = resultSet.getString("total_investment");
                String totalProfit = resultSet.getString("total_profit");
                String order_date = resultSet.getDate("order_date").toString();
                String delivary_date = resultSet.getDate("delivary_date").toString();
                String order_status = resultSet.getString("order_status");

                //System.out.println("Order Number" + order_number);
                System.out.println("Order id" + order_id);
                System.out.println("Order products" + totalNumberOfProducts);
                System.out.println("Order quantity" + totalQuantity);
                System.out.println("Order Date" + order_date);
                System.out.println("Delivary Date" + delivary_date);


                orderList.add(new OrderListEntity(Integer.toString(order_number), order_id, totalNumberOfProducts, totalQuantity, totalInvestment, totalSales, totalProfit, order_date, delivary_date, order_status));
                order_number++;
            }
            JdbcConnection.close(statement);
        } catch (SQLException e) {
            e.printStackTrace();
            JdbcConnection.close(statement);
        }

        return orderList;

    }
}

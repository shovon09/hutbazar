package hutbazar.model.admin.dao;

import hutbazar.util.JdbcConnection;
import hutbazar.model.admin.domain.SalesReportSummaryEntity;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: shihab
 * Date: 11/9/12
 * Time: 9:32 PM
 * To change this template use File | Settings | File Templates.
 */
public class SalesReportSummary
{
      public static List<SalesReportSummaryEntity> salesReport(String storeId)
      {
          String totalProducts = null;
          String totalOrders = null;
          String totalSales = null;
          String totalInvestment = null;
          String totalProfit = null;
          String mostSoldProductName = null;
          String mostSoldProductQuantity = null;


          Connection connection = JdbcConnection.getConnection();
          List<SalesReportSummaryEntity> salesReporySummary = new ArrayList<SalesReportSummaryEntity>();
          String sql = null;
          PreparedStatement statement = null;

          try
          {
               sql="SELECT product_name,number_of_sell FROM inventory NATURAL JOIN product_info WHERE number_of_sell >= ANY ( SELECT MAX( number_of_sell ) FROM inventory WHERE store_id =\"" +storeId+ "\" ) ";
               System.out.print(sql);
               statement = connection.prepareStatement(sql);
               statement.executeQuery(sql);
               ResultSet resultSet = statement.executeQuery();


              while (resultSet.next())
              {

                  mostSoldProductName=resultSet.getString("product_name");
                  mostSoldProductQuantity=resultSet.getString("number_of_sell");


             }
          } catch (SQLException e) {
              e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
          }





          connection = JdbcConnection.getConnection();
          sql = null;
          statement = null;

          try
          {
              sql="SELECT SUM( total_selling_price ) AS totalSales, SUM( avg_bought_price * quantity ) AS totalInvestment, SUM( profit ) AS totalProfit  FROM ordertable WHERE store_id=\"" +storeId+ "\"";


              statement = connection.prepareStatement(sql);
              statement.executeQuery(sql);
              ResultSet resultSet = statement.executeQuery();


              while (resultSet.next())
              {

                  totalSales=resultSet.getString("totalSales");
                  totalInvestment=resultSet.getString("totalInvestment");
                  totalProfit=resultSet.getString("totalProfit");




              }
          } catch (SQLException e) {
              e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
          }


          connection = JdbcConnection.getConnection();
          sql = null;
          statement = null;

          try
          {
              sql="SELECT COUNT( DISTINCT product_id ) AS totalProducts , COUNT( DISTINCT order_id ) AS totalOrders \n" +
                      "FROM ordertable WHERE store_id=\"" +storeId+ "\"";


              statement = connection.prepareStatement(sql);
              statement.executeQuery(sql);
              ResultSet resultSet = statement.executeQuery();


              while (resultSet.next())
              {
                  System.out.println("query hoise mosto sold most orders");
                  totalProducts=resultSet.getString("totalProducts");
                  totalOrders=resultSet.getString("totalOrders");

              }
          } catch (SQLException e) {
              e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
          }


          salesReporySummary.add(new SalesReportSummaryEntity(totalProducts,totalOrders,totalSales,totalInvestment,totalProfit,mostSoldProductName,mostSoldProductQuantity));


          return salesReporySummary;
      }
}
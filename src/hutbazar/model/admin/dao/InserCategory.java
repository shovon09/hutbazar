package hutbazar.model.admin.dao;

import hutbazar.util.JdbcConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Created with IntelliJ IDEA.
 * User: Shovon
 * Date: 11/10/12
 * Time: 1:44 AM
 * To change this template use File | Settings | File Templates.
 */
public class InserCategory {
    public static void insertCategory(String categoryName, String image, String storeId) {

        Connection connection = JdbcConnection.getConnection();
        PreparedStatement statement = null;

        int productId;
        String numberOfSell = "0";


        try {
            String query = "INSERT into category VALUES(?,?,?,?,?)";
            statement = connection.prepareStatement(query, statement.RETURN_GENERATED_KEYS);
            statement.setString(1, null);
            statement.setString(2, categoryName);
            statement.setString(3, image);
            statement.setString(4, storeId);
            statement.setInt(5, 0);
            statement.executeUpdate();
            JdbcConnection.close(statement);


        } catch (SQLException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }


    }
}

package hutbazar.model.admin.dao;

import hutbazar.util.JdbcConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Created with IntelliJ IDEA.
 * User: Shovon
 * Date: 11/13/12
 * Time: 1:13 PM
 * To change this template use File | Settings | File Templates.
 */
public class DeleteProduct {
    public static void deleteProduct(String productId){
        Connection connection = JdbcConnection.getConnection();
        PreparedStatement statement = null;
        String query = null;
        try {

            query = "DELETE FROM product_info WHERE product_id = ?";
            statement = connection.prepareStatement(query, statement.RETURN_GENERATED_KEYS);
            statement.setString(1,productId);
            statement.executeUpdate();

            JdbcConnection.close(statement);

            query = "DELETE FROM inventory WHERE product_id = ?";
            statement = connection.prepareStatement(query, statement.RETURN_GENERATED_KEYS);
            statement.setString(1,productId);
            statement.executeUpdate();

            JdbcConnection.close(statement);



        } catch (SQLException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

    }
}

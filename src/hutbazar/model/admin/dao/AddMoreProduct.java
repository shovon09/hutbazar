package hutbazar.model.admin.dao;

import hutbazar.util.JdbcConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Created with IntelliJ IDEA.
 * User: Shovon
 * Date: 11/9/12
 * Time: 10:52 PM
 * To change this template use File | Settings | File Templates.
 */
public class AddMoreProduct {
    public static void addMoreProduct(String productId, Double avgBoughtPrice, String sellingPrice, int quantity, String threshold) {

        Connection connection = JdbcConnection.getConnection();
        PreparedStatement statement = null;
        String query = null;
        System.out.println(productId);


        System.out.println(sellingPrice);
        System.out.println(quantity);
        System.out.println(threshold);

        try {

            query = "UPDATE `inventory` SET `avg_bought_price`=?,`stock`= ? ,`threshold`=?,`selling_price`= ? WHERE `product_id` = ?";
            statement = connection.prepareStatement(query, statement.RETURN_GENERATED_KEYS);
            statement.setDouble(1, avgBoughtPrice);
            statement.setInt(2, quantity);
            statement.setString(3, threshold);
            statement.setString(4, sellingPrice);
            statement.setString(5, productId);
            statement.executeUpdate();

            JdbcConnection.close(statement);


        } catch (SQLException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }


    }
}

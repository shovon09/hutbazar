package hutbazar.model.admin.dao;

import hutbazar.util.JdbcConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Created with IntelliJ IDEA.
 * User: Shovon
 * Date: 11/9/12
 * Time: 8:37 PM
 * To change this template use File | Settings | File Templates.
 */
public class UpdateProduct {
    public static void updateProduct(String productId, String productName, String categoryId, String storeId, String description, String image,
                                     String dateAdded, String discount, String promotionFrom, String promotionEnd, String boughtPrice, String sellingPrice,
                                     String quantity, String threshold, String promotionActive) {

        Connection connection = JdbcConnection.getConnection();
        PreparedStatement statement = null;
        String query = null;
        System.out.println(productId);
        System.out.println(productName);
        System.out.println(categoryId);
        System.out.println(storeId);
        System.out.println(description);
        System.out.println(image);
        System.out.println(dateAdded);
        System.out.println(discount);
        System.out.println(promotionFrom);
        System.out.println(promotionEnd);
        System.out.println(boughtPrice);
        System.out.println(sellingPrice);
        System.out.println(quantity);
        System.out.println(threshold);
        System.out.println(promotionActive);
        try {
            query = "UPDATE `product_info` SET `product_name`= ? ,`category_id`= ? ,`store_id`= ? ,`description`=?,`date_added`=?,`discount`=?,`promotion_from`=?,`promotion_end`=?,`active`= ? WHERE `product_id` = ?";
            statement = connection.prepareStatement(query, statement.RETURN_GENERATED_KEYS);
            statement.setString(1, productName);
            statement.setString(2, categoryId);
            statement.setString(3, storeId);
            statement.setString(4, description);
            statement.setString(5, dateAdded);
            statement.setString(6, discount);
            statement.setString(7, promotionFrom);
            statement.setString(8, promotionEnd);
            statement.setString(9, promotionActive);
            statement.setString(10, productId);
            statement.executeUpdate();

            query = "UPDATE `inventory` SET `product_name`=?,`avg_bought_price`=?,`store_id`=?,`stock`= ? ,`threshold`=?,`selling_price`= ? WHERE `product_id` = ?";
            statement = connection.prepareStatement(query, statement.RETURN_GENERATED_KEYS);

            statement.setString(1, productName);
            statement.setString(2, boughtPrice);
            statement.setString(3, storeId);
            statement.setString(4, quantity);
            statement.setString(5, threshold);
            statement.setString(6, sellingPrice);
            statement.setString(7, productId);


            statement.executeUpdate();

            query = "";
            statement = connection.prepareStatement(query, statement.RETURN_GENERATED_KEYS);

            JdbcConnection.close(statement);


        } catch (SQLException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }


    }
}

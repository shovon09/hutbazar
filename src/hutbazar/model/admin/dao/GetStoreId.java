package hutbazar.model.admin.dao;

import hutbazar.util.JdbcConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created with IntelliJ IDEA.
 * User: Shovon
 * Date: 11/9/12
 * Time: 11:45 AM
 * To change this template use File | Settings | File Templates.
 */
public class GetStoreId {
    public static String getStoreId(String userId) {

        Connection connection = JdbcConnection.getConnection();
        String sql = null;
        String storeId = null;
        PreparedStatement statement = null;
        try {
            sql = "SELECT * FROM store_info WHERE admin_id = ?";
            statement = connection.prepareStatement(sql);
            statement.setString(1, userId);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                storeId = resultSet.getString("store_id");
            }

            JdbcConnection.close(statement);
        } catch (SQLException e) {
            e.printStackTrace();
            JdbcConnection.close(statement);
        }
        return storeId;

    }
}

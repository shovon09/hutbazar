package hutbazar.model.admin.dao;

import hutbazar.model.admin.domain.ProductDetailsEntity;
import hutbazar.util.JdbcConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Shovon
 * Date: 11/9/12
 * Time: 12:54 AM
 * To change this template use File | Settings | File Templates.
 */
public class GetProductDetails {
    public static List<ProductDetailsEntity> getProductDetails(int stID) {

        Connection connection = JdbcConnection.getConnection();
        List<ProductDetailsEntity> prductDetailsEntityList = new ArrayList<ProductDetailsEntity>();
        String sql = null;
        PreparedStatement statement = null;
        try {
            sql = "SELECT * FROM `product_info` \n" +
                    "INNER JOIN inventory on product_info.`product_id` = inventory.product_id WHERE product_info.store_id = ?";

            statement = connection.prepareStatement(sql);
            statement.setInt(1, stID);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                int productId = resultSet.getInt("product_id");

                String productName = resultSet.getString("product_name");
                int categoryId = resultSet.getInt("category_id");
                int storeId = resultSet.getInt("store_id");
                String description = resultSet.getString("description");

                String image = resultSet.getString("image");
                Date dateAdded = resultSet.getDate("date_added");
                double discount = resultSet.getDouble("discount");
                Date promotionFrom = resultSet.getDate("promotion_from");
                Date promotionEnd = resultSet.getDate("promotion_end");
                int promotionActive = resultSet.getInt("active");
                double avgBoughtPrice = resultSet.getDouble("avg_bought_price");
                int stock = resultSet.getInt("stock");
                int threshold = resultSet.getInt("threshold");
                double sellingPrice = resultSet.getDouble("selling_price");
                int numberOfSell = resultSet.getInt("number_of_sell");
                prductDetailsEntityList.add(new ProductDetailsEntity(productId, productName, categoryId, storeId, description, image, dateAdded, discount, promotionFrom, promotionEnd, promotionActive, avgBoughtPrice, stock, threshold, sellingPrice, numberOfSell));

            }

            JdbcConnection.close(statement);
        } catch (SQLException e) {
            e.printStackTrace();
            JdbcConnection.close(statement);
        }
        return prductDetailsEntityList;

    }

    public static ProductDetailsEntity getProduct(int proId) {

        Connection connection = JdbcConnection.getConnection();
        ProductDetailsEntity prductDetailsEntityList = null;
        String sql = null;
        PreparedStatement statement = null;
        try {
            sql = "SELECT * FROM `product_info` \n" +
                    "INNER JOIN inventory on product_info.`product_id` = inventory.product_id WHERE product_info.product_id = ?";

            statement = connection.prepareStatement(sql);
            statement.setInt(1, proId);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                int productId = resultSet.getInt("product_id");

                String productName = resultSet.getString("product_name");
                int categoryId = resultSet.getInt("category_id");
                int storeId = resultSet.getInt("store_id");
                String description = resultSet.getString("description");

                String image = resultSet.getString("image");
                Date dateAdded = resultSet.getDate("date_added");
                double discount = resultSet.getDouble("discount");
                Date promotionFrom = resultSet.getDate("promotion_from");
                Date promotionEnd = resultSet.getDate("promotion_end");
                int promotionActive = resultSet.getInt("active");
                double avgBoughtPrice = resultSet.getDouble("avg_bought_price");
                int stock = resultSet.getInt("stock");
                int threshold = resultSet.getInt("threshold");
                double sellingPrice = resultSet.getDouble("selling_price");
                int numberOfSell = resultSet.getInt("number_of_sell");
                prductDetailsEntityList = new ProductDetailsEntity(productId, productName, categoryId, storeId, description, image, dateAdded, discount, promotionFrom, promotionEnd, promotionActive, avgBoughtPrice, stock, threshold, sellingPrice, numberOfSell);

            }

            JdbcConnection.close(statement);
        } catch (SQLException e) {
            e.printStackTrace();
            JdbcConnection.close(statement);
        }
        return prductDetailsEntityList;

    }

    public static String getTotalProduct(int stId) {
        Connection connection = JdbcConnection.getConnection();
        String sql = null;
        String total = null;
        PreparedStatement statement = null;
        try {
            sql = "SELECT COUNT(DISTINCT `product_id`) as total FROM product_info WHERE store_id = ?";
            statement = connection.prepareStatement(sql);
            statement.setInt(1, stId);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                total = resultSet.getString("total");
            }
            JdbcConnection.close(statement);

        } catch (SQLException e) {
            e.printStackTrace();
            JdbcConnection.close(statement);
        }
        return total;
    }
}

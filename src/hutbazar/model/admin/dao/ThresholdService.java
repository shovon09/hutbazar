package hutbazar.model.admin.dao;

import hutbazar.model.admin.domain.ThresholdEntity;
import hutbazar.util.JdbcConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: shihab
 * Date: 11/13/12
 * Time: 4:48 AM
 * To change this template use File | Settings | File Templates.
 */
public class ThresholdService
{
    public static List<ThresholdEntity> getProducts(String storeId)
    {
        System.out.print("product list eikhane dhukse :)");
        Connection connection = JdbcConnection.getConnection();
        List<ThresholdEntity> products = new ArrayList<ThresholdEntity>();

        String sql = null;
        PreparedStatement statement = null;

        try
        {
            sql = "SELECT product_id,product_name,stock,threshold FROM  `inventory`  WHERE stock BETWEEN 0  AND threshold +5  AND store_id = \""+storeId+"\"";

            System.out.println("product list sql"+sql);
            statement = connection.prepareStatement(sql);
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next())
            {
                System.out.println("product list detail query hoise");

                String product_id = resultSet.getString("product_id");
                String product_name = resultSet.getString("product_name");
                String stock = String.valueOf(resultSet.getInt("stock"));
                String threshold = String.valueOf(resultSet.getInt("threshold"));


                products.add(new ThresholdEntity(product_id, product_name, stock,threshold));

            }
            JdbcConnection.close(statement);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            JdbcConnection.close(statement);
        }

        return products;
    }
}

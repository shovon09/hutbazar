package hutbazar.model.admin.dao;

import hutbazar.model.admin.domain.SalesCompareEntity;
import hutbazar.util.JdbcConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created with IntelliJ IDEA.
 * User: Shovon
 * Date: 11/12/12
 * Time: 11:23 PM
 * To change this template use File | Settings | File Templates.
 */
public class SaleCompare {
    public static SalesCompareEntity getProductSales(String id) {

        Connection connection = JdbcConnection.getConnection();
        SalesCompareEntity salesCompareEntity = null;
        String sql = null;
        PreparedStatement statement = null;
        try {
            sql = "SELECT * FROM `inventory` WHERE `product_id` = ?";
            statement = connection.prepareStatement(sql);
            statement.setString(1, id);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                String productId = resultSet.getString("product_id");
                String productName = resultSet.getString("product_name");
                String storeId = resultSet.getString("store_id");
                double avgBoughtPrice = resultSet.getDouble("avg_bought_price");
                int stock = resultSet.getInt("stock");
                int threshold = resultSet.getInt("threshold");
                double sellingPrice = resultSet.getDouble("selling_price");
                int numberOfSell = resultSet.getInt("number_of_sell");
                double profitRatio = ((sellingPrice - avgBoughtPrice) / avgBoughtPrice) * 100;

                salesCompareEntity = new SalesCompareEntity(productId, productName, avgBoughtPrice, storeId, stock, threshold, sellingPrice, numberOfSell, profitRatio);

            }

            JdbcConnection.close(statement);
        } catch (SQLException e) {
            e.printStackTrace();
            JdbcConnection.close(statement);
        }
        return salesCompareEntity;

    }
}

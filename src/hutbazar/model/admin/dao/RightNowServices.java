package hutbazar.model.admin.dao;

import hutbazar.util.JdbcConnection;
import hutbazar.model.admin.domain.RightNowEntity;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: shihab
 * Date: 11/10/12
 * Time: 1:15 AM
 * To change this template use File | Settings | File Templates.
 */
public class RightNowServices
{
    public static List<RightNowEntity> getRightNow(String storeId)
    {
        Connection connection = JdbcConnection.getConnection();
        List<RightNowEntity> rightNow = new ArrayList<RightNowEntity>();

        String sql = null;

        int totalUsers=0;
        int totalOrders=0;
        double totalSales=0;
        double totalInvestment=0;
        double totalProfit=0;
        Date todaysDate = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String today = formatter.format(todaysDate);

        PreparedStatement statement = null;
        try
        {
            sql = "SELECT order_id, COUNT( DISTINCT product_id ) AS total_products, SUM( quantity ) AS total_quantity,SUM(total_selling_price) as total_sales,SUM(profit) AS total_profit,order_date, delivary_date,order_status FROM ordertable WHERE store_id =\"" +storeId+ "\" AND order_date=\"" +today+ "\"  GROUP BY order_id";
            System.out.print(sql);
            statement = connection.prepareStatement(sql);

            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next())
            {

                String order_id = resultSet.getString("order_id");
                String totalNumberOfProducts = String.valueOf(resultSet.getInt("total_products"));
                String totalQuantity = String.valueOf(resultSet.getInt("total_quantity"));
                String order_date = resultSet.getDate("order_date").toString();
                String delivary_date = resultSet.getDate("delivary_date").toString();
                String order_status = resultSet.getString("order_status");

                totalOrders=totalOrders+1;
                totalSales= totalSales + Double.parseDouble(resultSet.getString("total_sales"));
                totalProfit= totalProfit + Double.parseDouble(resultSet.getString("total_profit"));





                System.out.println("Order id" + order_id);
                System.out.println("Order products" + totalNumberOfProducts);
                System.out.println("Order quantity" + totalQuantity);
                System.out.println("Order Date" + order_date);
                System.out.println("Delivary Date" + delivary_date);
                System.out.println(today);
                System.out.println(order_date);




            }



            JdbcConnection.close(statement);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            JdbcConnection.close(statement);
        }

        /*  todaysDate = new java.util.Date();
      formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
      today = formatter.format(todaysDate);*/

        statement = null;
        try
        {
            sql = "SELECT reg_date from customer_info";
            statement = connection.prepareStatement(sql);

            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next())
            {

                Date regDate = resultSet.getDate("reg_date");


                formatter = new SimpleDateFormat("yyyy-MM-dd");
                String registrationDate =  formatter.format(regDate);

                if(today.equals(registrationDate))
                {
                    totalUsers++;
                }







            }




            JdbcConnection.close(statement);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            JdbcConnection.close(statement);
        }


        rightNow.add(new RightNowEntity(String.valueOf(totalOrders),String.valueOf(totalUsers),String.valueOf(totalSales),String.valueOf(totalProfit)));





        return rightNow;

    }

}


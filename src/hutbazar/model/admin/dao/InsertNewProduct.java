package hutbazar.model.admin.dao;

import hutbazar.util.JdbcConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Created with IntelliJ IDEA.
 * User: Shovon
 * Date: 11/9/12
 * Time: 5:06 PM
 * To change this template use File | Settings | File Templates.
 */
public class InsertNewProduct {
    public static void insertNewProduct(String productName, String categoryId, String storeId, String description, String image,
                                        String dateAdded, String discount, String promotionFrom, String promotionEnd, String boughtPrice, String sellingPrice,
                                        String quantity, String threshold, String promotionActive) {

        Connection connection = JdbcConnection.getConnection();
        PreparedStatement statement = null;

        int productId;
        String numberOfSell = "0";


        try {
            String query = "INSERT into buy_records VALUES(?,?,?,?,?,?,?)";
            statement = connection.prepareStatement(query, statement.RETURN_GENERATED_KEYS);
            statement.setString(1, null);
            statement.setString(2, storeId);
            statement.setString(3, categoryId);
            statement.setString(4, quantity);
            statement.setString(5, boughtPrice);
            statement.setString(6, dateAdded);
            statement.setString(7, productName);

            statement.executeUpdate();

            query = "INSERT into inventory VALUES(?,?,?,?,?,?,?,?)";
            statement = connection.prepareStatement(query, statement.RETURN_GENERATED_KEYS);
            statement.setString(1, null);
            statement.setString(2, productName);
            statement.setString(3, boughtPrice);
            statement.setString(4, storeId);
            statement.setString(5, quantity);
            statement.setString(6, threshold);
            statement.setString(7, sellingPrice);
            statement.setString(8, numberOfSell);
            statement.executeUpdate();

            query = "INSERT into product_info VALUES(?,?,?,?,?,?,?,?,?,?,?)";
            statement = connection.prepareStatement(query, statement.RETURN_GENERATED_KEYS);
            statement.setString(1, null);
            statement.setString(2, productName);
            statement.setString(3, categoryId);
            statement.setString(4, storeId);
            statement.setString(5, description);
            statement.setString(6, image);
            statement.setString(7, dateAdded);
            statement.setString(8, discount);
            statement.setString(9, promotionFrom);
            statement.setString(10, promotionEnd);
            statement.setString(11, promotionActive);

            statement.executeUpdate();

            JdbcConnection.close(statement);


        } catch (SQLException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

    }
}

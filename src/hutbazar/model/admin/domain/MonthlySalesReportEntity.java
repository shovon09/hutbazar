package hutbazar.model.admin.domain;

/**
 * Created with IntelliJ IDEA.
 * User: shihab
 * Date: 11/13/12
 * Time: 10:42 AM
 * To change this template use File | Settings | File Templates.
 */
public class MonthlySalesReportEntity
{
    private String month;
    private String quantity;
    private String totalSales;
    private String totalInvestment;
    private String totalProfit;
    private String products;

    public MonthlySalesReportEntity(String month, String quantity, String totalSales, String totalInvestment, String totalProfit, String products) {
        this.month = month;
        this.quantity = quantity;
        this.totalSales = totalSales;
        this.totalInvestment = totalInvestment;
        this.totalProfit = totalProfit;
        this.products = products;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getTotalSales() {
        return totalSales;
    }

    public void setTotalSales(String totalSales) {
        this.totalSales = totalSales;
    }

    public String getTotalInvestment() {
        return totalInvestment;
    }

    public void setTotalInvestment(String totalInvestment) {
        this.totalInvestment = totalInvestment;
    }

    public String getTotalProfit() {
        return totalProfit;
    }

    public void setTotalProfit(String totalProfit) {
        this.totalProfit = totalProfit;
    }

    public String getProducts() {
        return products;
    }

    public void setProducts(String products) {
        this.products = products;
    }
}

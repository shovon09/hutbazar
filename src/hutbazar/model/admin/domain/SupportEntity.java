package hutbazar.model.admin.domain;

/**
 * Created with IntelliJ IDEA.
 * User: Shovon
 * Date: 11/13/12
 * Time: 10:13 AM
 * To change this template use File | Settings | File Templates.
 */
public class SupportEntity {
   private String storeId;
    private String categoryId;
    private String productId;
    private String comment;
    private String read;
    private String email;

    public SupportEntity(String storeId, String categoryId, String productId, String comment, String read, String email) {
        this.storeId = storeId;
        this.categoryId = categoryId;
        this.productId = productId;
        this.comment = comment;
        this.read = read;
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getRead() {
        return read;
    }

    public void setRead(String read) {
        this.read = read;
    }
}

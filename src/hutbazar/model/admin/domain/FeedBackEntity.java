package hutbazar.model.admin.domain;

/**
 * Created with IntelliJ IDEA.
 * User: Shovon
 * Date: 11/13/12
 * Time: 12:49 PM
 * To change this template use File | Settings | File Templates.
 */
public class FeedBackEntity {
    private String queryId;
    private String customerquery;
    private String read;
    private String name;
    private String email;

    public FeedBackEntity(String queryId, String customerquery, String read, String name, String email) {
        this.queryId = queryId;
        this.customerquery = customerquery;
        this.read = read;
        this.name = name;
        this.email = email;
    }

    public String getQueryId() {
        return queryId;
    }

    public void setQueryId(String queryId) {
        this.queryId = queryId;
    }

    public String getCustomerquery() {
        return customerquery;
    }

    public void setCustomerquery(String customerquery) {
        this.customerquery = customerquery;
    }

    public String getRead() {
        return read;
    }

    public void setRead(String read) {
        this.read = read;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}

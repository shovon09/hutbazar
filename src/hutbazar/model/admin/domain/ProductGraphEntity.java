package hutbazar.model.admin.domain;

/**
 * Created with IntelliJ IDEA.
 * User: shihab
 * Date: 11/13/12
 * Time: 1:10 PM
 * To change this template use File | Settings | File Templates.
 */
public class ProductGraphEntity
{
    private String productName;
    private String quantity;

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public ProductGraphEntity(String productName, String quantity) {

        this.productName = productName;
        this.quantity = quantity;
    }
}

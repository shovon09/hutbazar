package hutbazar.model.admin.domain;

/**
 * Created with IntelliJ IDEA.
 * User: shihab
 * Date: 11/9/12
 * Time: 11:53 PM
 * To change this template use File | Settings | File Templates.
 */
public class NotSoldProductEntity
{
   private String productName;

    public NotSoldProductEntity(String productName) {
        this.productName = productName;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }
}
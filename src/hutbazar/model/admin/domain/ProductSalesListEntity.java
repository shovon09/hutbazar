package hutbazar.model.admin.domain;

/**
 * Created with IntelliJ IDEA.
 * User: shihab
 * Date: 11/12/12
 * Time: 8:21 PM
 * To change this template use File | Settings | File Templates.
 */
public class ProductSalesListEntity
{
    private String productId;
    private String productName;
    private String quantity;
    private String totalSales;
    private String totalInvestment;
    private String totalProfit;

    public ProductSalesListEntity(String productId, String productName, String quantity, String totalSales, String totalInvestment, String totalProfit) {
        this.productId = productId;
        System.out.println("Product id" + productId);
        this.productName = productName;
        this.quantity = quantity;
        this.totalSales = totalSales;
        this.totalInvestment = totalInvestment;
        this.totalProfit = totalProfit;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getTotalSales() {
        return totalSales;
    }

    public void setTotalSales(String totalSales) {
        this.totalSales = totalSales;
    }

    public String getTotalInvestment() {
        return totalInvestment;
    }

    public void setTotalInvestment(String totalInvestment) {
        this.totalInvestment = totalInvestment;
    }

    public String getTotalProfit() {
        return totalProfit;
    }

    public void setTotalProfit(String totalProfit) {
        this.totalProfit = totalProfit;
    }
}

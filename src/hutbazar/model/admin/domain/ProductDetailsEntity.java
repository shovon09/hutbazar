package hutbazar.model.admin.domain;

import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Shovon
 * Date: 11/9/12
 * Time: 12:44 AM
 * To change this template use File | Settings | File Templates.
 */
public class ProductDetailsEntity {
    private int productId;
    private String productName;
    private int categoryId;
    private int storeId;
    private String description;
    private String image;
    private Date dateAdded;
    private double discount;
    private Date promotionFrom;
    private Date promotionEnd;
    private int promotionActive;
    private double avgBoughtPrice;
    private int stock;
    private int threshold;
    private double sellingPrice;
    private int numberOfSell;

    public ProductDetailsEntity(int productId, String productName, int categoryId, int storeId, String description, String image, Date dateAdded, double discount, Date promotionFrom, Date promotionEnd, int promotionActive, double avgBoughtPrice, int stock, int threshold, double sellingPrice, int numberOfSell) {
        this.productId = productId;
        this.productName = productName;
        this.categoryId = categoryId;
        this.storeId = storeId;
        this.description = description;
        this.image = image;
        this.dateAdded = dateAdded;
        this.discount = discount;
        this.promotionFrom = promotionFrom;
        this.promotionEnd = promotionEnd;
        this.promotionActive = promotionActive;
        this.avgBoughtPrice = avgBoughtPrice;
        this.stock = stock;
        this.threshold = threshold;
        this.sellingPrice = sellingPrice;
        this.numberOfSell = numberOfSell;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public int getStoreId() {
        return storeId;
    }

    public void setStoreId(int storeId) {
        this.storeId = storeId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Date getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(Date dateAdded) {
        this.dateAdded = dateAdded;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public Date getPromotionFrom() {
        return promotionFrom;
    }

    public void setPromotionFrom(Date promotionFrom) {
        this.promotionFrom = promotionFrom;
    }

    public Date getPromotionEnd() {
        return promotionEnd;
    }

    public void setPromotionEnd(Date promotionEnd) {
        this.promotionEnd = promotionEnd;
    }

    public int getPromotionActive() {
        return promotionActive;
    }

    public void setPromotionActive(int promotionActive) {
        this.promotionActive = promotionActive;
    }

    public double getAvgBoughtPrice() {
        return avgBoughtPrice;
    }

    public void setAvgBoughtPrice(double avgBoughtPrice) {
        this.avgBoughtPrice = avgBoughtPrice;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public int getThreshold() {
        return threshold;
    }

    public void setThreshold(int threshold) {
        this.threshold = threshold;
    }

    public double getSellingPrice() {
        return sellingPrice;
    }

    public void setSellingPrice(double sellingPrice) {
        this.sellingPrice = sellingPrice;
    }

    public int getNumberOfSell() {
        return numberOfSell;
    }

    public void setNumberOfSell(int numberOfSell) {
        this.numberOfSell = numberOfSell;
    }
}

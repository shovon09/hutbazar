package hutbazar.model.admin.domain;

/**
 * Created with IntelliJ IDEA.
 * User: shihab
 * Date: 11/13/12
 * Time: 11:38 AM
 * To change this template use File | Settings | File Templates.
 */
public class CustomerSalesReportEntity
{
    private String customerId;
    private String customerName;
    private String quantity;
    private String totalProducts;
    private String totalSales;
    private String totalInvestment;
    private String totalProfit;

    public CustomerSalesReportEntity(String customerId, String customerName, String quantity, String totalProducts, String totalSales, String totalInvestment, String totalProfit) {
        this.customerId = customerId;
        this.customerName = customerName;
        this.quantity = quantity;
        this.totalProducts = totalProducts;
        this.totalSales = totalSales;
        this.totalInvestment = totalInvestment;
        this.totalProfit = totalProfit;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getTotalProducts() {
        return totalProducts;
    }

    public void setTotalProducts(String totalProducts) {
        this.totalProducts = totalProducts;
    }

    public String getTotalSales() {
        return totalSales;
    }

    public void setTotalSales(String totalSales) {
        this.totalSales = totalSales;
    }

    public String getTotalInvestment() {
        return totalInvestment;
    }

    public void setTotalInvestment(String totalInvestment) {
        this.totalInvestment = totalInvestment;
    }

    public String getTotalProfit() {
        return totalProfit;
    }

    public void setTotalProfit(String totalProfit) {
        this.totalProfit = totalProfit;
    }
}

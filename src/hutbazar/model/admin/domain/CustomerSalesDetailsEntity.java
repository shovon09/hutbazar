package hutbazar.model.admin.domain;

/**
 * Created with IntelliJ IDEA.
 * User: Shovon
 * Date: 11/14/12
 * Time: 12:23 AM
 * To change this template use File | Settings | File Templates.
 */
public class CustomerSalesDetailsEntity
{
    private String customerId;
    private String customerSales;

    private String totalInvestment;
    private String totalSales;
    private String totalProfit;

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getCustomerSales() {
        return customerSales;
    }

    public void setCustomerSales(String customerSales) {
        this.customerSales = customerSales;
    }

    public String getTotalInvestment() {
        return totalInvestment;
    }

    public void setTotalInvestment(String totalInvestment) {
        this.totalInvestment = totalInvestment;
    }

    public String getTotalSales() {
        return totalSales;
    }

    public void setTotalSales(String totalSales) {
        this.totalSales = totalSales;
    }

    public String getTotalProfit() {
        return totalProfit;
    }

    public void setTotalProfit(String totalProfit) {
        this.totalProfit = totalProfit;
    }

    public CustomerSalesDetailsEntity(String customerId, String customerSales, String totalInvestment, String totalSales, String totalProfit) {

        this.customerId = customerId;
        this.customerSales = customerSales;
        this.totalInvestment = totalInvestment;
        this.totalSales = totalSales;
        this.totalProfit = totalProfit;
    }
}

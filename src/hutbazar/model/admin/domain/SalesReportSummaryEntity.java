package hutbazar.model.admin.domain;

/**
 * Created with IntelliJ IDEA.
 * User: shihab
 * Date: 11/9/12
 * Time: 9:32 PM
 * To change this template use File | Settings | File Templates.
 */
public class SalesReportSummaryEntity
{
    private String totalProducts;
    private String totalOrders;
    private String totalSales;
    private String totalInvestment;
    private String totalProfit;
    private String mostSoldProductName;
    private String mostSoldProductQuantity;


    public String getTotalProducts() {
        return totalProducts;
    }

    public void setTotalProducts(String totalProducts) {
        this.totalProducts = totalProducts;
    }

    public String getTotalOrders() {
        return totalOrders;
    }

    public void setTotalOrders(String totalOrders) {
        this.totalOrders = totalOrders;
    }

    public String getTotalSales() {
        return totalSales;
    }

    public void setTotalSales(String totalSales) {
        this.totalSales = totalSales;
    }

    public String getTotalInvestment() {
        return totalInvestment;
    }

    public void setTotalInvestment(String totalInvestment) {
        this.totalInvestment = totalInvestment;
    }

    public String getTotalProfit() {
        return totalProfit;
    }

    public void setTotalProfit(String totalProfit) {
        this.totalProfit = totalProfit;
    }

    public String getMostSoldProductName() {
        return mostSoldProductName;
    }

    public void setMostSoldProductName(String mostSoldProductName) {
        this.mostSoldProductName = mostSoldProductName;
    }

    public String getMostSoldProductQuantity() {
        return mostSoldProductQuantity;
    }

    public void setMostSoldProductQuantity(String mostSoldProductQuantity) {
        this.mostSoldProductQuantity = mostSoldProductQuantity;
    }

    public SalesReportSummaryEntity(String totalProducts, String totalOrders, String totalSales, String totalInvestment, String totalProfit, String mostSoldProductName, String mostSoldProductQuantity) {

        this.totalProducts = totalProducts;
        this.totalOrders = totalOrders;
        this.totalSales = totalSales;
        this.totalInvestment = totalInvestment;
        this.totalProfit = totalProfit;
        this.mostSoldProductName = mostSoldProductName;
        this.mostSoldProductQuantity = mostSoldProductQuantity;
    }
}





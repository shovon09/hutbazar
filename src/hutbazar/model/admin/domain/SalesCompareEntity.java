package hutbazar.model.admin.domain;

/**
 * Created with IntelliJ IDEA.
 * User: Shovon
 * Date: 11/12/12
 * Time: 11:07 PM
 * To change this template use File | Settings | File Templates.
 */
public class SalesCompareEntity {
    private String productId;
    private String productName;
    private double unitPrice;
    private String storeId;
    private int stock;
    private int threshold;
    private double sellingPrice;
    private int numberOfSell;
    private double profitRatio;

    public SalesCompareEntity(String productId, String productName, double unitPrice, String storeId, int stock, int threshold, double sellingPrice, int numberOfSell, double profitRatio) {
        this.productId = productId;
        this.productName = productName;
        this.unitPrice = unitPrice;
        this.storeId = storeId;
        this.stock = stock;
        this.threshold = threshold;
        this.sellingPrice = sellingPrice;
        this.numberOfSell = numberOfSell;
        this.profitRatio = profitRatio;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(double unitPrice) {
        this.unitPrice = unitPrice;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public int getThreshold() {
        return threshold;
    }

    public void setThreshold(int threshold) {
        this.threshold = threshold;
    }

    public double getSellingPrice() {
        return sellingPrice;
    }

    public void setSellingPrice(double sellingPrice) {
        this.sellingPrice = sellingPrice;
    }

    public int getNumberOfSell() {
        return numberOfSell;
    }

    public void setNumberOfSell(int numberOfSell) {
        this.numberOfSell = numberOfSell;
    }

    public double getProfitRatio() {
        return profitRatio;
    }

    public void setProfitRatio(double profitRatio) {
        this.profitRatio = profitRatio;
    }
}

package hutbazar.model.admin.domain;

/**
 * Created with IntelliJ IDEA.
 * User: shihab
 * Date: 11/7/12
 * Time: 12:33 AM
 * To change this template use File | Settings | File Templates.
 */
public class OrderDetailsEntity
{
    private String orderId;
    private String storeName;
    private String productName;
    private String productQuantity;
    private String productUnitPrice;
    private String shipmentCharge;
    private String orderDate;
    private String delivaryDate;
    private String delivaryAddress;
    private String orderStatus;
    private String sales;
    private String investment;
    private String profit;

   public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductQuantity() {
        return productQuantity;
    }

    public void setProductQuantity(String productQuantity) {
        this.productQuantity = productQuantity;
    }

    public String getProductUnitPrice() {
        return productUnitPrice;
    }

    public void setProductUnitPrice(String productUnitPrice) {
        this.productUnitPrice = productUnitPrice;
    }

    public String getShipmentCharge() {
        return shipmentCharge;
    }

    public void setShipmentCharge(String shipmentCharge) {
        this.shipmentCharge = shipmentCharge;
    }


    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public String getDelivaryDate() {
        return delivaryDate;
    }

    public void setDelivaryDate(String delivaryDate) {
        this.delivaryDate = delivaryDate;
    }

    public String getDelivaryAddress() {
        return delivaryAddress;
    }

    public void setDelivaryAddress(String delivaryAddress) {
        this.delivaryAddress = delivaryAddress;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getSales() {
        return sales;
    }

    public void setSales(String sales) {
        this.sales = sales;
    }

    public String getInvestment() {
        return investment;
    }

    public void setInvestment(String investment) {
        this.investment = investment;
    }

    public String getProfit() {
        return profit;
    }

    public void setProfit(String profit) {
        this.profit = profit;
    }

    public String getOrderId() {
         return orderId;

     }

    public OrderDetailsEntity(String orderId, String storeName, String productName, String productQuantity, String productUnitPrice, String shipmentCharge, String orderDate, String delivaryDate, String delivaryAddress, String orderStatus, String sales, String investment, String profit) {

        this.orderId = orderId;
        this.storeName = storeName;
        this.productName = productName;
        this.productQuantity = productQuantity;
        this.productUnitPrice = productUnitPrice;
        this.shipmentCharge = shipmentCharge;

        this.orderDate = orderDate;
        this.delivaryDate = delivaryDate;
        this.delivaryAddress = delivaryAddress;
        this.orderStatus = orderStatus;
        this.sales = sales;
        this.investment = investment;
        this.profit = profit;
    }








 }








































































































































































































































package hutbazar.model.admin.domain;

/**
 * Created with IntelliJ IDEA.
 * User: Shovon
 * Date: 11/13/12
 * Time: 4:20 AM
 * To change this template use File | Settings | File Templates.
 */
public class WishListEntity {
    private String productName;
    private String userName;
    private String userMail;
    private String description;
    private String storeId;
    private String read;

    public WishListEntity(String productName, String userName, String userMail, String description, String storeId, String read) {
        this.productName = productName;
        this.userName = userName;
        this.userMail = userMail;
        this.description = description;
        this.storeId = storeId;
        this.read = read;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserMail() {
        return userMail;
    }

    public void setUserMail(String userMail) {
        this.userMail = userMail;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getRead() {
        return read;
    }

    public void setRead(String read) {
        this.read = read;
    }
}

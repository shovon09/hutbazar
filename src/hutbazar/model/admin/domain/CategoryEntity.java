package hutbazar.model.admin.domain;

/**
 * Created with IntelliJ IDEA.
 * User: Shovon
 * Date: 11/10/12
 * Time: 12:43 AM
 * To change this template use File | Settings | File Templates.
 */
public class CategoryEntity {
    private String categoryId;
    private String categoryName;
    private String image;
    private String storeId;

    public CategoryEntity(String categoryId, String categoryName, String image, String storeId) {
        this.categoryId = categoryId;
        this.categoryName = categoryName;
        this.image = image;
        this.storeId = storeId;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }
}

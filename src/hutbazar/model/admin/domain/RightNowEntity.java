package hutbazar.model.admin.domain;

/**
 * Created with IntelliJ IDEA.
 * User: shihab
 * Date: 11/10/12
 * Time: 1:10 AM
 * To change this template use File | Settings | File Templates.
 */
public class RightNowEntity
{
    private String totalOrders;
    private String totalUsers;
    private String totalSales;
    private String totalProfit;

    public RightNowEntity(String totalOrders, String totalUsers, String totalSales, String totalProfit) {
        this.totalOrders = totalOrders;
        this.totalUsers = totalUsers;
        this.totalSales = totalSales;
        this.totalProfit = totalProfit;
    }

    public String getTotalOrders() {
        return totalOrders;
    }

    public void setTotalOrders(String totalOrders) {
        this.totalOrders = totalOrders;
    }

    public String getTotalUsers() {
        return totalUsers;
    }

    public void setTotalUsers(String totalUsers) {
        this.totalUsers = totalUsers;
    }

    public String getTotalSales() {
        return totalSales;
    }

    public void setTotalSales(String totalSales) {
        this.totalSales = totalSales;
    }

    public String getTotalProfit() {
        return totalProfit;
    }

    public void setTotalProfit(String totalProfit) {
        this.totalProfit = totalProfit;
    }
}

package hutbazar.model.admin.domain;

/**
 * Created with IntelliJ IDEA.
 * User: shihab
 * Date: 11/13/12
 * Time: 4:06 AM
 * To change this template use File | Settings | File Templates.
 */
public class TopFiveCustomerEntity
{
    private String customerId;
    private String customerName;
    private String quantity;

    public TopFiveCustomerEntity(String customerId, String customerName, String quantity) {
        this.customerId = customerId;
        this.customerName = customerName;
        this.quantity = quantity;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }
}

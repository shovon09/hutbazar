package hutbazar.model.admin.domain;

/**
 * Created with IntelliJ IDEA.
 * User: shihab
 * Date: 11/13/12
 * Time: 4:45 AM
 * To change this template use File | Settings | File Templates.
 */
public class ThresholdEntity {
    private String productId;
    private String productName;
    private String stock;
    private String threshold;

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getStock() {
        return stock;
    }

    public void setStock(String stock) {
        this.stock = stock;
    }

    public String getThreshold() {
        return threshold;
    }

    public void setThreshold(String threshold) {
        this.threshold = threshold;
    }

    public ThresholdEntity(String productId, String productName, String stock, String threshold) {

        this.productId = productId;
        this.productName = productName;
        this.stock = stock;
        this.threshold = threshold;
    }
}
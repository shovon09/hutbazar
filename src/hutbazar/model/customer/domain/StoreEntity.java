package hutbazar.model.customer.domain;

import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * UserEntity: Shovon
 * Date: 10/23/12
 * Time: 10:08 AM
 * To change this template use File | Settings | File Templates.
 */
public class StoreEntity {
    private int store_id;
    private String store_name = null;
    private String description = null;
    private String type = null;
    private int admin_id;
    private Date activation_from = null;
    private Date activation_end = null;
    private String image = null;
    private int active;

    public StoreEntity(int store_id, String store_name, String description, String type, int admin_id, Date activation_from, Date activation_end, String image, int active) {
        this.store_id = store_id;
        this.store_name = store_name;
        this.description = description;
        this.type = type;
        this.admin_id = admin_id;
        this.activation_from = activation_from;
        this.activation_end = activation_end;
        this.image = image;
        this.active = active;
    }

    public int getStore_id() {
        return store_id;
    }

    public void setStore_id(int store_id) {
        this.store_id = store_id;
    }

    public String getStore_name() {
        return store_name;
    }

    public void setStore_name(String store_name) {
        this.store_name = store_name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getAdmin_id() {
        return admin_id;
    }

    public void setAdmin_id(int admin_id) {
        this.admin_id = admin_id;
    }

    public Date getActivation_from() {
        return activation_from;
    }

    public void setActivation_from(Date activation_from) {
        this.activation_from = activation_from;
    }

    public Date getActivation_end() {
        return activation_end;
    }

    public void setActivation_end(Date activation_end) {
        this.activation_end = activation_end;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getActive() {
        return active;
    }

    public void setActive(int active) {
        this.active = active;
    }
}

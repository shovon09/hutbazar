package hutbazar.model.customer.domain;

/**
 * Created with IntelliJ IDEA.
 * UserEntity: shihab
 * Date: 10/26/12
 * Time: 12:43 AM
 * To change this template use File | Settings | File Templates.
 */
public class UserEntity {
    public static final String CUSTOMER = "0";
    public static final String ADMIN = "1";

    private String customer_id = null;
    private String customer_name = null;
    private String email = null;
    private String contactNumber = null;
    private String address = null;
    private String division = null;
    private String password = null;
    private String total_credit = null;
    private String admin_type = null;

    public UserEntity(String customer_id, String customer_name, String email, String contactNumber, String address, String division, String password, String total_credit, String admin_type) {
        this.customer_id = customer_id;
        this.customer_name = customer_name;
        this.email = email;
        this.contactNumber = contactNumber;
        this.address = address;
        this.division = division;
        this.password = password;
        this.total_credit = total_credit;
        this.admin_type = admin_type;
    }

    public String getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(String customer_id) {
        this.customer_id = customer_id;
    }

    public String getCustomer_name() {
        return customer_name;
    }

    public void setCustomer_name(String customer_name) {
        this.customer_name = customer_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDivision() {
        return division;
    }

    public void setDivision(String division) {
        this.division = division;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTotal_credit() {
        return total_credit;
    }

    public void setTotal_credit(String total_credit) {
        this.total_credit = total_credit;
    }

    public String getAdmin_type() {
        return admin_type;
    }

    public void setAdmin_type(String admin_type) {
        this.admin_type = admin_type;
    }
}
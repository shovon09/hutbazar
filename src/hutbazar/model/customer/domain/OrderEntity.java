package hutbazar.model.customer.domain;

/**
 * Created with IntelliJ IDEA.
 * User: shihab
 * Date: 11/5/12
 * Time: 11:58 PM
 * To change this template use File | Settings | File Templates.
 */
public class OrderEntity {
    private String order_id;
    private String customer_id;
    private String category_id;
    private String store_id;
    private String product_id;
    private String quantity;
    private String unit_price;
    private String total_selling_price;
    private String avg_bought_price;
    private String profit;
    private String delivery_address;
    private String shipment_charge;
    private String division;
    private String order_date;
    private String delivary_date;
    private String order_status;


    public OrderEntity(String order_id, String customer_id, String category_id, String store_id, String product_id, String quantity, String unit_price, String total_selling_price, String avg_bought_price, String profit, String delivery_address, String shipment_charge, String division, String order_date, String delivary_date, String order_status) {
        this.order_id = order_id;
        this.customer_id = customer_id;
        this.category_id = category_id;
        this.store_id = store_id;
        this.product_id = product_id;
        this.quantity = quantity;
        this.unit_price = unit_price;
        this.total_selling_price = total_selling_price;
        this.avg_bought_price = avg_bought_price;
        this.profit = profit;
        this.delivery_address = delivery_address;
        this.shipment_charge = shipment_charge;
        this.division = division;
        this.order_date = order_date;
        this.delivary_date = delivary_date;
        this.order_status = order_status;

    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(String customer_id) {
        this.customer_id = customer_id;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getStore_id() {
        return store_id;
    }

    public void setStore_id(String store_id) {
        this.store_id = store_id;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getUnit_price() {
        return unit_price;
    }

    public void setUnit_price(String unit_price) {
        this.unit_price = unit_price;
    }

    public String getTotal_selling_price() {
        return total_selling_price;
    }

    public void setTotal_selling_price(String total_selling_price) {
        this.total_selling_price = total_selling_price;
    }

    public String getAvg_bought_price() {
        return avg_bought_price;
    }

    public void setAvg_bought_price(String avg_bought_price) {
        this.avg_bought_price = avg_bought_price;
    }

    public String getProfit() {
        return profit;
    }

    public void setProfit(String profit) {
        this.profit = profit;
    }

    public String getDelivery_address() {
        return delivery_address;
    }

    public void setDelivery_address(String delivery_address) {
        this.delivery_address = delivery_address;
    }

    public String getShipment_charge() {
        return shipment_charge;
    }

    public void setShipment_charge(String shipment_charge) {
        this.shipment_charge = shipment_charge;
    }

    public String getDivision() {
        return division;
    }

    public void setDivision(String division) {
        this.division = division;
    }

    public String getOrder_date() {
        return order_date;
    }

    public void setOrder_date(String order_date) {
        this.order_date = order_date;
    }

    public String getDelivary_date() {
        return delivary_date;
    }

    public void setDelivary_date(String delivary_date) {
        this.delivary_date = delivary_date;
    }

    public String getOrder_status() {
        return order_status;
    }

    public void setOrder_status(String order_status) {
        this.order_status = order_status;
    }
}
package hutbazar.model.customer.domain;

import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * UserEntity: Shovon
 * Date: 10/24/12
 * Time: 8:54 PM
 * To change this template use File | Settings | File Templates.
 */
public class ProductEntity {
    private int productId;
    private String productName;
    private int categoryId;
    private int storeId;
    private String description;
    private String image;
    private Date dateAdded;
    private double discount;
    private Date promotionFrom;
    private Date promotionEnd;
    private int promotionActive;

    public ProductEntity(int productId, String productName, int categoryId, int storeId, String description, String image, Date dateAdded, double discount, Date promotionFrom, Date promotionEnd, int promotionActive) {
        this.productId = productId;
        this.productName = productName;
        this.categoryId = categoryId;
        this.storeId = storeId;
        this.description = description;
        this.image = image;
        this.dateAdded = dateAdded;
        this.discount = discount;
        this.promotionFrom = promotionFrom;
        this.promotionEnd = promotionEnd;
        this.promotionActive = promotionActive;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public int getStoreId() {
        return storeId;
    }

    public void setStoreId(int storeId) {
        this.storeId = storeId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Date getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(Date dateAdded) {
        this.dateAdded = dateAdded;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public Date getPromotionFrom() {
        return promotionFrom;
    }

    public void setPromotionFrom(Date promotionFrom) {
        this.promotionFrom = promotionFrom;
    }

    public Date getPromotionEnd() {
        return promotionEnd;
    }

    public void setPromotionEnd(Date promotionEnd) {
        this.promotionEnd = promotionEnd;
    }

    public int getPromotionActive() {
        return promotionActive;
    }

    public void setPromotionActive(int promotionActive) {
        this.promotionActive = promotionActive;
    }
}

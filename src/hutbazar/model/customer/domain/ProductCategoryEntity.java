package hutbazar.model.customer.domain;

/**
 * Created with IntelliJ IDEA.
 * UserEntity: Shovon
 * Date: 10/23/12
 * Time: 9:36 PM
 * To change this template use File | Settings | File Templates.
 */
public class ProductCategoryEntity {
    private int categoryID;
    private String categoryName = null;
    private String image = null;
    private int storeID;
    private int parentID;

    public ProductCategoryEntity(int categoryID, String categoryName, String image, int storeID, int parentID) {
        this.categoryID = categoryID;
        this.categoryName = categoryName;
        this.image = image;
        this.storeID = storeID;
        this.parentID = parentID;
    }

    public int getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(int categoryID) {
        this.categoryID = categoryID;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getStoreID() {
        return storeID;
    }

    public void setStoreID(int storeID) {
        this.storeID = storeID;
    }

    public int getParentID() {
        return parentID;
    }

    public void setParentID(int parentID) {
        this.parentID = parentID;
    }
}

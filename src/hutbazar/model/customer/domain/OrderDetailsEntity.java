package hutbazar.model.customer.domain;

/**
 * Created with IntelliJ IDEA.
 * User: shihab
 * Date: 11/7/12
 * Time: 12:33 AM
 * To change this template use File | Settings | File Templates.
 */
public class OrderDetailsEntity {
    public String orderId;
    public String storeName;
    public String productName;
    public String productQuantity;
    public String productUnitPrice;
    public String shipmentCharge;
    public String total;
    public String orderDate;
    public String delivaryDate;
    public String delivaryAddress;
    public String orderStatus;

    public OrderDetailsEntity(String orderId, String storeName, String productName, String productQuantity, String productUnitPrice, String shipmentCharge, String total, String orderDate, String delivaryDate, String delivaryAddress, String orderStatus) {
        this.orderId = orderId;
        this.storeName = storeName;
        this.productName = productName;
        this.productQuantity = productQuantity;
        this.productUnitPrice = productUnitPrice;
        this.shipmentCharge = shipmentCharge;
        this.total = total;
        this.orderDate = orderDate;
        this.delivaryDate = delivaryDate;
        this.delivaryAddress = delivaryAddress;
        this.orderStatus = orderStatus;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductQuantity() {
        return productQuantity;
    }

    public void setProductQuantity(String productQuantity) {
        this.productQuantity = productQuantity;
    }

    public String getProductUnitPrice() {
        return productUnitPrice;
    }

    public void setProductUnitPrice(String productUnitPrice) {
        this.productUnitPrice = productUnitPrice;
    }

    public String getShipmentCharge() {
        return shipmentCharge;
    }

    public void setShipmentCharge(String shipmentCharge) {
        this.shipmentCharge = shipmentCharge;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public String getDelivaryDate() {
        return delivaryDate;
    }

    public void setDelivaryDate(String delivaryDate) {
        this.delivaryDate = delivaryDate;
    }

    public String getDelivaryAddress() {
        return delivaryAddress;
    }

    public void setDelivaryAddress(String delivaryAddress) {
        this.delivaryAddress = delivaryAddress;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }
}

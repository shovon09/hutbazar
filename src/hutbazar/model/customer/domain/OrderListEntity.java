package hutbazar.model.customer.domain;

/**
 * Created with IntelliJ IDEA.
 * User: shihab
 * Date: 11/6/12
 * Time: 12:26 AM
 * To change this template use File | Settings | File Templates.
 */
public class OrderListEntity {
    private String order_number;
    private String order_id;
    private String totalNumberOfProducts;
    private String totalQuantity;
    private String order_date;
    private String delivary_date;
    private String order_status;
    private String delivered;

    public String getOrder_number() {
        return order_number;
    }

    public void setOrder_number(String order_number) {
        this.order_number = order_number;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getTotalNumberOfProducts() {
        return totalNumberOfProducts;
    }

    public void setTotalNumberOfProducts(String totalNumberOfProducts) {
        this.totalNumberOfProducts = totalNumberOfProducts;
    }

    public String getTotalQuantity() {
        return totalQuantity;
    }

    public void setTotalQuantity(String totalQuantity) {
        this.totalQuantity = totalQuantity;
    }

    public String getOrder_date() {
        return order_date;
    }

    public void setOrder_date(String order_date) {
        this.order_date = order_date;
    }

    public String getDelivary_date() {
        return delivary_date;
    }

    public void setDelivary_date(String delivary_date) {
        this.delivary_date = delivary_date;
    }

    public String getOrder_status() {
        return order_status;
    }

    public void setOrder_status(String order_status) {
        this.order_status = order_status;
    }

    public String getDelivered() {
        return delivered;
    }

    public void setDelivered(String delivered) {
        this.delivered = delivered;
    }

    public OrderListEntity(String order_number, String order_id, String totalNumberOfProducts, String totalQuantity, String order_date, String delivary_date, String order_status, String delivered) {
        this.order_number = order_number;
        this.order_id = order_id;
        this.totalNumberOfProducts = totalNumberOfProducts;

        this.totalQuantity = totalQuantity;
        this.order_date = order_date;
        this.delivary_date = delivary_date;
        this.order_status = order_status;
        this.delivered = delivered;
    }
}
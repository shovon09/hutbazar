package hutbazar.model.customer.domain;

/**
 * Created with IntelliJ IDEA.
 * UserEntity: Shovon
 * Date: 10/26/12
 * Time: 11:43 AM
 * To change this template use File | Settings | File Templates.
 */
public class InventoryEntity {
    private int productId;
    private String productName;
    private double avgBoughtPrice;
    private int storeId;
    private int stock;
    private int threshold;
    private double sellingPrice;
    private int numberOfSell;

    public InventoryEntity(int productId, String productName, double avgBoughtPrice, int storeId, int stock, int threshold, double sellingPrice, int numberOfSell) {
        this.productId = productId;
        this.productName = productName;
        this.avgBoughtPrice = avgBoughtPrice;
        this.storeId = storeId;
        this.stock = stock;
        this.threshold = threshold;
        this.sellingPrice = sellingPrice;
        this.numberOfSell = numberOfSell;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public double getAvgBoughtPrice() {
        return avgBoughtPrice;
    }

    public void setAvgBoughtPrice(double avgBoughtPrice) {
        this.avgBoughtPrice = avgBoughtPrice;
    }

    public int getStoreId() {
        return storeId;
    }

    public void setStoreId(int storeId) {
        this.storeId = storeId;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public int getThreshold() {
        return threshold;
    }

    public void setThreshold(int threshold) {
        this.threshold = threshold;
    }

    public double getSellingPrice() {
        return sellingPrice;
    }

    public void setSellingPrice(double sellingPrice) {
        this.sellingPrice = sellingPrice;
    }

    public int getNumberOfSell() {
        return numberOfSell;
    }

    public void setNumberOfSell(int numberOfSell) {
        this.numberOfSell = numberOfSell;
    }
}

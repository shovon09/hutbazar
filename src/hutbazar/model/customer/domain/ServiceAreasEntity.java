package hutbazar.model.customer.domain;

/**
 * Created with IntelliJ IDEA.
 * UserEntity: Shovon
 * Date: 10/26/12
 * Time: 10:10 PM
 * To change this template use File | Settings | File Templates.
 */
public class ServiceAreasEntity {
    private String division;
    private double charge;

    public ServiceAreasEntity(String division, double charge) {
        this.division = division;
        this.charge = charge;
    }

    public String getDivision() {
        return division;
    }

    public void setDivision(String division) {
        this.division = division;
    }

    public double getCharge() {
        return charge;
    }

    public void setCharge(double charge) {
        this.charge = charge;
    }
}

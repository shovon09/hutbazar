package hutbazar.model.customer.domain;

/**
 * Created with IntelliJ IDEA.
 * UserEntity: Shovon
 * Date: 11/7/12
 * Time: 5:40 AM
 * To change this template use File | Settings | File Templates.
 */
public class SearchResultEntity {
    int productId;
    String productName;
    int categoryId;
    int storeId;
    double sellingPrice;

    public SearchResultEntity(int productId, String productName, int categoryId, int storeId, double sellingPrice) {
        this.productId = productId;
        this.productName = productName;
        this.categoryId = categoryId;
        this.storeId = storeId;
        this.sellingPrice = sellingPrice;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public int getStoreId() {
        return storeId;
    }

    public void setStoreId(int storeId) {
        this.storeId = storeId;
    }

    public double getSellingPrice() {
        return sellingPrice;
    }

    public void setSellingPrice(double sellingPrice) {
        this.sellingPrice = sellingPrice;
    }
}

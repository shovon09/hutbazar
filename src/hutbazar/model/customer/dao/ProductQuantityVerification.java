package hutbazar.model.customer.dao;

import hutbazar.util.JdbcConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created with IntelliJ IDEA.
 * UserEntity: Shovon
 * Date: 11/3/12
 * Time: 7:58 PM
 * To change this template use File | Settings | File Templates.
 */
public class ProductQuantityVerification {

    public static boolean verifyProductQuantity(int productId, int productQuantity) {

        Connection connection = JdbcConnection.getConnection();

        String query = null;
        PreparedStatement statement = null;
        try {
            query = "SELECT * FROM `inventory` WHERE `product_id` = ?";

            statement = connection.prepareStatement(query);
            statement.setInt(1, productId);
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();

            int stock = resultSet.getInt("stock");
            int numberOfSell = resultSet.getInt("number_of_sell");

            if (productQuantity > stock) {


            } else {
                stock = stock - productQuantity;
                numberOfSell = numberOfSell + productQuantity;
                query = "UPDATE `inventory` SET `stock`= ? ,`number_of_sell` = ? " + "WHERE `product_id` = ?";

                statement = connection.prepareStatement(query);
                statement.setInt(1, stock);
                statement.setInt(2, numberOfSell);
                statement.setInt(3, productId);
                int rowCount = statement.executeUpdate();
                return true;

            }


            JdbcConnection.close(statement);
        } catch (SQLException e) {
            e.printStackTrace();
            JdbcConnection.close(statement);
        }
        return false;

    }

    public static boolean checkCartIsValid(int productId, int productQuantity) {
        Connection connection = JdbcConnection.getConnection();

        String query = null;
        PreparedStatement statement = null;
        try {
            query = "SELECT * FROM `inventory` WHERE `product_id` = ?";

            statement = connection.prepareStatement(query);
            statement.setInt(1, productId);
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();

            int stock = resultSet.getInt("stock");
            int numberOfSell = resultSet.getInt("number_of_sell");
            if (stock >= productQuantity) {
                return true;
            }

            JdbcConnection.close(statement);
        } catch (SQLException e) {
            e.printStackTrace();
            JdbcConnection.close(statement);
        }

        return false;

    }
}

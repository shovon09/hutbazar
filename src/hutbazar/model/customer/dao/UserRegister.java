package hutbazar.model.customer.dao;

import hutbazar.util.JdbcConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created with IntelliJ IDEA.
 * User: shihab
 * Date: 10/28/12
 * Time: 10:09 PM
 * To change this template use File | Settings | File Templates.
 */
public class UserRegister {

    public static String register(String username, String email, String contactNumber, String address, String division, String password, String totalCredit, String registrationDate, String adminType) {
        Connection connection = JdbcConnection.getConnection();
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String userId = null;


        try {
            String query = "INSERT into customer_info VALUES(?,?,?,?,?,?,?,?,?,?)";
            statement = connection.prepareStatement(query, statement.RETURN_GENERATED_KEYS);
            statement.setString(1, null);
            statement.setString(2, username);
            statement.setString(3, email);
            statement.setString(4, contactNumber);
            statement.setString(5, address);
            statement.setString(6, division);
            statement.setString(7, password);
            statement.setString(8, totalCredit);
            statement.setString(9, registrationDate);
            statement.setString(10, adminType);
            statement.executeUpdate();

            statement = connection.prepareStatement("SELECT * FROM customer_info");
            resultSet = statement.executeQuery();

            while (resultSet.next()) {

                if (email.equals(resultSet.getString("email")) && password.equals(resultSet.getString("password"))) {
                    userId = resultSet.getString("customer_id");
                }

            }
            JdbcConnection.close(statement);


        } catch (SQLException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        System.out.println("Admin id sent to servlet" + userId);
        return userId;
    }

}

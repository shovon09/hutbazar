package hutbazar.model.customer.dao;

import hutbazar.util.JdbcConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Created with IntelliJ IDEA.
 * UserEntity: Shovon
 * Date: 10/31/12
 * Time: 11:01 PM
 * To change this template use File | Settings | File Templates.
 */
public class OrderInsertion {

    public static void insertOrder(String orderId, String customerId, int categoryId, int storeId, int productId, int quantity, double unitPrice,
                                   double total, double averageBoughtPrice, double profit, String deliveryAddress, String shipmentCharge,
                                   String division, String orderDate, String deliveryDate, String orderStatus) {

        Connection connection = JdbcConnection.getConnection();
        PreparedStatement statement = null;


        try {
            String query = "INSERT into ordertable VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            statement = connection.prepareStatement(query, statement.RETURN_GENERATED_KEYS);

            statement.setString(1, orderId);
            statement.setString(2, customerId);
            statement.setInt(3, categoryId);
            statement.setInt(4, storeId);
            statement.setInt(5, productId);
            statement.setInt(6, quantity);
            statement.setDouble(7, unitPrice);
            statement.setDouble(8, total);
            statement.setDouble(9, averageBoughtPrice);
            statement.setDouble(10, profit);
            statement.setString(11, deliveryAddress);
            statement.setString(12, shipmentCharge);
            statement.setString(13, division);
            statement.setString(14, orderDate);
            statement.setString(15, deliveryDate);
            statement.setString(16, orderStatus);

            statement.executeUpdate();
            JdbcConnection.close(statement);


        } catch (SQLException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

    }
}

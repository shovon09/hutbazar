package hutbazar.model.customer.dao;

import hutbazar.model.customer.domain.UserEntity;
import hutbazar.util.JdbcConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * UserEntity: shihab
 * Date: 10/23/12
 * Time: 11:45 PM
 * To change this template use File | Settings | File Templates.
 */
public class LoginCheckService {

    public static List<UserEntity> logincheck(String email, String password) {
        Connection connection = JdbcConnection.getConnection();
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        List<UserEntity> userEntities = new ArrayList<UserEntity>();

        try {


            statement = connection.prepareStatement("SELECT * FROM customer_info");
            resultSet = statement.executeQuery();

            while (resultSet.next()) {

                if (email.equals(resultSet.getString("email")) && password.equals(resultSet.getString("password"))) {

                    userEntities.add(new UserEntity(resultSet.getString("customer_id"), resultSet.getString("customer_name"), resultSet.getString("email"), resultSet.getString("contact_number"), resultSet.getString("address"), resultSet.getString("division"), resultSet.getString("password"), resultSet.getString("total_credit"), resultSet.getString("admin_type")));
                }

            }

            JdbcConnection.close(statement);


        } catch (Exception e) {
            e.printStackTrace();
        }

        return userEntities;

    }
}

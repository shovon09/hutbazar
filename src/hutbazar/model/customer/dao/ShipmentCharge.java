package hutbazar.model.customer.dao;

import hutbazar.util.JdbcConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created with IntelliJ IDEA.
 * UserEntity: Shovon
 * Date: 10/31/12
 * Time: 8:36 PM
 * To change this template use File | Settings | File Templates.
 */
public class ShipmentCharge {

    public static double getShipmentCharge(String division) {
        Connection connection = JdbcConnection.getConnection();
        double charge = 0.00;
        String sql = null;
        PreparedStatement statement = null;
        try {
            sql = "SELECT `charge` FROM `delivery_areas` WHERE `division` = " + division;

            statement = connection.prepareStatement(sql);
            //  statement.setString(1, "dhaka");
            ResultSet resultSet = statement.executeQuery();

            charge = resultSet.getDouble("charge");


            JdbcConnection.close(statement);
        } catch (SQLException e) {
            e.printStackTrace();
            JdbcConnection.close(statement);
        }
        return charge;

    }
}

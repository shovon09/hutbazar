package hutbazar.model.customer.dao;

import hutbazar.model.customer.domain.SearchResultEntity;
import hutbazar.util.JdbcConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * UserEntity: Shovon
 * Date: 11/6/12
 * Time: 10:15 PM
 * To change this template use File | Settings | File Templates.
 */
public class ProductSearch {
    public static List<SearchResultEntity> searchProduct(double lowerPrice, double upperPrice, String productName) {
        Connection connection = JdbcConnection.getConnection();
        List<SearchResultEntity> searchResults = new ArrayList<SearchResultEntity>();
        String query = null;
        PreparedStatement statement = null;
        try {
            if (lowerPrice == 0.0 && upperPrice == 0.00) {
                query = "SELECT product_info.`product_id`, product_info.`product_name`,`category_id`,product_info.`store_id`,inventory.selling_price FROM `product_info` " +
                        "INNER JOIN `inventory` on  product_info.`product_id`= inventory.product_id WHERE product_info.`product_name` LIKE '%" + productName + "%' ";

                statement = connection.prepareStatement(query);


            }
            if (lowerPrice != 0.0 && upperPrice != 0.00) {
                query = "SELECT product_info.`product_id`, product_info.`product_name`,`category_id`,product_info.`store_id`,inventory.selling_price FROM `product_info` " +
                        "INNER JOIN `inventory` on  product_info.`product_id`= inventory.product_id WHERE product_info.`product_name` LIKE '%" + productName + "%' AND inventory.selling_price BETWEEN ? AND ? ";

                statement = connection.prepareStatement(query);
                statement.setDouble(1, lowerPrice);
                statement.setDouble(2, upperPrice);

            }
            if (lowerPrice != 0.0 && upperPrice == 0.00) {
                query = "SELECT product_info.`product_id`, product_info.`product_name`,`category_id`,product_info.`store_id`,inventory.selling_price FROM `product_info` " +
                        "INNER JOIN `inventory` on  product_info.`product_id`= inventory.product_id WHERE product_info.`product_name` LIKE '%" + productName + "%' AND inventory.selling_price >= ? ";

                statement = connection.prepareStatement(query);
                statement.setDouble(1, lowerPrice);

            }
            if (lowerPrice == 0.0 && upperPrice != 0.00) {
                query = "SELECT product_info.`product_id`, product_info.`product_name`,`category_id`,product_info.`store_id`,inventory.selling_price FROM `product_info` " +
                        "INNER JOIN `inventory` on  product_info.`product_id`= inventory.product_id WHERE product_info.`product_name` LIKE '%" + productName + "%' AND inventory.selling_price <= ? ";

                statement = connection.prepareStatement(query);
                statement.setDouble(1, upperPrice);

            }

            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                int productId = resultSet.getInt("product_id");
                String pName = resultSet.getString("product_name");
                int categoryId = resultSet.getInt("category_id");
                int storeId = resultSet.getInt("store_id");
                double sellingPrice = resultSet.getDouble("selling_price");
                System.out.println(productId + pName + categoryId + storeId + sellingPrice);
                searchResults.add(new SearchResultEntity(productId, pName, categoryId, storeId, sellingPrice));


            }

            JdbcConnection.close(statement);
        } catch (SQLException e) {
            e.printStackTrace();
            JdbcConnection.close(statement);
        }

        return searchResults;

    }
}

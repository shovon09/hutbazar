package hutbazar.model.customer.dao;

import hutbazar.model.customer.domain.*;
import hutbazar.util.JdbcConnection;

import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;


/**
 * Created with IntelliJ IDEA.
 * User: Shovon
 * Date: 10/23/12
 * Time: 10:47 AM
 * To change this template use File | Settings | File Templates.
 */
public class CustomerService {

    // Static Method to get the stores ...//

    public static List<StoreEntity> getStores() {
        Connection connection = JdbcConnection.getConnection();
        List<StoreEntity> stores = new ArrayList<StoreEntity>();
        String sql = null;
        PreparedStatement statement = null;
        try {
            sql = "SELECT * FROM store_info";
            statement = connection.prepareStatement(sql);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {

                Integer id = resultSet.getInt("store_id");
                String name = resultSet.getString("store_name");
                String description = resultSet.getString("description");
                String type = resultSet.getString("type");
                Integer admin_id = resultSet.getInt("admin_id");
                Date activation_from = resultSet.getDate("activation_form");
                Date activation_end = resultSet.getDate("activation_end");
                String image = resultSet.getString("image");
                Integer active = resultSet.getInt("active");
                stores.add(new StoreEntity(id, name, description, type, admin_id, activation_from, activation_end, image, active));
            }
            JdbcConnection.close(statement);
        } catch (SQLException e) {
            e.printStackTrace();
            JdbcConnection.close(statement);
        }

        return stores;
    }

    public static List<StoreEntity> getSecondaryStores() {
        Connection connection = JdbcConnection.getConnection();
        List<StoreEntity> stores = new ArrayList<StoreEntity>();
        String sql = null;
        PreparedStatement statement = null;
        try {
            sql = "SELECT * FROM `store_info` WHERE `type` = 'Golden' LIMIT 6";
            statement = connection.prepareStatement(sql);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {

                Integer id = resultSet.getInt("store_id");
                String name = resultSet.getString("store_name");
                String description = resultSet.getString("description");
                String type = resultSet.getString("type");
                Integer admin_id = resultSet.getInt("admin_id");
                Date activation_from = resultSet.getDate("activation_form");
                Date activation_end = resultSet.getDate("activation_end");
                String image = resultSet.getString("image");
                Integer active = resultSet.getInt("active");
                stores.add(new StoreEntity(id, name, description, type, admin_id, activation_from, activation_end, image, active));
            }
            JdbcConnection.close(statement);
        } catch (SQLException e) {
            e.printStackTrace();
            JdbcConnection.close(statement);
        }

        return stores;
    }


    public static List<StoreEntity> getPrimaryStores() {
        Connection connection = JdbcConnection.getConnection();
        List<StoreEntity> stores = new ArrayList<StoreEntity>();
        String sql = null;
        PreparedStatement statement = null;
        try {
            sql = "SELECT * FROM `store_info` WHERE `type` = 'Platinum'";
            statement = connection.prepareStatement(sql);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {

                Integer id = resultSet.getInt("store_id");
                String name = resultSet.getString("store_name");
                String description = resultSet.getString("description");
                String type = resultSet.getString("type");
                Integer admin_id = resultSet.getInt("admin_id");
                Date activation_from = resultSet.getDate("activation_form");
                Date activation_end = resultSet.getDate("activation_end");
                String image = resultSet.getString("image");
                Integer active = resultSet.getInt("active");
                stores.add(new StoreEntity(id, name, description, type, admin_id, activation_from, activation_end, image, active));
            }
            JdbcConnection.close(statement);
        } catch (SQLException e) {
            e.printStackTrace();
            JdbcConnection.close(statement);
        }

        return stores;
    }

    public static List<ProductCategoryEntity> getProductCatagory(String id) {

        Connection connection = JdbcConnection.getConnection();
        List<ProductCategoryEntity> productCategoryEntities = new ArrayList<ProductCategoryEntity>();
        int store_id = Integer.parseInt(id);
        String sql = null;
        PreparedStatement statement = null;
        try {
            sql = "SELECT * FROM `category` WHERE `store_id` = ? ";

            statement = connection.prepareStatement(sql);
            statement.setInt(1, store_id);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Integer catId = resultSet.getInt("category_id");
                String catName = resultSet.getString("category_name");
                String image = resultSet.getString("image");
                Integer stId = resultSet.getInt("store_id");
                Integer parID = resultSet.getInt("parent_category_id");
//              System.out.println(cat_id + " " +cat_name + " ");
                productCategoryEntities.add(new ProductCategoryEntity(catId, catName, image, stId, parID));
            }
            JdbcConnection.close(statement);
        } catch (SQLException e) {
            e.printStackTrace();
            JdbcConnection.close(statement);
        }
        return productCategoryEntities;


    }

    public static List<ProductCategoryEntity> getChildCategory(String catId) {

        Connection connection = JdbcConnection.getConnection();
        List<ProductCategoryEntity> productCategoryEntities = new ArrayList<ProductCategoryEntity>();
        int categoryId = Integer.parseInt(catId);
        String sql = null;
        PreparedStatement statement = null;
        try {
            sql = "SELECT * FROM `category` WHERE `parent_category_id` = ? ";

            statement = connection.prepareStatement(sql);
            statement.setInt(1, categoryId);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Integer cateId = resultSet.getInt("category_id");
                String catName = resultSet.getString("category_name");
                String image = resultSet.getString("image");
                Integer stId = resultSet.getInt("store_id");
                Integer parID = resultSet.getInt("parent_category_id");
//              System.out.println(cat_id + " " +cat_name + " ");
                productCategoryEntities.add(new ProductCategoryEntity(cateId, catName, image, stId, parID));
            }
            JdbcConnection.close(statement);
        } catch (SQLException e) {
            e.printStackTrace();
            JdbcConnection.close(statement);
        }
        return productCategoryEntities;


    }

    public static List<ProductEntity> getProducts(String catId) {

        Connection connection = JdbcConnection.getConnection();
        List<ProductEntity> productEntities = new ArrayList<ProductEntity>();
        int categoryId = Integer.parseInt(catId);
        String sql = null;
        PreparedStatement statement = null;
        try {
            sql = "SELECT * FROM `product_info` WHERE `category_id` = ? ";

            statement = connection.prepareStatement(sql);
            statement.setInt(1, categoryId);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Integer productID = resultSet.getInt("product_id");
                String productName = resultSet.getString("product_name");
                Integer cateId = resultSet.getInt("category_id");
                Integer storeId = resultSet.getInt("store_id");
                String description = resultSet.getString("description");
                String image = resultSet.getString("image");
                Date dateAdded = resultSet.getDate("date_added");
                Double discount = resultSet.getDouble("discount");
                Date promotionFrom = resultSet.getDate("promotion_from");
                Date promotionEnd = resultSet.getDate("promotion_end");
                Integer promotionActive = resultSet.getInt("active");
                productEntities.add(new ProductEntity(productID, productName, cateId, storeId, description, image, dateAdded, discount, promotionFrom, promotionEnd, promotionActive));


            }
            JdbcConnection.close(statement);
        } catch (SQLException e) {
            e.printStackTrace();
            JdbcConnection.close(statement);
        }
        return productEntities;
    }

    public static List<ProductEntity> getAllProducts() {

        Connection connection = JdbcConnection.getConnection();
        List<ProductEntity> productEntities = new ArrayList<ProductEntity>();
        String sql = null;
        PreparedStatement statement = null;
        try {
            sql = "SELECT * FROM `product_info` ";

            statement = connection.prepareStatement(sql);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Integer productID = resultSet.getInt("product_id");
                String productName = resultSet.getString("product_name");
                Integer cateId = resultSet.getInt("category_id");
                Integer storeId = resultSet.getInt("store_id");
                String description = resultSet.getString("description");
                String image = resultSet.getString("image");
                Date dateAdded = resultSet.getDate("date_added");
                Double discount = resultSet.getDouble("discount");
                Date promotionFrom = resultSet.getDate("promotion_from");
                Date promotionEnd = resultSet.getDate("promotion_end");
                Integer promotionActive = resultSet.getInt("active");
                productEntities.add(new ProductEntity(productID, productName, cateId, storeId, description, image, dateAdded, discount, promotionFrom, promotionEnd, promotionActive));


            }
            JdbcConnection.close(statement);
        } catch (SQLException e) {
            e.printStackTrace();
            JdbcConnection.close(statement);
        }
        return productEntities;
    }

    public static List<ProductEntity> getProductsWithPromotion() {

        Connection connection = JdbcConnection.getConnection();
        List<ProductEntity> productEntities = new ArrayList<ProductEntity>();
        String sql = null;
        PreparedStatement statement = null;
        try {
            sql = "SELECT * FROM `product_info` WHERE `active` = 1";

            statement = connection.prepareStatement(sql);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Integer productID = resultSet.getInt("product_id");
                String productName = resultSet.getString("product_name");
                Integer cateId = resultSet.getInt("category_id");
                Integer storeId = resultSet.getInt("store_id");
                String description = resultSet.getString("description");
                String image = resultSet.getString("image");
                Date dateAdded = resultSet.getDate("date_added");
                Double discount = resultSet.getDouble("discount");
                Date promotionFrom = resultSet.getDate("promotion_from");
                Date promotionEnd = resultSet.getDate("promotion_end");
                Integer promotionActive = resultSet.getInt("active");
                productEntities.add(new ProductEntity(productID, productName, cateId, storeId, description, image, dateAdded, discount, promotionFrom, promotionEnd, promotionActive));


            }
            JdbcConnection.close(statement);
        } catch (SQLException e) {
            e.printStackTrace();
            JdbcConnection.close(statement);
        }
        return productEntities;
    }

    public static ProductEntity getProductDetails(String pId) {

        Connection connection = JdbcConnection.getConnection();
        ProductEntity product = null;
        int productId = Integer.parseInt(pId);
        String sql = null;
        PreparedStatement statement = null;
        try {
            sql = "SELECT * FROM `product_info` WHERE `product_id` = ? ";

            statement = connection.prepareStatement(sql);
            statement.setInt(1, productId);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Integer productID = resultSet.getInt("product_id");
                String productName = resultSet.getString("product_name");
                Integer cateId = resultSet.getInt("category_id");
                Integer storeId = resultSet.getInt("store_id");
                String description = resultSet.getString("description");
                String image = resultSet.getString("image");
                Date dateAdded = resultSet.getDate("date_added");
                Double discount = resultSet.getDouble("discount");
                Date promotionFrom = resultSet.getDate("promotion_from");
                Date promotionEnd = resultSet.getDate("promotion_end");
                Integer promotionActive = resultSet.getInt("active");
                product = new ProductEntity(productID, productName, cateId, storeId, description, image, dateAdded, discount, promotionFrom, promotionEnd, promotionActive);


            }
            JdbcConnection.close(statement);
        } catch (SQLException e) {
            e.printStackTrace();
            JdbcConnection.close(statement);
        }
        return product;
    }

    public static InventoryEntity getInventoryDetails(String pId) {

        Connection connection = JdbcConnection.getConnection();
        InventoryEntity inventoryDetails = null;
        int productId = Integer.parseInt(pId);
        String sql = null;
        PreparedStatement statement = null;
        try {
            sql = "SELECT * FROM `inventory` WHERE `product_id` = ? ";

            statement = connection.prepareStatement(sql);
            statement.setInt(1, productId);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {

                Integer productID = resultSet.getInt("product_id");
                String productName = resultSet.getString("product_name");
                Double avgBoughtPrice = resultSet.getDouble("avg_bought_price");
                Integer storeId = resultSet.getInt("store_id");
                Integer stock = resultSet.getInt("stock");
                Integer threshold = resultSet.getInt("threshold");
                Double sellingPrice = resultSet.getDouble("selling_price");
                Integer numberOfSell = resultSet.getInt("number_of_sell");
                inventoryDetails = new InventoryEntity(productID, productName, avgBoughtPrice, storeId, stock, threshold, sellingPrice, numberOfSell);


            }
            JdbcConnection.close(statement);
        } catch (SQLException e) {
            e.printStackTrace();
            JdbcConnection.close(statement);
        }
        return inventoryDetails;
    }

    public static List<ServiceAreasEntity> getServiceAreas() {
        Connection connection = JdbcConnection.getConnection();
        List<ServiceAreasEntity> serviceAreasEntityList = new ArrayList<ServiceAreasEntity>();

        String sql = null;
        PreparedStatement statement = null;
        try {
            sql = "SELECT * FROM `delivery_areas`";

            statement = connection.prepareStatement(sql);

            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                String division = resultSet.getString("division");
                double charge = resultSet.getDouble("charge");
                serviceAreasEntityList.add(new ServiceAreasEntity(division, charge));

            }
            JdbcConnection.close(statement);
        } catch (SQLException e) {
            e.printStackTrace();
            JdbcConnection.close(statement);
        }
        return serviceAreasEntityList;

    }

    public static boolean addToCart(String sessionID, String userId, String categoryId, String storeId, String productId, String quantity, double unitPrice, double total, String avgBoughtPrice, double profit) {
        Connection connection = JdbcConnection.getConnection();
        PreparedStatement statement = null;

        if (userId.equals("")) {
            userId = null;
        }


        try {
            String query = "INSERT into cart VALUES(?,?,?,?,?,?,?,?,?,?)";
            statement = connection.prepareStatement(query, statement.RETURN_GENERATED_KEYS);
            statement.setString(1, sessionID);
            statement.setString(2, userId);
            statement.setString(3, categoryId);
            statement.setString(4, storeId);
            statement.setString(5, productId);
            statement.setString(6, quantity);
            statement.setDouble(7, unitPrice);
            statement.setDouble(8, total);
            statement.setString(9, avgBoughtPrice);
            statement.setDouble(10, profit);
            statement.executeUpdate();
            JdbcConnection.close(statement);


        } catch (SQLException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        return true;

    }

    public static void ownAStore(String storeName, String storeDescription, String storeType, String storeImage, String adminId) {
        Connection connection = JdbcConnection.getConnection();
        PreparedStatement statement = null;

        try {
            String query = "INSERT into store_info VALUES(?,?,?,?,?,?,?,?,?)";
            statement = connection.prepareStatement(query, statement.RETURN_GENERATED_KEYS);
            System.out.print(adminId);
            statement.setString(1, null);
            statement.setString(2, storeName);
            statement.setString(3, storeDescription);
            statement.setString(4, storeType);
            statement.setString(5, adminId);
            statement.setString(6, null);
            statement.setString(7, null);
            statement.setString(8, storeImage);
            statement.setString(9, "0");

            statement.executeUpdate();
            JdbcConnection.close(statement);


        } catch (SQLException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }


    }

    public static void receiveFeedback(String userName, String userEmail, String userFeedback) {
        Connection connection = JdbcConnection.getConnection();
        PreparedStatement statement = null;

        try {
            String query = "INSERT into feedback VALUES(?,?,?,?,?)";
            statement = connection.prepareStatement(query, statement.RETURN_GENERATED_KEYS);

            statement.setString(1, null);
            statement.setString(2, userFeedback);
            statement.setString(3, "0");
            statement.setString(4, userName);
            statement.setString(5, userEmail);


            statement.executeUpdate();
            JdbcConnection.close(statement);


        } catch (SQLException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }


    public static void receiveWishlist(String userName, String userEmail, String userProduct, String userProductDescription, String storeId) {
        Connection connection = JdbcConnection.getConnection();
        PreparedStatement statement = null;

        try {
            String query = "INSERT into wishlist VALUES(?,?,?,?,?,?)";
            statement = connection.prepareStatement(query, statement.RETURN_GENERATED_KEYS);

            statement.setString(1, userProduct);
            statement.setString(2, userName);
            statement.setString(3, userEmail);
            statement.setString(4, userProductDescription);
            statement.setString(5, storeId);
            statement.setString(6, "0");


            statement.executeUpdate();
            JdbcConnection.close(statement);


        } catch (SQLException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }


    public static void customerSupport(String storeId, String categoryId, String productId, String comment,String email) {
        Connection connection = JdbcConnection.getConnection();
        PreparedStatement statement = null;

        try {
            String query = "INSERT into support VALUES(?,?,?,?,?,?)";
            statement = connection.prepareStatement(query, statement.RETURN_GENERATED_KEYS);

            statement.setString(1, storeId);
            statement.setString(2, categoryId);
            statement.setString(3, productId);
            statement.setString(4, comment);
            statement.setString(5, "0");
            statement.setString(6,email);


            statement.executeUpdate();
            JdbcConnection.close(statement);


        } catch (SQLException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public static void updateUser(String userId, String userName, String userEmail, String userContactNumber, String userAddress, String userDivision, String userPassword) {
        Connection connection = JdbcConnection.getConnection();
        PreparedStatement statement = null;

        try {
            System.out.println(userPassword);
            String sql = "UPDATE customer_info set customer_name = '" + userName + "' , email = '" + userEmail + "', contact_number = '" + userContactNumber + "', address = '" + userAddress + "', division = '" + userDivision + "', password = '" + userPassword + "'   where customer_id = " + userId;
            statement = connection.prepareStatement(sql, statement.RETURN_GENERATED_KEYS);
            System.out.println(sql);
            statement.executeUpdate();
            JdbcConnection.close(statement);


        } catch (SQLException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }


    public static List<OrderListEntity> getOrderList(String userId)
    {
        Connection connection = JdbcConnection.getConnection();
        List<OrderListEntity> orderList = new ArrayList<OrderListEntity>();

        String sql = null;
        PreparedStatement statement = null;
        try
        {
            sql = "SELECT order_id, COUNT( DISTINCT product_id ) AS total_products, SUM( quantity ) AS total_quantity, order_date, delivary_date,order_status FROM ordertable  WHERE customer_id=\"" +userId+ " \" GROUP BY order_id  ";
            System.out.print(sql);
            statement = connection.prepareStatement(sql);

            ResultSet resultSet = statement.executeQuery();
            int i=1;
            while (resultSet.next())
            {

                String isDelivered;
                String order_id = resultSet.getString("order_id");
                String totalNumberOfProducts = String.valueOf(resultSet.getInt("total_products"));
                String totalQuantity = String.valueOf(resultSet.getInt("total_quantity"));
                String order_date = resultSet.getDate("order_date").toString();
                String delivary_date = resultSet.getDate("delivary_date").toString();
                String order_status = resultSet.getString("order_status");

                java.util.Date orderDate = null;
                try {
                    orderDate = new SimpleDateFormat("yyyy-MM-dd").parse(order_date);
                } catch (ParseException e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
                java.util.Date delivaryDate =  new SimpleDateFormat("yyyy-MM-dd").parse(delivary_date);
                java.util.Date today = new java.util.Date();
                int diffInDays = (int) ((today.getTime() - orderDate.getTime())/(1000 * 60 * 60 * 24));



                String order_number= Integer.toString(i);
                /*System.out.println("Order Number" + order_number);
               System.out.println("Order id" + order_id);
               System.out.println("Order products" + totalNumberOfProducts);
               System.out.println("Order quantity" + totalQuantity);
               System.out.println("Order Date" + order_date);
               System.out.println("Delivary Date" + delivary_date);
               System.out.println(today);
               System.out.println(orderDate); */


                if(diffInDays == 1 || diffInDays==0)
                {
                    isDelivered=null;


                }

                else
                {
                    isDelivered="no";

                }



                orderList.add(new OrderListEntity(order_number,order_id,totalNumberOfProducts,totalQuantity,order_date,delivary_date,order_status,isDelivered));
                i++;
            }
            JdbcConnection.close(statement);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            JdbcConnection.close(statement);
        }
        catch (ParseException e)
        {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return orderList;
    }
    public static List<OrderDetailsEntity> getOrderDetails(String orderID) {
        Connection connection = JdbcConnection.getConnection();
        List<OrderDetailsEntity> orderDetails = new ArrayList<OrderDetailsEntity>();
        System.out.print(orderID);
        String sql = null;
        PreparedStatement statement = null;
        try {
            sql = "SELECT order_id AS orderId, store_name AS storeName,product_name AS productName,quantity AS productQuantity,unit_price as productUnitPrice,shipment_charge AS shipmentCharge,total_selling_price as total,order_date as orderDate,delivary_date as delivaryDate,delivery_address as delivaryAddress,order_status as orderStatus\n" +
                    "FROM product_info\n" +
                    "JOIN store_info\n" +
                    "JOIN ordertable\n" +
                    "WHERE product_info.store_id = store_info.store_id\n" +
                    "AND product_info.product_id = ordertable.product_id\n" +
                    "AND ordertable.order_id = ? ";

            statement = connection.prepareStatement(sql);
            statement.setString(1, orderID);

            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {

                String orderId = resultSet.getString("orderId");
                String storeName = resultSet.getString("storeName");
                String productName = resultSet.getString("productName");
                String productQuantity = resultSet.getString("productQuantity");
                String productUnitPrice = resultSet.getString("productUnitPrice");
                String shipmentCharge = resultSet.getString("shipmentCharge");
                String total = resultSet.getString("total");
                String orderDate = resultSet.getString("orderDate");
                String delivaryDate = resultSet.getString("delivaryDate");
                String delivaryAddress = resultSet.getString("delivaryAddress");
                String orderStatus = resultSet.getString("orderStatus");


                System.out.println("OrderID" + orderId);

                orderDetails.add(new OrderDetailsEntity(orderId, storeName, productName, productQuantity, productUnitPrice, shipmentCharge, total, orderDate, delivaryDate, delivaryAddress, orderStatus));

            }
            JdbcConnection.close(statement);
        } catch (SQLException e) {
            e.printStackTrace();
            JdbcConnection.close(statement);
        }

        return orderDetails;
    }


    public static void cancelOrder(String orderID) {

        Connection connection = JdbcConnection.getConnection();
        List<OrderDetailsEntity> orderDetails = new ArrayList<OrderDetailsEntity>();
        System.out.print("OrderID to update" + orderID);
        String sql = null;
        PreparedStatement statement = null;
        try {
            sql = "SELECT order_id AS orderId, store_name AS storeName,product_name AS productName,quantity AS productQuantity,unit_price as productUnitPrice,shipment_charge AS shipmentCharge,total_selling_price as total,order_date as orderDate,delivary_date as delivaryDate,delivery_address as delivaryAddress,order_status as orderStatus\n" +
                    "FROM product_info\n" +
                    "JOIN store_info\n" +
                    "JOIN ordertable\n" +
                    "WHERE product_info.store_id = store_info.store_id\n" +
                    "AND product_info.product_id = ordertable.product_id\n" +
                    "AND ordertable.order_id = ? ";

            statement = connection.prepareStatement(sql);
            statement.setString(1, orderID);

            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {

                String orderId = resultSet.getString("orderId");
                String storeName = resultSet.getString("storeName");
                String productName = resultSet.getString("productName");
                String productQuantity = resultSet.getString("productQuantity");
                String productUnitPrice = resultSet.getString("productUnitPrice");
                String shipmentCharge = resultSet.getString("shipmentCharge");
                String total = resultSet.getString("total");
                String orderDate = resultSet.getString("orderDate");
                String delivaryDate = resultSet.getString("delivaryDate");
                String delivaryAddress = resultSet.getString("delivaryAddress");
                String orderStatus = resultSet.getString("orderStatus");


                System.out.println("OrderID" + orderId);

                orderDetails.add(new OrderDetailsEntity(orderId, storeName, productName, productQuantity, productUnitPrice, shipmentCharge, total, orderDate, delivaryDate, delivaryAddress, orderStatus));

            }


            for (int i = 0; i < orderDetails.size(); i++) {
                try {
                    sql = null;
                    statement = null;
                    String productName = orderDetails.get(i).getProductName();

                    int productQuantity = Integer.parseInt(orderDetails.get(i).getProductQuantity());
                    sql = "UPDATE `inventory` SET number_of_sell = number_of_sell -" + productQuantity + " WHERE `product_name` = \"" + productName + "\"";
                    // System.out.print(sql);
                    statement = connection.prepareStatement(sql);
                    statement.executeUpdate();


                    sql = "UPDATE `inventory` SET stock = stock +" + productQuantity + " WHERE `product_name`=\"" + productName + "\"";
                    statement = connection.prepareStatement(sql);
                    statement.executeUpdate();


                    String productId = null;
                    sql = "SELECT * FROM `product_info` WHERE `product_name`=\"" + productName + "\"";
                    statement = connection.prepareStatement(sql);

                    ResultSet resultset = statement.executeQuery();
                    while (resultset.next()) {
                        productId = resultset.getString("product_id");
                    }
                    System.out.println(sql);


                    sql = "DELETE FROM `ordertable` WHERE `order_id`=\"" + orderID + "\" AND  `product_id`= \"" + productId + "\"";
                    System.out.println(sql);
                    statement = connection.prepareStatement(sql);
                    statement.executeUpdate();


                } catch (SQLException e) {
                    e.printStackTrace();

                }

            }


        } catch (SQLException e) {
            e.printStackTrace();
            JdbcConnection.close(statement);
        }


    }


    public static String getStoreId(String userID) {

        Connection connection = JdbcConnection.getConnection();
        String sql = null;
        PreparedStatement statement = null;
        String storeId = null;
        try {
            sql = "SELECT * FROM `store_info` WHERE `admin_id` = ? ";

            statement = connection.prepareStatement(sql);
            statement.setString(1, userID);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {

                storeId = resultSet.getString("store_id");


            }
            JdbcConnection.close(statement);
        } catch (SQLException e) {
            e.printStackTrace();
            JdbcConnection.close(statement);
        }
        return storeId;
    }
}



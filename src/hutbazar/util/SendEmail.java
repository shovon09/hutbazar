package hutbazar.util; /**
 * Created with IntelliJ IDEA.
 * User: Shovon
 * Date: 11/13/12
 * Time: 11:08 AM
 * To change this template use File | Settings | File Templates.
 */

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

public class SendEmail {

    public static void sendEmail(String customerEmail, String subject, String content) throws MessagingException {

        String host = "smtp.gmail.com";
        String from = "imshovon@gmail.com";
        String pass = "sh1094009";
        Properties props = System.getProperties();
        props.put("mail.smtp.starttls.enable", "true"); // added this line
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.user", from);
        props.put("mail.smtp.password", pass);
        props.put("mail.smtp.port", "587");
        props.put("mail.smtp.auth", "true");

        String[] to = {customerEmail}; // added this line

        Session session = Session.getDefaultInstance(props, null);
        MimeMessage message = new MimeMessage(session);
        message.setFrom(new InternetAddress(from));

        InternetAddress[] toAddress = new InternetAddress[to.length];

        // To get the array of addresses
        for (int i = 0; i < to.length; i++) { // changed from a while loop
            toAddress[i] = new InternetAddress(to[i]);
        }


        for (int i = 0; i < toAddress.length; i++) { // changed from a while loop
            message.addRecipient(Message.RecipientType.TO, toAddress[i]);
        }
        message.setSubject(subject);

        message.setContent("<h1>Thank you For Let us Know.</h1>"
                + "</br>" + content,
                "text/html");

        Transport transport = session.getTransport("smtp");
        transport.connect(host, from, pass);
        System.out.println("Trying to send email");
        transport.sendMessage(message, message.getAllRecipients());
        transport.close();
        System.out.println("done");

    }
}

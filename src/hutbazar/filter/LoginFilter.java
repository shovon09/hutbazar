package hutbazar.filter;

import hutbazar.model.customer.domain.UserEntity;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * UserEntity: Shovon
 * Date: 11/7/12
 * Time: 10:59 PM
 * To change this template use File | Settings | File Templates.
 */
@WebFilter(urlPatterns = "/admin/*")
public class LoginFilter implements Filter {
    public void destroy() {
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {

        String userAdminType = (String) ((HttpServletRequest) req).getSession(true).getAttribute("userAdminType");
//        HttpSession usersession =  ((HttpServletRequest) req).getSession(true);
//        String userAdminType = (String) usersession.getAttribute("userAdminType");
        if (userAdminType == null || !userAdminType.equals(UserEntity.ADMIN)) {
            ((HttpServletResponse) resp).sendRedirect(((HttpServletRequest) req).getContextPath() + "/login");
            return;
        }

        chain.doFilter(req, resp);
    }

    public void init(FilterConfig config) throws ServletException {

    }

}

package hutbazar.controller.customer;

import hutbazar.model.customer.dao.CustomerService;
import hutbazar.model.customer.domain.ServiceAreasEntity;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * UserEntity: Shovon
 * Date: 10/26/12
 * Time: 10:02 PM
 * To change this template use File | Settings | File Templates.
 */
@WebServlet("/serviceAreas")
public class ServiceAreas extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<ServiceAreasEntity> serviceAreas = CustomerService.getServiceAreas();
        request.setAttribute("serviceAreas", serviceAreas);
        request.getRequestDispatcher("CustomerView/ServiceAreas.jsp").forward(request, response);
    }
}

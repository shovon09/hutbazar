package hutbazar.controller.customer;

import hutbazar.model.customer.dao.CustomerService;
import hutbazar.model.customer.domain.InventoryEntity;
import hutbazar.model.customer.domain.ProductEntity;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * UserEntity: Shovon
 * Date: 10/25/12
 * Time: 11:46 PM
 * To change this template use File | Settings | File Templates.
 */
@WebServlet("/productDetails")
public class ProductDetailsView extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String productId = request.getParameter("product_id");
        String categoryId = request.getParameter("category_id");
        ProductEntity productDetails = CustomerService.getProductDetails(productId);
        InventoryEntity inventoryDetails = CustomerService.getInventoryDetails(productId);
        request.setAttribute("categoryId", categoryId);
        request.setAttribute("product", productDetails);
        request.setAttribute("inventory", inventoryDetails);
        request.getRequestDispatcher("CustomerView/ProductDetailsView.jsp").forward(request, response);

    }
}

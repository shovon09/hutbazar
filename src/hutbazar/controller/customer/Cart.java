package hutbazar.controller.customer;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * UserEntity: Shovon
 * Date: 10/29/12
 * Time: 9:43 PM
 * To change this template use File | Settings | File Templates.
 */
public class Cart {

    List<CartEntry> cartEntries = new ArrayList<CartEntry>();
    private int totalProduct = 0;
    private double sumTotal = 0.00;


    public void addCartItem(CartEntry cartEntry) {
        cartEntries.add(cartEntry);
    }

    public CartEntry getItem(int productId) {
        // eikhane check korbo je kon cartentry retutn korbo ///
//        int productId = cartEntry.getProductId();
        for (CartEntry cartItem : cartEntries) {
            if (cartItem.getProductId() == productId) {
                return cartItem;
            }

        }
        return null;
    }

    public List<CartEntry> getCartEntries() {

        return cartEntries;
    }

    public void deleteItem(int productId) {

        for (int i = cartEntries.size() - 1; i >= 0; i--) {
            CartEntry cartItem = cartEntries.get(i);
            if (cartItem.getProductId() == productId) {
                cartEntries.remove(i);
            }
        }


    }

    public int getTotalProduct() {
        totalProduct = 0;
        for (CartEntry cartEntry : cartEntries) {
            totalProduct = cartEntry.getQuantity() + totalProduct;
        }
        return totalProduct;
    }

    public double getSumTotal() {
        sumTotal = 0.00;
        for (CartEntry cartEntry : cartEntries) {
            sumTotal = cartEntry.getTotal() + sumTotal;
        }
        return sumTotal;
    }
}

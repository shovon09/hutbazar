package hutbazar.controller.customer;

import hutbazar.model.customer.dao.CustomerService;
import hutbazar.model.customer.domain.ProductEntity;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Shovon
 * Date: 11/12/12
 * Time: 8:14 PM
 * To change this template use File | Settings | File Templates.
 */
@WebServlet("/productWithPromotion")
public class ProductWithPromotion extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<ProductEntity> products = CustomerService.getProductsWithPromotion();
        request.setAttribute("product", products);
        request.getRequestDispatcher("CustomerView/ProductWithPromotion.jsp").forward(request, response);
    }
}

package hutbazar.controller.customer;

import hutbazar.model.customer.dao.CustomerService;
import hutbazar.model.customer.dao.UserRegister;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created with IntelliJ IDEA.
 * User: shihab
 * Date: 10/30/12
 * Time: 12:52 AM
 * To change this template use File | Settings | File Templates.
 */
@WebServlet("/ownAStore")
@MultipartConfig
public class OwnAStore extends HttpServlet {
    private final static Logger LOGGER = Logger.getLogger(OwnAStore.class.getCanonicalName());

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //insert store admin information in database
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String email = request.getParameter("email");
        String contactNo = request.getParameter("contactNumber");
        String address = request.getParameter("address");
        String division = request.getParameter("division");
        String totalCredit = "0";
        String adminType = "1";
        Date currentDate = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        String registrationDate = dateFormat.format(currentDate).toString();

        response.setContentType("text/html;charset=UTF-8");

        // Create path components to save the file
        final String realativePath = "/Images";
        final String path = getServletContext().getRealPath(realativePath);
        final Part filePart = request.getPart("file");
        final String fileName = getFileName(filePart);

        OutputStream out = null;
        InputStream filecontent = null;
        final PrintWriter writer = response.getWriter();

        try {
            out = new FileOutputStream(new File(path + File.separator + fileName));
            filecontent = filePart.getInputStream();

            int read = 0;
            final byte[] bytes = new byte[1024];

            while ((read = filecontent.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }

            LOGGER.log(Level.INFO, "File{0}being uploaded to {1}",
                    new Object[]{fileName, path});

        } catch (FileNotFoundException fne) {
            writer.println("You either did not specify a file to upload or are " +
                    "trying to upload a file to a protected or nonexistent location.");
            writer.println("<br/> ERROR: " + fne.getMessage());

            LOGGER.log(Level.SEVERE, "Problems during file upload. Error: {0}",
                    new Object[]{fne.getMessage()});
        } finally {
            if (out != null) {
                out.close();
            }
            if (filecontent != null) {
                filecontent.close();
            }
            if (writer != null) {
                writer.close();
            }
        }


        String adminId = UserRegister.register(username, email, contactNo, address, division, password, totalCredit, registrationDate, adminType);

        //insert store information in the database

        String storeName = request.getParameter("storeName");
        String storeDescription = request.getParameter("storeDescription");
        String storeType = request.getParameter("storeType");
        String storeImage = "." + realativePath + "/" + fileName;


        CustomerService.ownAStore(storeName, storeDescription, storeType, storeImage, adminId);
        System.out.println("kutta");
        request.setAttribute("success", "1");
        System.out.println("balser");
        response.sendRedirect("login");

    }


    private String getFileName(final Part part) {
        final String partHeader = part.getHeader("content-disposition");
        LOGGER.log(Level.INFO, "Part Header = {0}", partHeader);
        for (String content : part.getHeader("content-disposition").split(";")) {
            if (content.trim().startsWith("filename")) {
                return content.substring(
                        content.indexOf('=') + 1).trim().replace("\"", "");
            }
        }
        return null;
    }
}

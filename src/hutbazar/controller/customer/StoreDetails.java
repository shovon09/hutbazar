package hutbazar.controller.customer;

import hutbazar.model.customer.dao.CustomerService;
import hutbazar.model.customer.domain.ProductCategoryEntity;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * UserEntity: Shovon
 * Date: 10/23/12
 * Time: 9:09 PM
 * To change this template use File | Settings | File Templates.
 */
@WebServlet("/storeCatagory")
public class StoreDetails extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String store_id = request.getParameter("store_id");

        List<ProductCategoryEntity> productCategoryEntity = CustomerService.getProductCatagory(store_id);

        request.setAttribute("categories", productCategoryEntity);
        request.getRequestDispatcher("CustomerView/StoreDetailsView.jsp").forward(request, response);


    }
}

package hutbazar.controller.customer;

import hutbazar.model.customer.dao.OrderInsertion;
import hutbazar.model.customer.dao.ProductQuantityVerification;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Created with IntelliJ IDEA.
 * UserEntity: Shovon
 * Date: 10/31/12
 * Time: 1:37 PM
 * To change this template use File | Settings | File Templates.
 */
@WebServlet("/checkOut")
public class CheckOut extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String deliveryDivisions = request.getParameter("deliveryDivision");
        String[] chargeAndDiv = deliveryDivisions.split(",");
        String charge = chargeAndDiv[0];
        System.out.println(charge);
        String division = chargeAndDiv[1];
        System.out.println(division);

        String address = request.getParameter("address");
        Date today = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String orderDate = dateFormat.format(today).toString();
        Date dayAfterTomorrow = new Date(System.currentTimeMillis() + 86400 * 1000 * 2);
        String deliveryDate = dateFormat.format(dayAfterTomorrow).toString();
        String orderStatus = "pending";

        String orderId = UUID.randomUUID().toString(); // unique order id for each order ...//
        System.out.println(orderId);

        HttpSession userSession = request.getSession(true);
        Cart cart = (Cart) userSession.getAttribute("cart");
        ArrayList<Integer> productWithInvalidQuantity = new ArrayList<Integer>();

        List<CartEntry> cartEntries = cart.getCartEntries();

        // check whether any items in the cart has the quantity greater than the stock of the product .. //
        boolean purchaseIsPossible = true;
        int loop = 0;
        for (CartEntry cartItem : cartEntries) {

            int productId = cartItem.getProductId();
            int quantity = cartItem.getQuantity();

            boolean productQuantityVerificationIsOk = ProductQuantityVerification.checkCartIsValid(productId, quantity);

            if (!productQuantityVerificationIsOk) {
                purchaseIsPossible = false;
                productWithInvalidQuantity.add(productId);
                System.out.println("Failed");

            }


        }
        if (purchaseIsPossible) {

            for (CartEntry cartItem : cartEntries) {

                String customerId = (String) userSession.getAttribute("userId");
                int categoryId = cartItem.getCategoryId();
                int storeId = cartItem.getStoreId();
                int productId = cartItem.getProductId();
                int quantity = cartItem.getQuantity();
                double unitPrice = cartItem.getUnitPriceAfterPromotion();
                double total = cartItem.getTotal();
                double averageBoughtPrice = cartItem.getAverageBoughtPrice();
                double profit = cartItem.getProfit();

                boolean purchaseIsOk = ProductQuantityVerification.verifyProductQuantity(productId, quantity);
                OrderInsertion.insertOrder(orderId, customerId, categoryId, storeId, productId,
                        quantity, unitPrice, total, averageBoughtPrice, profit, address, charge, division, orderDate, deliveryDate, orderStatus);


            }

            request.setAttribute("invoice", cartEntries);
            request.setAttribute("orderDate", orderDate);
            request.setAttribute("deliveryDate", deliveryDate);
            request.setAttribute("address", address);
            Double sumTotal = (Double) userSession.getAttribute("sumTotal");
            request.setAttribute("total", sumTotal);
            userSession.setAttribute("cart", null);
            userSession.setAttribute("sumTotal", null);
            userSession.setAttribute("cartItem", null);
            System.out.println("Success");

            request.getRequestDispatcher("CustomerView/PurchaseSuccessPage.jsp").forward(request, response);


        } else {// purchase is failed redirect to the cart page and show the error message

            double sumTotal = 0.00;

            for (CartEntry cartItem : cartEntries) {
                sumTotal = sumTotal + cartItem.getTotal();
            }
            request.setAttribute("sumTotal", sumTotal);
            request.setAttribute("carts", cartEntries);
            userSession.setAttribute("productIdWithInvalidQuantity", productWithInvalidQuantity);
            request.getRequestDispatcher("CustomerView/CartDetails.jsp").forward(request, response);


        }


    }


}

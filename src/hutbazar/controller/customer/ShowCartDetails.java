package hutbazar.controller.customer;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * UserEntity: Shovon
 * Date: 10/31/12
 * Time: 10:29 AM
 * To change this template use File | Settings | File Templates.
 */
@WebServlet("/showCartDetails")
public class ShowCartDetails extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession userSession = request.getSession(true);

        Cart cart;

        cart = (Cart) userSession.getAttribute("cart");

        List<CartEntry> cartEntries = cart.getCartEntries();
        double sumTotal = 0.00;
        for (CartEntry cartItem : cartEntries) {
            sumTotal = sumTotal + cartItem.getTotal();
        }
        request.setAttribute("sumTotal", sumTotal);
        request.setAttribute("carts", cartEntries);
        request.getRequestDispatcher("CustomerView/CartDetails.jsp").forward(request, response);
    }
}

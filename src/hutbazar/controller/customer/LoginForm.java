package hutbazar.controller.customer;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * UserEntity: Shovon
 * Date: 10/22/12
 * Time: 3:12 PM
 * To change this template use File | Settings | File Templates.
 */
@WebServlet("/login")
public class LoginForm extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("CustomerView/LoginView.jsp").forward(request, response);
    }
}

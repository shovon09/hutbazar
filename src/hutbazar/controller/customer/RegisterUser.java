package hutbazar.controller.customer;

import hutbazar.model.customer.dao.UserRegister;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: shihab
 * Date: 10/28/12
 * Time: 8:40 PM
 * To change this template use File | Settings | File Templates.
 */
@WebServlet("/registeruser")
public class RegisterUser extends HttpServlet {


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String email = request.getParameter("email");
        String contactNo = request.getParameter("contactNumber");
        String address = request.getParameter("address");
        String division = request.getParameter("division");
        String totalCredit = "0";
        String adminType = "0";

        Date currentDate = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        String registrationDate = dateFormat.format(currentDate).toString();

        UserRegister.register(username, email, contactNo, address, division, password, totalCredit, registrationDate, adminType);
        request.setAttribute("success", "1");
        request.getRequestDispatcher("CustomerView/LoginView.jsp").forward(request, response);

        /*   System.out.println("Username-"+username);
            System.out.println("Email-"+email);
            System.out.println("ContactNumber-"+contactNo);
            System.out.println("Address-"+address);
            System.out.println("Division-"+division);
            System.out.println("Password-"+password);
            System.out.println("Total Credit-"+totalCredit);
            System.out.println("Registration Date"+registrationDate);
            System.out.println("Admin Type-"+adminType);
        */


    }


}

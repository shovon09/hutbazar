package hutbazar.controller.customer;

import hutbazar.model.customer.dao.CustomerService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: shihab
 * Date: 11/1/12
 * Time: 2:26 AM
 * To change this template use File | Settings | File Templates.
 */
@WebServlet("/userAccount")
public class UserAccount extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession usersession = request.getSession(true);
        if (request.getParameter("userNewName") != null) {
            /*System.out.println(usersession.getAttribute("userId"));
            System.out.println(request.getParameter("userNewName"));
            System.out.println(request.getParameter("userNewEmail"));
            System.out.println(request.getParameter("userNewContactNumber"));
            System.out.println(request.getParameter("userNewAddress"));
            System.out.println(request.getParameter("userNewDivision"));
            System.out.println(request.getParameter("userNewPassword"));
            System.out.println(request.getParameter("userNewConfirmPassword"));*/

            usersession.setAttribute("userName", request.getParameter("userNewName"));
            usersession.setAttribute("userEmail", request.getParameter("userNewEmail"));
            usersession.setAttribute("userContactNumber", request.getParameter("userNewContactNumber"));
            usersession.setAttribute("userAddress", request.getParameter("userNewAddress"));
            usersession.setAttribute("userDivision", request.getParameter("userNewDivision"));
            usersession.setAttribute("userPassword", request.getParameter("userNewPassword"));
            usersession.setAttribute("isUpdate", null);
            CustomerService.updateUser((String) usersession.getAttribute("userId"), (String) usersession.getAttribute("userName"), (String) usersession.getAttribute("userEmail"), (String) usersession.getAttribute("userContactNumber"), (String) usersession.getAttribute("userAddress"), (String) usersession.getAttribute("userDivision"), (String) usersession.getAttribute("userPassword"));


        } else {
            usersession.setAttribute("isUpdate", "yes");

        }

        request.getRequestDispatcher("CustomerView/UserAccountView.jsp").forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession usersession = request.getSession(true);
        // request.setAttribute("isUpdate","yes");
        request.getRequestDispatcher("CustomerView/UserAccountView.jsp").forward(request, response);
    }
}

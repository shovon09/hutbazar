package hutbazar.controller.customer;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * UserEntity: Shovon
 * Date: 10/29/12
 * Time: 12:13 PM
 * To change this template use File | Settings | File Templates.
 */
@WebServlet("/addToCart")
public class AddToCart extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        HttpSession userSession = request.getSession(true);

        Cart cart;
        CartEntry cartEntry;
        cart = (Cart) userSession.getAttribute("cart");
        if (cart == null) {
            cart = new Cart();
            userSession.setAttribute("cart", cart);
//            userSession.setAttribute("cartItem", new Integer(0));
//            userSession.setAttribute("sumTotal", new Double(0.0));
        }

        String sessionId = userSession.getId();
        String userId = request.getParameter("userId");
        String categoryId = request.getParameter("categoryId");
        String storeId = request.getParameter("storeId");
        String productId = request.getParameter("productId");
        String productName = request.getParameter("productName");
        String image = request.getParameter("image");
        String quantity = request.getParameter("quantity");
        String unitPrice = request.getParameter("unitPrice");
        String avgBoughtPrice = request.getParameter("avgBoughtPrice");
        String promotion = request.getParameter("promotion");
        String promotionFrom = request.getParameter("promotionFrom");
        String promotionEnd = request.getParameter("promotionEnd");
        String promotionActive = request.getParameter("promotionActive");

        if (!promotionActive.equals("0")) {
            System.out.println(promotionFrom);
            try {
                Date promotionFromDate = new SimpleDateFormat("yyyy-MM-dd").parse(promotionFrom);
                Date promotionEndDate = new SimpleDateFormat("yyyy-MM-dd").parse(promotionEnd);
                Date today = new Date();

                if (today.after(promotionFromDate) && today.before(promotionEndDate)) {
                    System.out.println("promotionActive");
                    double discount = Double.parseDouble(unitPrice) * (Double.parseDouble(promotion)) / 100.00;
                    double priceAfterDiscount = Double.parseDouble(unitPrice) - discount;
                    double totalAfterDiscount = (double) Integer.parseInt(quantity) * priceAfterDiscount;
                    double profitAfterDiscount = totalAfterDiscount - Integer.parseInt(quantity) * Double.parseDouble(avgBoughtPrice);


                    CartEntry previousEntry = cart.getItem(Integer.parseInt(productId));
                    if (previousEntry == null) {
                        cartEntry = new CartEntry(Integer.parseInt(categoryId), Integer.parseInt(storeId),
                                Integer.parseInt(productId), productName, image, Integer.parseInt(quantity), Double.parseDouble(unitPrice), priceAfterDiscount, totalAfterDiscount, Double.parseDouble(promotion), Double.parseDouble(avgBoughtPrice), profitAfterDiscount);
                        cart.addCartItem(cartEntry);

                        Integer totalProduct = cart.getTotalProduct();
                        Double sum = cart.getSumTotal();
                        userSession.setAttribute("cartItem", totalProduct);
                        userSession.setAttribute("sumTotal", sum);

                    } else {
                        int previousQuantity = previousEntry.getQuantity();
                        double previousTotal = previousEntry.getTotal();
                        double previousProfit = previousEntry.getProfit();
                        int newQuantity = Integer.parseInt(quantity) + previousQuantity;
                        double newTotal = previousTotal + totalAfterDiscount;
                        double newProfit = previousProfit + profitAfterDiscount;
                        previousEntry.setQuantity(newQuantity);
                        previousEntry.setTotal(newTotal);
                        previousEntry.setProfit(newProfit);


                        Integer totalProduct = cart.getTotalProduct();
                        Double sum = cart.getSumTotal();
                        userSession.setAttribute("cartItem", totalProduct);
                        userSession.setAttribute("sumTotal", sum);
                    }


                } else {

                    System.out.println("promotionInActive");
                    double total = Double.parseDouble(unitPrice) * Integer.parseInt(quantity);
                    double profit = total - Integer.parseInt(quantity) * Double.parseDouble(avgBoughtPrice);

                    CartEntry previousEntry = cart.getItem(Integer.parseInt(productId));
                    if (previousEntry == null) {
                        cartEntry = new CartEntry(Integer.parseInt(categoryId), Integer.parseInt(storeId),
                                Integer.parseInt(productId), productName, image, Integer.parseInt(quantity), Double.parseDouble(unitPrice), Double.parseDouble(unitPrice), total, Double.parseDouble(promotion), Double.parseDouble(avgBoughtPrice), profit);
                        cart.addCartItem(cartEntry);


                        Integer totalProduct = cart.getTotalProduct();
                        Double sum = cart.getSumTotal();
                        userSession.setAttribute("cartItem", totalProduct);
                        userSession.setAttribute("sumTotal", sum);
                    } else {
                        int previousQuantity = previousEntry.getQuantity();
                        double previousTotal = previousEntry.getTotal();
                        double previousProfit = previousEntry.getProfit();
                        int newQuantity = Integer.parseInt(quantity) + previousQuantity;
                        double newTotal = previousTotal + total;
                        double newProfit = previousProfit + profit;
                        previousEntry.setQuantity(newQuantity);
                        previousEntry.setTotal(newTotal);
                        previousEntry.setProfit(newProfit);


                        Integer totalProduct = cart.getTotalProduct();
                        Double sum = cart.getSumTotal();
                        userSession.setAttribute("cartItem", totalProduct);
                        userSession.setAttribute("sumTotal", sum);
                    }


                }

            } catch (ParseException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        } else {

            System.out.println("promotionInActive");
            double total = Double.parseDouble(unitPrice) * Integer.parseInt(quantity);
            double profit = total - Integer.parseInt(quantity) * Double.parseDouble(avgBoughtPrice);

            CartEntry previousEntry = cart.getItem(Integer.parseInt(productId));
            if (previousEntry == null) {
                cartEntry = new CartEntry(Integer.parseInt(categoryId), Integer.parseInt(storeId),
                        Integer.parseInt(productId), productName, image, Integer.parseInt(quantity), Double.parseDouble(unitPrice), Double.parseDouble(unitPrice), total, Double.parseDouble(promotion), Double.parseDouble(avgBoughtPrice), profit);
                cart.addCartItem(cartEntry);


                Integer totalProduct = cart.getTotalProduct();
                Double sum = cart.getSumTotal();
                userSession.setAttribute("cartItem", totalProduct);
                userSession.setAttribute("sumTotal", sum);
            } else {
                int previousQuantity = previousEntry.getQuantity();
                double previousTotal = previousEntry.getTotal();
                double previousProfit = previousEntry.getProfit();
                int newQuantity = Integer.parseInt(quantity) + previousQuantity;
                double newTotal = previousTotal + total;
                double newProfit = previousProfit + profit;
                previousEntry.setQuantity(newQuantity);
                previousEntry.setTotal(newTotal);
                previousEntry.setProfit(newProfit);


                Integer totalProduct = cart.getTotalProduct();
                Double sum = cart.getSumTotal();
                userSession.setAttribute("cartItem", totalProduct);
                userSession.setAttribute("sumTotal", sum);
            }
        }


        List<CartEntry> cartEntries = cart.getCartEntries();
        double sumTotal = 0.00;
        for (CartEntry cartItem : cartEntries) {
            sumTotal = sumTotal + cartItem.getTotal();
        }
        request.setAttribute("sumTotal", sumTotal);
        request.setAttribute("carts", cartEntries);
        request.getRequestDispatcher("CustomerView/CartDetails.jsp").forward(request, response);


    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}

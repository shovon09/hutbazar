package hutbazar.controller.customer;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * UserEntity: shihab
 * Date: 10/28/12
 * Time: 7:34 PM
 * To change this template use File | Settings | File Templates.
 */
@WebServlet("/logout")
public class Logout extends HttpServlet {


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession userSession = request.getSession(true);
        userSession.invalidate();
        request.getRequestDispatcher("index.jsp").forward(request, response);

    }
}

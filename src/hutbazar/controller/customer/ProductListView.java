package hutbazar.controller.customer;

import hutbazar.model.customer.dao.CustomerService;
import hutbazar.model.customer.domain.ProductCategoryEntity;
import hutbazar.model.customer.domain.ProductEntity;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * UserEntity: Shovon
 * Date: 10/24/12
 * Time: 10:01 PM
 * To change this template use File | Settings | File Templates.
 */
@WebServlet("/listProduct")
public class ProductListView extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String categoryID = request.getParameter("catId");
        String storeID = request.getParameter("store_id");
        List<ProductCategoryEntity> productCategoryEntity = CustomerService.getChildCategory(categoryID);
        if (productCategoryEntity.isEmpty()) {
            List<ProductEntity> products = CustomerService.getProducts(categoryID);

            request.setAttribute("storeId", storeID);
            request.setAttribute("products", products);
            request.getRequestDispatcher("CustomerView/CategoryDetailsView.jsp").forward(request, response);

        } else {
            request.setAttribute("categories", productCategoryEntity);
            request.getRequestDispatcher("CustomerView/StoreDetailsView.jsp").forward(request, response);
        }

    }
}

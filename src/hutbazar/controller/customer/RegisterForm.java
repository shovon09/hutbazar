package hutbazar.controller.customer;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * UserEntity: shihab
 * Date: 10/28/12
 * Time: 7:40 PM
 * To change this template use File | Settings | File Templates.
 */
@WebServlet("/register")
public class RegisterForm extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("CustomerView/RegisterFormView.jsp").forward(request, response);
    }
}

package hutbazar.controller.customer;

import hutbazar.model.customer.dao.CustomerService;
import hutbazar.model.customer.domain.OrderDetailsEntity;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: shihab
 * Date: 11/6/12
 * Time: 11:35 PM
 * To change this template use File | Settings | File Templates.
 */
@javax.servlet.annotation.WebServlet("/orderDetails")
public class UserOrderDetails extends javax.servlet.http.HttpServlet {
    protected void doPost(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, java.io.IOException {
        String orderId = request.getParameter("orderIDToGetDetails");
        double total = 0;
        int totalQuantity = 0;

        List<OrderDetailsEntity> orderDetails = CustomerService.getOrderDetails(orderId);
        /*   System.out.print("The Size of the list is " +orderDetails.size());*/
        for (int i = 0; i < orderDetails.size(); i++) {
            /*     System.out.print("Product " +orderDetails.get(i).getProductName());*/
            total = total + Double.parseDouble(orderDetails.get(i).getTotal());
            totalQuantity = Integer.parseInt(totalQuantity + orderDetails.get(i).getProductQuantity());

        }

        double totalAfterShipment = total + Double.parseDouble(orderDetails.get(0).getShipmentCharge());
        request.setAttribute("shipmentCharge", orderDetails.get(0).getShipmentCharge());
        request.setAttribute("orderDate", orderDetails.get(0).getOrderDate());
        request.setAttribute("delivaryDate", orderDetails.get(0).getDelivaryDate());
        request.setAttribute("delivaryAddress", orderDetails.get(0).getDelivaryAddress());
        request.setAttribute("orderStatus", orderDetails.get(0).getOrderStatus());
        request.setAttribute("total", total);
        request.setAttribute("totalQuantity", totalQuantity);
        request.setAttribute("totalAfterShipment", totalAfterShipment);


        request.setAttribute("orderDetails", orderDetails);
        request.getRequestDispatcher("CustomerView/UserOrderDetails.jsp").forward(request, response);

    }

    protected void doGet(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, java.io.IOException {

    }
}

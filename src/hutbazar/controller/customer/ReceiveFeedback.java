package hutbazar.controller.customer;

import hutbazar.model.customer.dao.CustomerService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: shihab
 * Date: 10/30/12
 * Time: 1:26 AM
 * To change this template use File | Settings | File Templates.
 */
@WebServlet("/giveFeedback")
public class ReceiveFeedback extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String userName = request.getParameter("userName");
        String userEmail = request.getParameter("userEmail");
        String userFeedback = request.getParameter("userFeedback");
        CustomerService.receiveFeedback(userName, userEmail, userFeedback);
        request.setAttribute("success", "1");
        request.getRequestDispatcher("CustomerView/FeedBackFormView.jsp").forward(request, response);


    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}

package hutbazar.controller.customer;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: shihab
 * Date: 10/29/12
 * Time: 11:28 PM
 * To change this template use File | Settings | File Templates.
 */
@WebServlet("/ownStore")
public class OwnAStoreForm extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("CustomerView/OwnAStoreFormView.jsp").forward(request, response);
    }
}

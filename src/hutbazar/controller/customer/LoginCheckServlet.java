package hutbazar.controller.customer;

import hutbazar.model.customer.dao.CustomerService;
import hutbazar.model.customer.dao.LoginCheckService;
import hutbazar.model.customer.domain.UserEntity;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * UserEntity: shihab
 * Date: 10/23/12
 * Time: 11:20 PM
 * To change this template use File | Settings | File Templates.
 */
@WebServlet("/logincheck")
public class LoginCheckServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String email = request.getParameter("Email");
        String password = request.getParameter("Password");

        System.out.println("Servlet Before Checking in database-" + email);
        System.out.println("Servlet Before Checking in database-" + password);

        List<UserEntity> userEntityList = LoginCheckService.logincheck(email, password);

        try {
            String userAdminType = "";
            if (!userEntityList.isEmpty()) {
                for (UserEntity userEntity : userEntityList) {
                    HttpSession usersession = request.getSession(true);
                    usersession.setAttribute("userId", userEntity.getCustomer_id());
                    usersession.setAttribute("userName", userEntity.getCustomer_name());
                    usersession.setAttribute("userContactNumber", userEntity.getContactNumber());
                    usersession.setAttribute("userAddress", userEntity.getAddress());
                    usersession.setAttribute("userPassword", userEntity.getPassword());
                    usersession.setAttribute("userDivision", userEntity.getDivision());
                    usersession.setAttribute("userAdminType", userEntity.getAdmin_type());
                    userAdminType = userEntity.getAdmin_type();
                    usersession.setAttribute("userEmail", userEntity.getEmail());
                    System.out.println(usersession.getId());
                }

                if (userAdminType.equals(UserEntity.ADMIN)) {


                    for (UserEntity userEntity : userEntityList) {
                        HttpSession usersession = request.getSession(true);
                        usersession.setAttribute("userId", userEntity.getCustomer_id());
                        usersession.setAttribute("userStoreId", CustomerService.getStoreId(userEntity.getCustomer_id()));
                        usersession.setAttribute("userName", userEntity.getCustomer_name());
                        usersession.setAttribute("userContactNumber", userEntity.getContactNumber());
                        usersession.setAttribute("userAddress", userEntity.getAddress());
                        usersession.setAttribute("userPassword", userEntity.getPassword());
                        usersession.setAttribute("userDivision", userEntity.getDivision());
                        usersession.setAttribute("userAdminType", userEntity.getAdmin_type());
                        userAdminType = userEntity.getAdmin_type();
                        usersession.setAttribute("userEmail", userEntity.getEmail());
                        System.out.println("Store ID is-" + usersession.getAttribute("userStoreId"));
                    }
                    response.sendRedirect("admin/dashboard");
                } else {
                    //normal logged in user
                    System.out.print("hello");
                    request.getRequestDispatcher("index.jsp").forward(request, response);
                }

            } else {
                //logged in failed,go to loin page again
                System.out.print("hello");
                request.setAttribute("failed", "yes");
                request.setAttribute("success", null);  //if not successfull
                request.getRequestDispatcher("CustomerView/LoginView.jsp").forward(request, response);
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }


    }


}

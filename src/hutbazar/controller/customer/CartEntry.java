package hutbazar.controller.customer;

/**
 * Created with IntelliJ IDEA.
 * UserEntity: Shovon
 * Date: 10/29/12
 * Time: 9:43 PM
 * To change this template use File | Settings | File Templates.
 */
public class CartEntry {
    private int categoryId;
    private int storeId;
    private int productId;
    private String productName;
    private String image;
    private int quantity;
    private double unitPrice;
    private double unitPriceAfterPromotion;
    private double total;
    private double promotion;
    private double averageBoughtPrice;
    private double profit;

    public CartEntry(int categoryId, int storeId, int productId, String productName, String image, int quantity, double unitPrice, double unitPriceAfterPromotion, double total, double promotion, double averageBoughtPrice, double profit) {
        this.categoryId = categoryId;
        this.storeId = storeId;
        this.productId = productId;
        this.productName = productName;
        this.image = image;
        this.quantity = quantity;
        this.unitPrice = unitPrice;
        this.unitPriceAfterPromotion = unitPriceAfterPromotion;
        this.total = total;
        this.promotion = promotion;
        this.averageBoughtPrice = averageBoughtPrice;
        this.profit = profit;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public double getPromotion() {
        return promotion;
    }

    public void setPromotion(double promotion) {
        this.promotion = promotion;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public int getStoreId() {
        return storeId;
    }

    public void setStoreId(int storeId) {
        this.storeId = storeId;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(double unitPrice) {
        this.unitPrice = unitPrice;
    }

    public double getUnitPriceAfterPromotion() {
        return unitPriceAfterPromotion;
    }

    public void setUnitPriceAfterPromotion(double unitPriceAfterPromotion) {
        this.unitPriceAfterPromotion = unitPriceAfterPromotion;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public double getAverageBoughtPrice() {
        return averageBoughtPrice;
    }

    public void setAverageBoughtPrice(double averageBoughtPrice) {
        this.averageBoughtPrice = averageBoughtPrice;
    }

    public double getProfit() {
        return profit;
    }

    public void setProfit(double profit) {
        this.profit = profit;
    }
}

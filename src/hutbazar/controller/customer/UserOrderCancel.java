package hutbazar.controller.customer;

import hutbazar.model.customer.dao.CustomerService;
import hutbazar.model.customer.domain.OrderListEntity;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: shihab
 * Date: 11/7/12
 * Time: 12:04 AM
 * To change this template use File | Settings | File Templates.
 */
@WebServlet("/orderCancel")
public class UserOrderCancel extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String orderId=request.getParameter("orderIDToCancel");
        CustomerService.cancelOrder(orderId);
        HttpSession userSession = request.getSession(true);
        List<OrderListEntity> orderList = CustomerService.getOrderList((String) userSession.getAttribute("userId"));
        System.out.print("The Size is " + orderList.size());
        request.setAttribute("orderList",orderList);
        request.getRequestDispatcher("CustomerView/CustomerOrders.jsp").forward(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}

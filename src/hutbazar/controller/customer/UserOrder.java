package hutbazar.controller.customer;

import hutbazar.model.customer.dao.CustomerService;
import hutbazar.model.customer.domain.OrderListEntity;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: shihab
 * Date: 11/5/12
 * Time: 11:54 PM
 * To change this template use File | Settings | File Templates.
 */
@WebServlet("/userOrder")
public class UserOrder extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession userSession = request.getSession(true);
        List<OrderListEntity> orderList = CustomerService.getOrderList((String) userSession.getAttribute("userId"));
//        List<OrderListEntity> orderList = CustomerService.getOrderList();
        System.out.print("The Size is " + orderList.size());
        request.setAttribute("orderList", orderList);
        request.getRequestDispatcher("CustomerView/CustomerOrders.jsp").forward(request, response);
    }
}

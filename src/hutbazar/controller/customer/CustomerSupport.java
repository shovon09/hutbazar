package hutbazar.controller.customer;

import hutbazar.model.customer.dao.CustomerService;
import hutbazar.model.customer.domain.ProductCategoryEntity;
import hutbazar.model.customer.domain.ProductEntity;
import hutbazar.model.customer.domain.StoreEntity;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: shihab
 * Date: 10/30/12
 * Time: 11:45 PM
 * To change this template use File | Settings | File Templates.
 */
@WebServlet("/support")
public class CustomerSupport extends javax.servlet.http.HttpServlet {
    protected void doPost(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, java.io.IOException {
        HttpSession usersession = request.getSession(true);

        if (request.getParameter("state") != null) {

            usersession.setAttribute("selectedStoreId", request.getParameter("storeId"));
            List<ProductCategoryEntity> category = CustomerService.getProductCatagory((String) usersession.getAttribute("selectedStoreId"));
            List<StoreEntity> store = CustomerService.getStores();
            request.setAttribute("stores", store);
            request.setAttribute("category", category);
            request.getRequestDispatcher("CustomerView/SupportFormView.jsp").forward(request, response);
        }

        if (request.getParameter("state2") != null) {

            usersession.setAttribute("selectedCategoryId", request.getParameter("categoryId"));
            List<ProductCategoryEntity> category = CustomerService.getProductCatagory((String) usersession.getAttribute("selectedStoreId"));
            List<StoreEntity> store = CustomerService.getStores();
            List<ProductEntity> product = CustomerService.getProducts((String) usersession.getAttribute("selectedCategoryId"));
            request.setAttribute("stores", store);
            request.setAttribute("category", category);
            request.setAttribute("product", product);
            request.getRequestDispatcher("CustomerView/SupportFormView.jsp").forward(request, response);
        }

        if (request.getParameter("state3") != null) {
            usersession.setAttribute("selectedProductId", request.getParameter("productId"));
            CustomerService.customerSupport((String) usersession.getAttribute("selectedStoreId"), (String) usersession.getAttribute("selectedCategoryId"), (String) usersession.getAttribute("selectedProductId"), request.getParameter("userComment"),(String) usersession.getAttribute("userEmail"));
            usersession.setAttribute("selectedStoreId", null);
            usersession.setAttribute("selectedCategoryId", null);
            usersession.setAttribute("selectedProductId", null);
            List<StoreEntity> store = CustomerService.getStores();
            request.setAttribute("stores", store);
            request.setAttribute("success", "yes");
            request.getRequestDispatcher("CustomerView/SupportFormView.jsp").forward(request, response);
        }
    }

    protected void doGet(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, java.io.IOException {
        HttpSession usersession = request.getSession(true);
        List<StoreEntity> storeEntities = CustomerService.getStores();
        request.setAttribute("stores", storeEntities);
        usersession.setAttribute("selectedStoreId", null);
        usersession.setAttribute("selectedCategoryId", null);
        usersession.setAttribute("selectedProductId", null);
        request.getRequestDispatcher("CustomerView/SupportFormView.jsp").forward(request, response);
    }
}

package hutbazar.controller.customer;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * UserEntity: Shovon
 * Date: 10/30/12
 * Time: 9:47 PM
 * To change this template use File | Settings | File Templates.
 */
@WebServlet("/editCart")
public class EditCart extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int quantity = Integer.parseInt(request.getParameter("quantity"));
        int productId = Integer.parseInt(request.getParameter("productId"));
        HttpSession userSession = request.getSession(true);
        Cart cart = (Cart) userSession.getAttribute("cart");
        CartEntry cartEntry = cart.getItem(productId);
        cartEntry.setQuantity(quantity);
        double unitPrice = cartEntry.getUnitPriceAfterPromotion();
        double averageBoughtPrice = cartEntry.getAverageBoughtPrice();
        double total = unitPrice * quantity;
        double profit = total - averageBoughtPrice * quantity;
        cartEntry.setTotal(total);
        cartEntry.setProfit(profit);

        Integer totalProduct = cart.getTotalProduct();
        Double sum = cart.getSumTotal();
        userSession.setAttribute("cartItem", totalProduct);
        userSession.setAttribute("sumTotal", sum);

        List<CartEntry> cartEntries = cart.getCartEntries();
        for (CartEntry a : cartEntries) {
            System.out.println(a.getProductId());
            System.out.println(a.getQuantity());
            System.out.println(a.getAverageBoughtPrice());
            System.out.println(a.getTotal());
            System.out.println(a.getProfit());

        }
        double sumTotal = 0.00;
        for (CartEntry cartItem : cartEntries) {
            sumTotal = sumTotal + cartItem.getTotal();
        }
        request.setAttribute("sumTotal", sumTotal);
        request.setAttribute("carts", cartEntries);
        request.getRequestDispatcher("CustomerView/CartDetails.jsp").forward(request, response);


    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}

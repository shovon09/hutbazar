package hutbazar.controller.customer;

import hutbazar.model.customer.dao.CustomerService;
import hutbazar.model.customer.domain.StoreEntity;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: shihab
 * Date: 10/30/12
 * Time: 12:23 PM
 * To change this template use File | Settings | File Templates.
 */
@WebServlet("/wishlist")
public class WishlistForm extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String userName = request.getParameter("userName");
        String userEmail = request.getParameter("userEmail");
        String userProduct = request.getParameter("userProduct");
        String userProductDescription = request.getParameter("userProductDescription");
        String storeId = request.getParameter("storeId");


        CustomerService.receiveWishlist(userName, userEmail, userProduct, userProductDescription, storeId);
        List<StoreEntity> storeEntities = CustomerService.getStores();
        request.setAttribute("success", "1");
        request.setAttribute("stores", storeEntities);
        request.getRequestDispatcher("CustomerView/WishlistFormView.jsp").forward(request, response);

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<StoreEntity> storeEntities = CustomerService.getStores();
        request.setAttribute("stores", storeEntities);
        request.getRequestDispatcher("CustomerView/WishlistFormView.jsp").forward(request, response);
    }
}

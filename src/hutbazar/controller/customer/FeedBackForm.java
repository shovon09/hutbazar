package hutbazar.controller.customer;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: shihab
 * Date: 10/30/12
 * Time: 1:03 AM
 * To change this template use File | Settings | File Templates.
 */
@WebServlet("/feedback")
public class FeedBackForm extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("CustomerView/FeedBackFormView.jsp").forward(request, response);
    }
}

package hutbazar.controller.customer;

import hutbazar.model.customer.dao.ProductSearch;
import hutbazar.model.customer.domain.SearchResultEntity;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * UserEntity: Shovon
 * Date: 11/6/12
 * Time: 9:44 PM
 * To change this template use File | Settings | File Templates.
 */
@WebServlet("/searchProduct")
public class SearchProduct extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String lowPrice = request.getParameter("lowerBound");
        String upPrice = request.getParameter("upperBound");
        String keyWord = request.getParameter("keyWord");
        if (lowPrice.isEmpty()) {
            lowPrice = "0";
        }
        if (upPrice.isEmpty()) {
            upPrice = "0";
        }
        List<SearchResultEntity> searchResult = ProductSearch.searchProduct(Double.parseDouble(lowPrice), Double.parseDouble(upPrice), keyWord);

        request.setAttribute("searchResult", searchResult);
        request.getRequestDispatcher("CustomerView/SearchResultPage.jsp").forward(request, response);

    }


}

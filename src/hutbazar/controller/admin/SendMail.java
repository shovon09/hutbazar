package hutbazar.controller.admin;

import hutbazar.util.SendEmail;

import javax.mail.MessagingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: Shovon
 * Date: 11/13/12
 * Time: 12:13 PM
 * To change this template use File | Settings | File Templates.
 */
@WebServlet("/admin/sendMail")
public class SendMail extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String email = request.getParameter("email");
        String subject = request.getParameter("subject");
        String message = request.getParameter("message");
        String confirmation;
        confirmation = "Mail send Succesfully";
        try {
            SendEmail.sendEmail(email, subject, message);
        } catch (MessagingException e) {
            confirmation = "Sorry mail could not send.Please check your internet connection or mail address";
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        request.setAttribute("confirmation", confirmation);
        request.getRequestDispatcher("/AdminView/ConfirmationView.jsp").forward(request,response);

    }


}

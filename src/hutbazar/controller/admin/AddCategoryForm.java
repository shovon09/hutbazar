package hutbazar.controller.admin;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: Shovon
 * Date: 11/10/12
 * Time: 1:22 AM
 * To change this template use File | Settings | File Templates.
 */
@WebServlet("/admin/addCategoryForm")
public class AddCategoryForm extends HttpServlet {


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.getRequestDispatcher("/AdminView/AddCategoryForm.jsp").forward(request, response);

    }
}

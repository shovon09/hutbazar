package hutbazar.controller.admin;

import hutbazar.model.admin.dao.OrderDetailsService;
import hutbazar.model.admin.domain.OrderDetailsEntity;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.data.general.DefaultPieDataset;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: shihab
 * Date: 11/13/12
 * Time: 2:59 AM
 * To change this template use File | Settings | File Templates.
 */
@WebServlet("/admin/orderProductsGraph")
public class OrderProductGraph extends HttpServlet {


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {

        int i=0;
        String orderId = request.getParameter("orderIdToGetDetails");
        System.out.print("orderId pailam hoilo " +orderId);
        OutputStream out = response.getOutputStream();
        List<OrderDetailsEntity> orderDetails = OrderDetailsService.getOrderDetails(orderId);
        DefaultPieDataset dataset = new DefaultPieDataset();
        for(i=0;i<orderDetails.size();i++)
        {
            dataset.setValue(orderDetails.get(i).getProductName(), Double.parseDouble(orderDetails.get(i).getSales()));

        }


        JFreeChart chart = ChartFactory.createPieChart("Product Sales Ratio Chart", dataset, true, true, false);

        ChartUtilities.writeChartAsPNG(out, chart, 300, 300);/* Write the data to the output stream */

    }
}

package hutbazar.controller.admin;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: Shovon
 * Date: 11/13/12
 * Time: 11:59 AM
 * To change this template use File | Settings | File Templates.
 */
@WebServlet("/admin/mailForm")
public class MailForm extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String email = request.getParameter("email");
        request.setAttribute("email", email);

        request.getRequestDispatcher("/AdminView/MailForm.jsp").forward(request, response);
    }
}

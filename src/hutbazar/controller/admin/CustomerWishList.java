package hutbazar.controller.admin;

import hutbazar.model.admin.dao.GetCustomerWishList;
import hutbazar.model.admin.domain.WishListEntity;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Shovon
 * Date: 11/13/12
 * Time: 3:52 AM
 * To change this template use File | Settings | File Templates.
 */
@WebServlet("/admin/customerWishList")
public class CustomerWishList extends HttpServlet {


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String storeId = (String) request.getSession(true).getAttribute("storeId");
        List<WishListEntity> wishListEntityList = GetCustomerWishList.getWishList(storeId);
        request.setAttribute("wishList", wishListEntityList);
        request.getRequestDispatcher("/AdminView/CustomerWishList.jsp").forward(request, response);
    }
}

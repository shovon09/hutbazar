package hutbazar.controller.admin;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.data.general.DefaultPieDataset;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Created with IntelliJ IDEA.
 * User: Shovon
 * Date: 11/13/12
 * Time: 2:11 AM
 * To change this template use File | Settings | File Templates.
 */
@WebServlet("/admin/overAllSalesRatio")
public class OverAllSalesRatio extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String totalSales = request.getParameter("totalSales");
        String totalProfit = request.getParameter("totalProfit");
        String totalInvestment = request.getParameter("totalInvestment");
        OutputStream out = response.getOutputStream(); /* Get the output stream from the response object */
        response.setContentType("image/png"); /* Set the HTTP Response Type */
        DefaultPieDataset dataSet = new DefaultPieDataset();
        dataSet.setValue("Total Sales", Double.parseDouble(totalSales));
        dataSet.setValue("Total Investment", Double.parseDouble(totalInvestment));
        dataSet.setValue("Total Profit", Double.parseDouble(totalProfit));


        JFreeChart chart = ChartFactory.createPieChart("Sales/Profit/Investment Ratio", dataSet, true, true, false);

//        JFreeChart chart = ChartFactory.createBarChart()

        ChartUtilities.writeChartAsPNG(out, chart, 300, 300);
    }
}

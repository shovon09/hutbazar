package hutbazar.controller.admin;

import hutbazar.model.admin.dao.OrderListService;
import hutbazar.model.admin.domain.OrderListEntity;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: shihab
 * Date: 11/9/12
 * Time: 1:20 AM
 * To change this template use File | Settings | File Templates.
 */
@WebServlet("/admin/showOrders")
public class ShowOrders extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.print("eikhane dhukse");
        HttpSession usersession = request.getSession(true);
        List<OrderListEntity> orderList = OrderListService.getOrderList((String) usersession.getAttribute("userStoreId"));
        System.out.print("The Size is " + orderList.size());
        request.setAttribute("orderList", orderList);
        request.getRequestDispatcher("/AdminView/Orders.jsp").forward(request, response);

    }
}

package hutbazar.controller.admin;

import hutbazar.model.admin.dao.NotSoldProductService;
import hutbazar.model.admin.dao.SalesReportSummary;
import hutbazar.model.admin.domain.NotSoldProductEntity;
import hutbazar.model.admin.domain.SalesReportSummaryEntity;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: shihab
 * Date: 11/9/12
 * Time: 9:30 PM
 * To change this template use File | Settings | File Templates.
 */
@WebServlet("/admin/salesReport")
public class SalesReportServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession usersession = request.getSession(true);
        List<SalesReportSummaryEntity> salesReportSummary = SalesReportSummary.salesReport((String) usersession.getAttribute("userStoreId"));
        List<NotSoldProductEntity> notSoldProducts = NotSoldProductService.getNotSoldProducts((String) usersession.getAttribute("userStoreId"));
        request.setAttribute("salesReportSummary", salesReportSummary);
        request.setAttribute("notSoldProducts", notSoldProducts);
        request.getRequestDispatcher("/AdminView/SalesReportSummary.jsp").forward(request, response);
    }
}

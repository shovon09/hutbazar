package hutbazar.controller.admin;

import hutbazar.model.admin.dao.InsertNewProduct;
import hutbazar.model.admin.dao.UpdateProduct;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created with IntelliJ IDEA.
 * User: Shovon
 * Date: 11/9/12
 * Time: 12:07 PM
 * To change this template use File | Settings | File Templates.
 */
@WebServlet("/admin/addProduct")
@MultipartConfig
public class AddProduct extends HttpServlet {
    private final static Logger LOGGER = Logger.getLogger(AddProduct.class.getCanonicalName());

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String productName = request.getParameter("name");
        String categoryId = request.getParameter("category");
        String description = request.getParameter("description");
        String discount = request.getParameter("discount");
        String boughtQuantity = request.getParameter("quantity");
        String boughtPrice = request.getParameter("boughtPrice");
        String sellingPrice = request.getParameter("sellingPrice");
        String threshold = request.getParameter("threshold");
        String promotionStartDate = request.getParameter("dstart");
        String promotionEndDate = request.getParameter("dend");
        String productId = request.getParameter("productId");
        if (promotionStartDate.equals("")) {
            promotionEndDate = null;
            promotionStartDate = null;
        }
        String promotionActiveString = request.getParameter("active");
        if (promotionActiveString.equals("")) {
            promotionActiveString = "0";
        }
        String storeId = (String) request.getSession(true).getAttribute("storeId");
        Date currentDate = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        String dateAdded = dateFormat.format(currentDate).toString();

        response.setContentType("text/html;charset=UTF-8");

        // Create path components to save the file
        final String realativePath = "/Images";
        final String path = getServletContext().getRealPath(realativePath);
        final Part filePart = request.getPart("file");
        final String fileName = getFileName(filePart);

        OutputStream out = null;
        InputStream filecontent = null;
        final PrintWriter writer = response.getWriter();

        try {
            out = new FileOutputStream(new File(path + File.separator + fileName));
            filecontent = filePart.getInputStream();

            int read = 0;
            final byte[] bytes = new byte[1024];

            while ((read = filecontent.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }

            LOGGER.log(Level.INFO, "File{0}being uploaded to {1}",
                    new Object[]{fileName, path});

        } catch (FileNotFoundException fne) {
            writer.println("You either did not specify a file to upload or are " +
                    "trying to upload a file to a protected or nonexistent location.");
            writer.println("<br/> ERROR: " + fne.getMessage());

            LOGGER.log(Level.SEVERE, "Problems during file upload. Error: {0}",
                    new Object[]{fne.getMessage()});
        } finally {
            if (out != null) {
                out.close();
            }
            if (filecontent != null) {
                filecontent.close();
            }
            if (writer != null) {
                writer.close();
            }
        }

        String storeImage = "." + realativePath + "/" + fileName;
        if (!productId.equals("") && (productId != null)) {
            System.out.println(productId);
            System.out.println(productName);
            System.out.println(categoryId);
            System.out.println(storeId);
            System.out.println(description);
            System.out.println(storeImage);
            System.out.println(dateAdded);
            System.out.println(discount);
            System.out.println(promotionStartDate);
            System.out.println(promotionEndDate);
            System.out.println(boughtPrice);
            System.out.println(sellingPrice);
            System.out.println(boughtQuantity);
            System.out.println(threshold);
            System.out.println(promotionActiveString);
            UpdateProduct.updateProduct(productId, productName, categoryId, storeId, description, storeImage, dateAdded, discount, promotionStartDate, promotionEndDate, boughtPrice, sellingPrice, boughtQuantity, threshold, promotionActiveString);
            System.out.println("taile hobe");

        } else {
            InsertNewProduct.insertNewProduct(productName, categoryId, storeId, description, storeImage, dateAdded, discount, promotionStartDate, promotionEndDate, boughtPrice, sellingPrice, boughtQuantity, threshold, promotionActiveString);

        }

        response.sendRedirect(request.getContextPath() + "/admin/manageProduct?storeId=" + storeId);

    }

    private String getFileName(final Part part) {
        final String partHeader = part.getHeader("content-disposition");
        LOGGER.log(Level.INFO, "Part Header = {0}", partHeader);
        for (String content : part.getHeader("content-disposition").split(";")) {
            if (content.trim().startsWith("filename")) {
                return content.substring(
                        content.indexOf('=') + 1).trim().replace("\"", "");
            }
        }
        return null;
    }
}

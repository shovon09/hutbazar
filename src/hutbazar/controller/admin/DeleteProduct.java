package hutbazar.controller.admin;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: Shovon
 * Date: 11/13/12
 * Time: 1:10 PM
 * To change this template use File | Settings | File Templates.
 */
@WebServlet("/admin/deleteProduct")
public class DeleteProduct extends HttpServlet {


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String productId = request.getParameter("productId");
        String storeId = (String) request.getSession(true).getAttribute("storeId");
        hutbazar.model.admin.dao.DeleteProduct.deleteProduct(productId);
        response.setContentType("text/html;charset=UTF-8");
        response.sendRedirect(request.getContextPath() + "/admin/manageProduct?storeId=" + storeId);

    }
}

package hutbazar.controller.admin;

import hutbazar.model.admin.dao.MonthlySalesReportService;
import hutbazar.model.admin.domain.MonthlySalesReportEntity;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: shihab
 * Date: 11/13/12
 * Time: 9:45 AM
 * To change this template use File | Settings | File Templates.
 */
@WebServlet("/admin/monthWiseSalesReport")
public class MonthWiseSalesReport extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {



    }


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        HttpSession usersession= request.getSession(true);
        List<MonthlySalesReportEntity> monthlySalesReport = MonthlySalesReportService.getMonthySalesReport((String) usersession.getAttribute("userStoreId")) ;
        request.setAttribute("monthlySalesReport",monthlySalesReport);
        request.getRequestDispatcher("/AdminView/MonthlySalesReport.jsp").forward(request, response);
    }
}

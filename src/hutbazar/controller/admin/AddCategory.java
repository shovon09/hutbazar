package hutbazar.controller.admin;

import hutbazar.model.admin.dao.InserCategory;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created with IntelliJ IDEA.
 * User: Shovon
 * Date: 11/10/12
 * Time: 1:36 AM
 * To change this template use File | Settings | File Templates.
 */
@WebServlet("/admin/addCategory")
@MultipartConfig
public class AddCategory extends HttpServlet {
    private final static Logger LOGGER = Logger.getLogger(AddCategory.class.getCanonicalName());

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String categoryName = request.getParameter("name");
        String storeId = (String) request.getSession(true).getAttribute("storeId");

        response.setContentType("text/html;charset=UTF-8");

        // Create path components to save the file
        final String realativePath = "/Images";
        final String path = getServletContext().getRealPath(realativePath);
        final Part filePart = request.getPart("file");
        final String fileName = getFileName(filePart);

        OutputStream out = null;
        InputStream filecontent = null;
        final PrintWriter writer = response.getWriter();

        try {
            out = new FileOutputStream(new File(path + File.separator + fileName));
            filecontent = filePart.getInputStream();

            int read = 0;
            final byte[] bytes = new byte[1024];

            while ((read = filecontent.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }

            LOGGER.log(Level.INFO, "File{0}being uploaded to {1}",
                    new Object[]{fileName, path});

        } catch (FileNotFoundException fne) {
            writer.println("You either did not specify a file to upload or are " +
                    "trying to upload a file to a protected or nonexistent location.");
            writer.println("<br/> ERROR: " + fne.getMessage());

            LOGGER.log(Level.SEVERE, "Problems during file upload. Error: {0}",
                    new Object[]{fne.getMessage()});
        } finally {
            if (out != null) {
                out.close();
            }
            if (filecontent != null) {
                filecontent.close();
            }
            if (writer != null) {
                writer.close();
            }
        }

        String categoryImage = "." + realativePath + "/" + fileName;
        InserCategory.insertCategory(categoryName, categoryImage, storeId);
        response.sendRedirect(request.getContextPath() + "/admin/manageCategory?storeId=" + storeId);
        System.out.println("hoise");

    }

    private String getFileName(final Part part) {
        final String partHeader = part.getHeader("content-disposition");
        LOGGER.log(Level.INFO, "Part Header = {0}", partHeader);
        for (String content : part.getHeader("content-disposition").split(";")) {
            if (content.trim().startsWith("filename")) {
                return content.substring(
                        content.indexOf('=') + 1).trim().replace("\"", "");
            }
        }
        return null;
    }


}

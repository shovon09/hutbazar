package hutbazar.controller.admin;

import hutbazar.model.admin.domain.OrderListEntity;
import hutbazar.model.admin.dao.NewOrderListService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: shihab
 * Date: 11/9/12
 * Time: 8:18 PM
 * To change this template use File | Settings | File Templates.
 */
@WebServlet("/admin/newOrders")
public class NewOrdersServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {

        System.out.print("eikhane dhukse");
        HttpSession usersession = request.getSession(true);
        List<OrderListEntity> orderList = NewOrderListService.getNewOrderList((String) usersession.getAttribute("userStoreId"));
        System.out.print("The Size is " + orderList.size());
        request.setAttribute("orderList",orderList);
        request.getRequestDispatcher("/AdminView/NewOrders.jsp").forward(request,response);

    }
}

package hutbazar.controller.admin;

import hutbazar.model.admin.dao.GetProductDetails;
import hutbazar.model.admin.domain.ProductDetailsEntity;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Shovon
 * Date: 11/8/12
 * Time: 11:47 PM
 * To change this template use File | Settings | File Templates.
 */
@WebServlet("/admin/manageProduct")
public class ProductManage extends HttpServlet {


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int storeId = Integer.parseInt(request.getParameter("storeId"));
        List<ProductDetailsEntity> productDetailsEntityList = GetProductDetails.getProductDetails(storeId);
        String totalProduct = GetProductDetails.getTotalProduct(storeId);
        request.setAttribute("productDetails", productDetailsEntityList);
        request.setAttribute("totalProduct", totalProduct);
        request.getRequestDispatcher("/AdminView/ManageProductPage.jsp").forward(request, response);
    }
}

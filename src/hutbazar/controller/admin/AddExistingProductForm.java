package hutbazar.controller.admin;

import hutbazar.model.admin.dao.GetProductDetails;
import hutbazar.model.admin.domain.ProductDetailsEntity;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: Shovon
 * Date: 11/9/12
 * Time: 9:58 PM
 * To change this template use File | Settings | File Templates.
 */
@WebServlet("/admin/addExistingProductForm")
public class AddExistingProductForm extends HttpServlet {


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String productId = request.getParameter("productId");
        ProductDetailsEntity product = GetProductDetails.getProduct(Integer.parseInt(productId));
        request.setAttribute("product", product);
        request.getRequestDispatcher("/AdminView/AddMoreProductForm.jsp").forward(request, response);


    }
}

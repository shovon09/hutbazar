package hutbazar.controller.admin;

import hutbazar.model.admin.dao.ProductListService;
import hutbazar.model.admin.domain.ProductSalesListEntity;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: shihab
 * Date: 11/12/12
 * Time: 8:18 PM
 * To change this template use File | Settings | File Templates.
 */
@WebServlet("/admin/productWiseSalesReport")
public class ProductWiseSalesReport extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        HttpSession usersession = request.getSession(true);
        List<ProductSalesListEntity> products = ProductListService.getProductList((String) usersession.getAttribute("userStoreId"));
        System.out.print("The Size of product List is " + products.size());
        request.setAttribute("products",products);
        request.getRequestDispatcher("/AdminView/ProductWiseSalesReportList.jsp").forward(request, response);
    }

}

package hutbazar.controller.admin;

import hutbazar.model.admin.dao.GetCategoryDetails;
import hutbazar.model.admin.domain.CategoryEntity;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Shovon
 * Date: 11/10/12
 * Time: 12:29 AM
 * To change this template use File | Settings | File Templates.
 */
@WebServlet("/admin/manageCategory")
public class ManageCategory extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String storeId = request.getParameter("storeId");
        List<CategoryEntity> categoryEntityList = GetCategoryDetails.getCategory(storeId);
        String totalCategory = GetCategoryDetails.getTotalCategory(storeId);
        request.setAttribute("category", categoryEntityList);
        request.setAttribute("totalCategory", totalCategory);
        request.getRequestDispatcher("/AdminView/ManageCategory.jsp").forward(request, response);

    }
}

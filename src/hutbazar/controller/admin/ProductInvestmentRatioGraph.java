package hutbazar.controller.admin;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Created with IntelliJ IDEA.
 * User: Shovon
 * Date: 11/13/12
 * Time: 11:51 PM
 * To change this template use File | Settings | File Templates.
 */
@WebServlet("/admin/productInvestmentRatioGraph")
public class ProductInvestmentRatioGraph extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String productInvestment = request.getParameter("productInvestment");
        String totalInvestment = request.getParameter("totalInvestment");
        System.out.print(productInvestment);
        System.out.print(totalInvestment);


        OutputStream out = response.getOutputStream(); /* Get the output stream from the response object */
        response.setContentType("image/png"); /* Set the HTTP Response Type */
//        DefaultPieDataset dataset = new DefaultPieDataset();
        DefaultCategoryDataset dataSet = new DefaultCategoryDataset();


        dataSet.addValue(Double.parseDouble(productInvestment), "Product Investment","Product");
        dataSet.addValue(Double.parseDouble(totalInvestment), "Total Investment","Product");



//        JFreeChart chart = ChartFactory.createPieChart("PE Ratio ", dataset, true, true, false);
        JFreeChart chart = ChartFactory.createBarChart3D("Product Investment Vs Total Investment", "Investment", "Taka", dataSet, PlotOrientation.VERTICAL, true, true, false);

        ChartUtilities.writeChartAsPNG(out, chart, 300, 300);
    }
}

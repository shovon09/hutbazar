package hutbazar.controller.admin;

import hutbazar.model.admin.dao.GetProductDetails;
import hutbazar.model.admin.dao.SaleCompare;
import hutbazar.model.admin.domain.ProductDetailsEntity;
import hutbazar.model.admin.domain.SalesCompareEntity;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Shovon
 * Date: 11/12/12
 * Time: 10:07 PM
 * To change this template use File | Settings | File Templates.
 */
@WebServlet("/admin/compareProduct")
public class CompareProduct extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String firstProductId = request.getParameter("firstProduct");
        SalesCompareEntity productOneCompareList = SaleCompare.getProductSales(firstProductId);
        String secondProductId = request.getParameter("secondProduct");
        SalesCompareEntity productTwoCompareList = SaleCompare.getProductSales(secondProductId);
        String storeId = (String) request.getSession(true).getAttribute("storeId");
        List<ProductDetailsEntity> productList = GetProductDetails.getProductDetails(Integer.parseInt(storeId));
        request.setAttribute("products", productList);
        request.setAttribute("firstProductSales", productOneCompareList);
        request.setAttribute("secondProductSales", productTwoCompareList);
        request.getRequestDispatcher("/AdminView/CompareProductForm.jsp").forward(request, response);

    }


}


package hutbazar.controller.admin;

import hutbazar.model.admin.dao.GetProductDetails;
import hutbazar.model.admin.domain.ProductDetailsEntity;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Shovon
 * Date: 11/12/12
 * Time: 9:35 PM
 * To change this template use File | Settings | File Templates.
 */
@WebServlet("/admin/showCompareForm")
public class ShowCompareForm extends HttpServlet {


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String storeId = request.getParameter("storeId");
        List<ProductDetailsEntity> productList = GetProductDetails.getProductDetails(Integer.parseInt(storeId));
        request.setAttribute("products", productList);
        request.getRequestDispatcher("/AdminView/CompareProductForm.jsp").forward(request, response);
    }
}

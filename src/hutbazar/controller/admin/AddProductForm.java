package hutbazar.controller.admin;

import hutbazar.model.admin.dao.GetProductDetails;
import hutbazar.model.admin.domain.ProductDetailsEntity;
import hutbazar.model.customer.dao.CustomerService;
import hutbazar.model.customer.domain.ProductCategoryEntity;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Shovon
 * Date: 11/9/12
 * Time: 2:26 AM
 * To change this template use File | Settings | File Templates.
 */
@WebServlet("/admin/addProductForm")
public class AddProductForm extends HttpServlet {


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String storeId = (String) request.getSession(true).getAttribute("storeId");
        List<ProductCategoryEntity> category = null;
        ProductDetailsEntity product = null;
        String productId = request.getParameter("productId");
        if (productId != null && !productId.equals("")) {

            product = GetProductDetails.getProduct(Integer.parseInt(productId));
        }

        category = CustomerService.getProductCatagory(storeId);
        request.setAttribute("category", category);
        request.setAttribute("product", product);
        request.getRequestDispatcher("/AdminView/AddProductForm.jsp").forward(request, response);
    }
}

package hutbazar.controller.admin;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: Shovon
 * Date: 11/13/12
 * Time: 1:52 PM
 * To change this template use File | Settings | File Templates.
 */
@WebServlet("/admin/logout")
public class Logout extends HttpServlet {


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession usersession = request.getSession(true);
        usersession.invalidate();
        response.setContentType("text/html;charset=UTF-8");
        response.sendRedirect(request.getContextPath());

    }
}

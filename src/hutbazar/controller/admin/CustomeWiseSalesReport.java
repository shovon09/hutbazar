package hutbazar.controller.admin;

import hutbazar.model.admin.dao.CustomerWiseSalesReport;
import hutbazar.model.admin.domain.CustomerSalesReportEntity;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: shihab
 * Date: 11/13/12
 * Time: 11:25 AM
 * To change this template use File | Settings | File Templates.
 */
@WebServlet("/admin/customerWiseSalesReport")
public class CustomeWiseSalesReport extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {

        HttpSession usersession = request.getSession(true);
        List<CustomerSalesReportEntity> customers = CustomerWiseSalesReport.getCustomer((String) usersession.getAttribute("userStoreId"));
        System.out.print("The Size of product List is "+customers.size());
        request.setAttribute("customers",customers);
        request.getRequestDispatcher("/AdminView/CustomerWiseSalesReportList.jsp").forward(request, response);
    }
}

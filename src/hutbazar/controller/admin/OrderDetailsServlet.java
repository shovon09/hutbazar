package hutbazar.controller.admin;

import hutbazar.model.admin.dao.OrderDetailsService;
import hutbazar.model.admin.domain.OrderDetailsEntity;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.data.general.DefaultPieDataset;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: shihab
 * Date: 11/9/12
 * Time: 6:05 PM
 * To change this template use File | Settings | File Templates.
 */
@WebServlet("/admin/orderDetails")
public class OrderDetailsServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String orderId = request.getParameter("orderIDToGetDetails");
        HttpSession usersession= request.getSession(true);
        usersession.setAttribute("orderId",orderId);
        int totalQuantity=0;
        double totalProfit=0;
        double totalSales=0;
        double totalInvestment=0;

        List<OrderDetailsEntity> orderDetails = OrderDetailsService.getOrderDetails(orderId);
        for(int i = 0 ; i<orderDetails.size();i++)
        {
            //   System.out.print("Product " +orderDetails.get(i).getProductName());
            totalSales = totalSales + Double.parseDouble( orderDetails.get(i).getSales());
            totalInvestment += (Double.parseDouble(orderDetails.get(i).getInvestment()) * Integer.parseInt(orderDetails.get(i).getProductQuantity()));
            System.out.print(totalInvestment);
            totalProfit = totalProfit + Double.parseDouble( orderDetails.get(i).getProfit());
            totalQuantity= totalQuantity+ Integer.parseInt(orderDetails.get(i).getProductQuantity());

        }

        double totalAfterShipment=totalSales + Double.parseDouble(orderDetails.get(0).getShipmentCharge());
        request.setAttribute("shipmentCharge",orderDetails.get(0).getShipmentCharge());
        request.setAttribute("orderDate",orderDetails.get(0).getOrderDate());
        request.setAttribute("delivaryDate",orderDetails.get(0).getDelivaryDate());
        request.setAttribute("delivaryAddress",orderDetails.get(0).getDelivaryAddress());
        request.setAttribute("orderStatus",orderDetails.get(0).getOrderStatus());
        request.setAttribute("totalSales",totalSales);
        request.setAttribute("totalInvestment",totalInvestment);
        request.setAttribute("totalProfit",totalProfit);
        request.setAttribute("totalQuantity",totalQuantity);
        request.setAttribute("totalAfterShipment",totalAfterShipment);
        request.setAttribute("orderDetails", orderDetails);
/*        System.out.print(orderDetails);*/
        request.getRequestDispatcher("/AdminView/OrderDetails.jsp").forward(request, response);

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        int i=0;
        String orderId = request.getParameter("orderIdToGetDetails");
        System.out.print("orderId id hoilo" + orderId);
        OutputStream out = response.getOutputStream();
        List<OrderDetailsEntity> orderDetails = OrderDetailsService.getOrderDetails(orderId);
        DefaultPieDataset dataset = new DefaultPieDataset();
//        for(i=0;i<orderDetails.size();i++)
//        {
//            dataset.setValue(orderDetails.get(i).getProductName(), Double.parseDouble(orderDetails.get(i).getSales()));
//
//        }
        for(OrderDetailsEntity order : orderDetails){
            dataset.setValue(order.getProductName(), Double.parseDouble(order.getSales()));
        }


        JFreeChart chart = ChartFactory.createPieChart3D("Product Sales Ratio Chart", dataset, true, true, false);

        ChartUtilities.writeChartAsPNG(out, chart, 400, 400);/* Write the data to the output stream */
    }
}

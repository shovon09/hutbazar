package hutbazar.controller.admin;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Created with IntelliJ IDEA.
 * User: Shovon
 * Date: 11/13/12
 * Time: 12:57 AM
 * To change this template use File | Settings | File Templates.
 */
@WebServlet("/admin/firstProductPEChart")
public class FirstProductPEChart extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String name = request.getParameter("firstProductName");
        String peRatio = request.getParameter("firstProfitRatio");
        double profitRatio = Double.parseDouble(peRatio);
        String secondName = request.getParameter("secondProductName");
        String SpeRatio = request.getParameter("secondProfitRatio");
        double SprofitRatio = Double.parseDouble(SpeRatio);
        OutputStream out = response.getOutputStream(); /* Get the output stream from the response object */
        response.setContentType("image/png"); /* Set the HTTP Response Type */
        DefaultCategoryDataset dataSet = new DefaultCategoryDataset();


        dataSet.addValue(profitRatio, name, name);
        dataSet.addValue(SprofitRatio, secondName, secondName);


//        JFreeChart chart = ChartFactory.createPieChart("PE Ratio ", dataset, true, true, false);
        JFreeChart chart = ChartFactory.createBarChart3D("Product PE Ratio", "Product", " %(profit/UnitPrice)", dataSet, PlotOrientation.VERTICAL, true, true, false);


//        JFreeChart chart = ChartFactory.createBarChart()

        ChartUtilities.writeChartAsPNG(out, chart, 400, 400);
    }
}

package hutbazar.controller.admin;

import hutbazar.model.admin.dao.ProductListService;
import hutbazar.model.admin.dao.SalesReportSummary;
import hutbazar.model.admin.domain.ProductSalesListEntity;
import hutbazar.model.admin.domain.SalesReportSummaryEntity;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: shihab
 * Date: 11/12/12
 * Time: 10:07 PM
 * To change this template use File | Settings | File Templates.
 */
@WebServlet("/admin/productWiseSalesReportDetails")
public class ProductWiseSalesReportDetails extends HttpServlet
{
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {

         String productId = request.getParameter("productId");
         HttpSession usersession = request.getSession(true);
         List<SalesReportSummaryEntity> salesReportSummary = SalesReportSummary.salesReport((String) usersession.getAttribute("userStoreId"));
         List<ProductSalesListEntity> products = ProductListService.getProductListDetails(productId);

         String totalProfit = salesReportSummary.get(0).getTotalProfit();
         String totalInvestment = salesReportSummary.get(0).getTotalInvestment();
         String totalSales = salesReportSummary.get(0).getTotalSales();

         String productProfit = products.get(0).getTotalProfit();
         String productInvestment = products.get(0).getTotalInvestment();
         String productSales = products.get(0).getTotalSales();


         request.setAttribute("productProfit",productProfit);
         request.setAttribute("productSales",productSales);
         request.setAttribute("productInvestment",productInvestment);

         request.setAttribute("totalProfit",totalProfit);
         request.setAttribute("totalInvestment",totalInvestment);
         request.setAttribute("totalSales",totalSales);

         request.setAttribute("products",products);
         request.getRequestDispatcher("/AdminView/ProductWiseSalesReportDetails.jsp").forward(request, response);
    }

}

package hutbazar.controller.admin;

import hutbazar.model.admin.dao.AddMoreProduct;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


/**
 * Created with IntelliJ IDEA.
 * User: Shovon
 * Date: 11/9/12
 * Time: 10:43 PM
 * To change this template use File | Settings | File Templates.
 */
@WebServlet("/admin/addMoreProduct")
public class AddMoreProductServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String productId = request.getParameter("productId");
        String productName = request.getParameter("name");
        String quantity = request.getParameter("quantity");
        String boughtPrice = request.getParameter("boughtPrice");
        String avgBoughtPrice = request.getParameter("avgBoughtPrice");
        String stock = request.getParameter("stock");
        String sellingPrice = request.getParameter("sellingPrice");
        String threshold = request.getParameter("threshold");
        String storeId = request.getParameter("storeId");
        System.out.println(stock);
        System.out.println(quantity);

        int newQuantity = Integer.parseInt(stock) + Integer.parseInt(quantity);
        Double newBoughtPrice = (Double.parseDouble(boughtPrice) + Double.parseDouble(avgBoughtPrice)) / 2;
        AddMoreProduct.addMoreProduct(productId, newBoughtPrice, sellingPrice, newQuantity, threshold);
        response.sendRedirect(request.getContextPath() + "/admin/manageProduct?storeId=" + storeId);


    }


}

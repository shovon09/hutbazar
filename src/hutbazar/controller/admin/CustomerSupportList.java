package hutbazar.controller.admin;

import hutbazar.model.admin.dao.GetCustomerSupportRequest;
import hutbazar.model.admin.domain.SupportEntity;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Shovon
 * Date: 11/13/12
 * Time: 10:11 AM
 * To change this template use File | Settings | File Templates.
 */
@WebServlet("/admin/customerSupportList")
public class CustomerSupportList extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String storeId = (String) request.getSession(true).getAttribute("userStoreId");
        List<SupportEntity> supportEntities = GetCustomerSupportRequest.getSupportList(storeId);
        request.setAttribute("supportList",supportEntities);
        request.getRequestDispatcher("/AdminView/CustomerSupportList.jsp").forward(request,response);
    }
}

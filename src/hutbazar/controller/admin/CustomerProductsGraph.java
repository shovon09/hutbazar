package hutbazar.controller.admin;

import hutbazar.model.admin.dao.ProductGraphService;
import hutbazar.model.admin.domain.ProductGraphEntity;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.data.general.DefaultPieDataset;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: shihab
 * Date: 11/13/12
 * Time: 1:53 PM
 * To change this template use File | Settings | File Templates.
 */
@WebServlet("/admin/customerProductsGraph")
public class CustomerProductsGraph extends HttpServlet {


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        HttpSession usersession = request.getSession(true);
        List<ProductGraphEntity> productGraph = ProductGraphService.getProducts((String) usersession.getAttribute("userStoreId"), request.getParameter("customerId"));
//        request.setAttribute("productGraph",productGraph);
//        request.getRequestDispatcher("/AdminView/CustomerWiseSalesReportDetails.jsp").forward(request,response);
        int i=0;
        OutputStream out = response.getOutputStream();

        DefaultPieDataset dataset = new DefaultPieDataset();
        for(i=0;i<productGraph.size();i++)
        {
            dataset.setValue(productGraph.get(i).getProductName(), Double.parseDouble(productGraph.get(i).getQuantity()));

        }


        JFreeChart chart = ChartFactory.createPieChart("Product Bought Ratio Chart", dataset, true, true, false);

        ChartUtilities.writeChartAsPNG(out, chart, 300, 300);/* Write the data to the output stream */

    }
}

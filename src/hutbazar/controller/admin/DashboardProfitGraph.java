package hutbazar.controller.admin;

import hutbazar.model.admin.dao.MonthlySalesReportService;
import hutbazar.model.admin.domain.MonthlySalesReportEntity;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Shovon
 * Date: 11/13/12
 * Time: 9:45 PM
 * To change this template use File | Settings | File Templates.
 */
@WebServlet("/admin/dashboardProfitGraph")
public class DashboardProfitGraph extends HttpServlet {


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        HttpSession usersession = request.getSession(true);
        List<MonthlySalesReportEntity> monthlySalesReport = MonthlySalesReportService.getMonthySalesReport((String) usersession.getAttribute("userStoreId"));

        OutputStream out = response.getOutputStream(); /* Get the output stream from the response object */
        response.setContentType("image/png"); /* Set the HTTP Response Type */
//        DefaultPieDataset dataset = new DefaultPieDataset();
        DefaultCategoryDataset dataSet = new DefaultCategoryDataset();

        for (MonthlySalesReportEntity m : monthlySalesReport) {

            dataSet.addValue(Double.parseDouble(m.getQuantity()), "Quantity", m.getMonth());
        }


//        JFreeChart chart = ChartFactory.createPieChart("PE Ratio ", dataset, true, true, false);
        JFreeChart chart = ChartFactory.createBarChart3D("Month Vs Product Sell ", "Month", "Items", dataSet, PlotOrientation.VERTICAL, true, true, false);

        ChartUtilities.writeChartAsPNG(out, chart, 300, 300);
    }
}

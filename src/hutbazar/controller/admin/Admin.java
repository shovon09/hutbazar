package hutbazar.controller.admin;

import hutbazar.model.admin.dao.GetStoreId;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * UserEntity: Shovon
 * Date: 10/25/12
 * Time: 7:25 PM
 * To change this template use File | Settings | File Templates.
 */
@WebServlet("/admin/dashboard")
public class Admin extends HttpServlet {


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("hello");
        String userId = (String) request.getSession(true).getAttribute("userId");
        String storeId = GetStoreId.getStoreId(userId);
        request.getSession(true).setAttribute("storeId", storeId);
        request.getRequestDispatcher("/AdminView/DashBoard.jsp").forward(request, response);
    }
}

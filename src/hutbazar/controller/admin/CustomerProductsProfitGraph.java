package hutbazar.controller.admin;

import hutbazar.model.admin.dao.CustomerSalesDetails;
import hutbazar.model.admin.dao.SalesReportSummary;
import hutbazar.model.admin.domain.CustomerSalesDetailsEntity;
import hutbazar.model.admin.domain.SalesReportSummaryEntity;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Shovon
 * Date: 11/14/12
 * Time: 12:15 AM
 * To change this template use File | Settings | File Templates.
 */
@WebServlet("/admin/customerProductsProfitGraph")
public class CustomerProductsProfitGraph extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        double customerProfit;
        double totalProfit = 0;
        HttpSession usersession = request.getSession(true);
        List<SalesReportSummaryEntity> salesReportSummary = SalesReportSummary.salesReport((String) usersession.getAttribute("userStoreId"));

        CustomerSalesDetailsEntity customerSell = CustomerSalesDetails.getCustomerSell((String) usersession.getAttribute("userStoreId"), request.getParameter("customerId"));
//        request.setAttribute("productGraph",productGraph);
//        request.getRequestDispatcher("/AdminView/CustomerWiseSalesReportDetails.jsp").forward(request,response);

        OutputStream out = response.getOutputStream(); /* Get the output stream from the response object */
        response.setContentType("image/png"); /* Set the HTTP Response Type */
//        DefaultPieDataset dataset = new DefaultPieDataset();
        DefaultCategoryDataset dataSet = new DefaultCategoryDataset();


        dataSet.addValue(Double.parseDouble(customerSell.getTotalProfit()), "Customer Profit", "Profit");
        dataSet.addValue(Double.parseDouble(salesReportSummary.get(0).getTotalProfit()), "Total Profit", "Profit");


//        JFreeChart chart = ChartFactory.createPieChart("PE Ratio ", dataset, true, true, false);
        JFreeChart chart = ChartFactory.createBarChart3D("Customer Profit Vs Total Profit", "Profit", "Taka", dataSet, PlotOrientation.VERTICAL, true, true, false);

        ChartUtilities.writeChartAsPNG(out, chart, 300, 300);


    }
}

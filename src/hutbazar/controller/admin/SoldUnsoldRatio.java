package hutbazar.controller.admin;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.data.general.DefaultPieDataset;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Created with IntelliJ IDEA.
 * User: Shovon
 * Date: 11/13/12
 * Time: 2:47 AM
 * To change this template use File | Settings | File Templates.
 */
@WebServlet("/admin/soldUnsoldRatio")
public class SoldUnsoldRatio extends HttpServlet {


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String totalProductSold = request.getParameter("totalProductSold");
        String totalProductUnsold = request.getParameter("totalProductUnsold");
        OutputStream out = response.getOutputStream(); /* Get the output stream from the response object */
        response.setContentType("image/png"); /* Set the HTTP Response Type */
        DefaultPieDataset dataSet = new DefaultPieDataset();
        dataSet.setValue("Total Sold Product", Double.parseDouble(totalProductSold));
        dataSet.setValue("Total Unsold Product", Double.parseDouble(totalProductUnsold));


        JFreeChart chart = ChartFactory.createPieChart("Sold/Unsold Ratio", dataSet, true, true, false);

//        JFreeChart chart = ChartFactory.createBarChart()

        ChartUtilities.writeChartAsPNG(out, chart, 300, 300);
    }
}

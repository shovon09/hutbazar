package hutbazar.controller.admin;

import hutbazar.model.admin.dao.ProductGraphService;
import hutbazar.model.admin.domain.ProductGraphEntity;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: shihab
 * Date: 11/13/12
 * Time: 12:46 PM
 * To change this template use File | Settings | File Templates.
 */
@javax.servlet.annotation.WebServlet("/admin/customerWiseSalesReportDetails")
public class CustomerWiseSalesReportDetails extends javax.servlet.http.HttpServlet {
    protected void doPost(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, java.io.IOException {

    }

    protected void doGet(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, java.io.IOException
    {
        HttpSession usersession = request.getSession(true);
        List<ProductGraphEntity> productGraph = ProductGraphService.getProducts((String) usersession.getAttribute("userStoreId"), request.getParameter("customerId"));
        request.setAttribute("customerId",request.getParameter("customerId"));
        request.setAttribute("productGraph",productGraph);


        request.getRequestDispatcher("/AdminView/CustomerWiseSalesReportDetails.jsp").forward(request,response);

    }
}

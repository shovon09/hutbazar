-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 13, 2012 at 08:01 PM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `hutbazar`
--

-- --------------------------------------------------------

--
-- Table structure for table `buy_records`
--

CREATE TABLE IF NOT EXISTS `buy_records` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `store_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `bought_quantity` int(11) NOT NULL,
  `bought_price` double NOT NULL,
  `bought_date` date NOT NULL,
  `product_name` varchar(255) NOT NULL,
  KEY `product_id` (`product_id`,`store_id`,`category_id`),
  KEY `store_id` (`store_id`),
  KEY `category_id` (`category_id`),
  KEY `product_name` (`product_name`),
  KEY `store_id_2` (`store_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11019 ;

--
-- Dumping data for table `buy_records`
--

INSERT INTO `buy_records` (`product_id`, `store_id`, `category_id`, `bought_quantity`, `bought_price`, `bought_date`, `product_name`) VALUES
(11000, 1, 1001, 110, 3000, '2012-10-01', 'Jamdani Shari'),
(11004, 1, 1001, 111, 2500, '2012-09-02', 'Kamij'),
(11005, 1, 1001, 110, 1000, '2012-10-01', 'Fatua'),
(11006, 1, 1000, 111, 120, '2012-11-01', 'T-Shirts'),
(11010, 1, 1000, 40, 4000, '2012-11-09', 'Jacket'),
(11013, 1, 1002, 40, 150, '2012-11-09', 'Sandle'),
(11017, 1, 1002, 40, 4000, '2012-11-09', 'Boot'),
(11018, 1, 1000, 30, 300, '2012-11-10', 'gamcha'),
(11000, 1, 1001, 110, 3000, '2012-10-01', 'Jamdani Shari'),
(11004, 1, 1001, 111, 2500, '2012-09-02', 'Kamij'),
(11005, 1, 1001, 110, 1000, '2012-10-01', 'Fatua'),
(11006, 1, 1000, 111, 120, '2012-11-01', 'T-Shirts'),
(11010, 1, 1000, 40, 4000, '2012-11-09', 'Jacket'),
(11013, 1, 1002, 40, 150, '2012-11-09', 'Sandle'),
(11017, 1, 1002, 40, 4000, '2012-11-09', 'Boot'),
(11018, 1, 1000, 30, 300, '2012-11-10', 'gamcha');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `store_id` int(11) NOT NULL,
  `parent_category_id` int(11) NOT NULL,
  PRIMARY KEY (`category_id`),
  KEY `store_id` (`store_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1014 ;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`category_id`, `category_name`, `image`, `store_id`, `parent_category_id`) VALUES
(1000, 'Men''s Wear', '', 1, 0),
(1001, 'Women''s Wear', '', 1, 0),
(1002, 'Shoes', '', 1, 0),
(1003, 'Jewellery', '', 1, 0),
(1004, 'Panjabi', './Images/panjabi.jpg', 1, 0),
(1013, 'Jacket', './Images/beige_dayDown.gif', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `customer_info`
--

CREATE TABLE IF NOT EXISTS `customer_info` (
  `customer_id` int(5) NOT NULL AUTO_INCREMENT,
  `customer_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `contact_number` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `division` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `total_credit` double NOT NULL,
  `reg_date` datetime NOT NULL,
  `admin_type` int(11) NOT NULL,
  PRIMARY KEY (`customer_id`),
  KEY `division` (`division`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1051 ;

--
-- Dumping data for table `customer_info`
--

INSERT INTO `customer_info` (`customer_id`, `customer_name`, `email`, `contact_number`, `address`, `division`, `password`, `total_credit`, `reg_date`, `admin_type`) VALUES
(1000, 'Arong', 'arong@arong.com', '1544535345345', 'Kollanpur,Dhaka', 'Dhaka', '1', 0, '2012-10-31 00:00:00', 1),
(1001, 'KayKraft', 'kaykraft@kaykraft.com', '01559809808', 'Dhanmondi,Dhaka', 'Dhaka', '2', 500000, '2012-10-01 00:00:00', 1),
(1002, 'karim', 'k@a.com', '443234', '1', 'Dhaka', '1', 0, '2012-10-29 00:00:00', 0),
(1020, 'Rahim Badhsha', 'rahim@yahoo.com', '4235425234', '5435345345', 'Dhaka', '2', 0, '2012-11-10 03:46:34', 0),
(1022, 'Modhu Rahman', 'modhu@yahoo.com', '0194325432432', 'Dhaka,DHaka', 'Dhaka', '1', 0, '2012-11-10 04:35:54', 0),
(1023, 'shihab', 'shihab@gmail.com', '1', '1', 'Dhaka', '1', 0, '2012-11-13 00:00:00', 0),
(1024, 'rahman', 'rahman@gmail.com', '1', '1', 'Dhaka', '1', 0, '2012-11-13 00:00:00', 0),
(1025, 'islam', 'islam@gmail.com', '1', '1', 'Dhaka', '1', 0, '2012-11-13 00:00:00', 0),
(1026, 'hasan', 'hasan@gmail.com', '1', '1', 'Dhaka', '1', 0, '2012-11-13 00:00:00', 0),
(1050, 'apex', 'apex@apex.com', 'adsad', 'Dhaka', 'Dhaka', '2', 0, '2012-11-10 04:34:18', 1);

-- --------------------------------------------------------

--
-- Table structure for table `delivery_areas`
--

CREATE TABLE IF NOT EXISTS `delivery_areas` (
  `division` varchar(255) NOT NULL,
  `charge` double NOT NULL,
  PRIMARY KEY (`division`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `delivery_areas`
--

INSERT INTO `delivery_areas` (`division`, `charge`) VALUES
('Dhaka', 50),
('Khulna', 150);

-- --------------------------------------------------------

--
-- Table structure for table `feedback`
--

CREATE TABLE IF NOT EXISTS `feedback` (
  `queryid` int(11) NOT NULL AUTO_INCREMENT,
  `customerquery` varchar(255) NOT NULL,
  `read` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`queryid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `feedback`
--

INSERT INTO `feedback` (`queryid`, `customerquery`, `read`, `name`, `email`) VALUES
(1, 'Your Website Sucks', 0, 'shihab', 'shihab.csedu09@gmail.com'),
(2, 'dasdada', 0, 'Rahman', 'shihab.csedu09@gmail.com'),
(3, 'a', 0, 's', 'a'),
(4, 'a', 0, 's', 'a'),
(5, 'd', 0, 'd', 'd'),
(6, 'd', 0, 'd', 'd'),
(7, 'd', 0, 'd', 'd'),
(8, '1', 0, 's', '1'),
(9, 'asd', 0, 'ddd', 'kajol'),
(10, '', 0, '', ''),
(11, '', 0, '', ''),
(12, '', 0, '', ''),
(13, '', 0, '', ''),
(14, 'dasd', 0, 'asd', 'shihab.csedu09@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `inventory`
--

CREATE TABLE IF NOT EXISTS `inventory` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_name` varchar(255) NOT NULL,
  `avg_bought_price` double NOT NULL,
  `store_id` int(11) NOT NULL,
  `stock` int(11) NOT NULL,
  `threshold` int(11) NOT NULL,
  `selling_price` double NOT NULL,
  `number_of_sell` int(11) NOT NULL,
  KEY `product_id` (`product_id`),
  KEY `product_id_2` (`product_id`),
  KEY `store_id` (`store_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11019 ;

--
-- Dumping data for table `inventory`
--

INSERT INTO `inventory` (`product_id`, `product_name`, `avg_bought_price`, `store_id`, `stock`, `threshold`, `selling_price`, `number_of_sell`) VALUES
(11000, 'Jamdani Shari', 3000, 1, 37, 15, 5995, 73),
(11004, 'Kamij', 2500, 1, 111, 10, 5000, 0),
(11005, 'Fatua', 1000, 1, 106, 10, 2500, 4),
(11006, 'T-Shirts', 120, 1, 53, 15, 250, 58),
(11010, 'Jacket', 4000, 1, 8, 10, 8000, 32),
(11013, 'Sandle', 150, 1, 39, 10, 400, 1),
(11017, 'BooT', 3500, 1, 25, 15, 5000, 25),
(11018, 'gamcha', 350, 1, 0, 20, 500, 50);

-- --------------------------------------------------------

--
-- Table structure for table `ordertable`
--

CREATE TABLE IF NOT EXISTS `ordertable` (
  `order_id` varchar(255) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `unit_price` double NOT NULL,
  `total_selling_price` double NOT NULL,
  `avg_bought_price` double NOT NULL,
  `profit` double NOT NULL,
  `delivery_address` varchar(255) NOT NULL,
  `shipment_charge` double NOT NULL,
  `division` varchar(255) NOT NULL,
  `order_date` date NOT NULL,
  `delivary_date` date NOT NULL,
  `order_status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ordertable`
--

INSERT INTO `ordertable` (`order_id`, `customer_id`, `category_id`, `store_id`, `product_id`, `quantity`, `unit_price`, `total_selling_price`, `avg_bought_price`, `profit`, `delivery_address`, `shipment_charge`, `division`, `order_date`, `delivary_date`, `order_status`) VALUES
('29ba91d8-c212-4d1a-bab4-dec385a25709', 1020, 1001, 1, 11005, 2, 2500, 5000, 1000, 3000, 'hhj', 50, 'Dhaka', '2012-11-10', '2012-11-12', 'delivered'),
('8f2e1df2-1382-4ea8-85f6-fadd1303b483', 1002, 1000, 1, 11010, 1, 7600, 7600, 4000, 3600, '1', 50, 'Dhaka', '2012-11-12', '2012-11-14', 'pending'),
('a8203595-8239-4bbb-8060-e8fc6fbe6662', 1002, 1002, 1, 11013, 1, 400, 400, 150, 250, '', 50, 'Dhaka', '2012-11-12', '2012-11-14', 'pending'),
('3ec65e90-bf38-4ac1-87e9-ac36b15e6fe0', 1002, 1000, 1, 11010, 1, 7600, 7600, 4000, 3600, '4234', 50, 'Dhaka', '2012-11-12', '2012-11-14', 'pending'),
('bbec3a3e-efc7-4b25-92ce-67aaf431ab14', 1002, 1001, 1, 11005, 1, 2500, 2500, 1000, 1500, '23', 50, 'Dhaka', '2012-11-12', '2012-11-14', 'pending'),
('60599483-ee2b-419e-93e0-0e29a0e7bc91', 1002, 1000, 1, 11006, 5, 250, 1250, 120, 650, '123', 50, 'Dhaka', '2012-11-12', '2012-11-14', 'pending'),
('2622660e-7d8d-447a-8fad-548abc1686a5', 1002, 1000, 1, 11006, 1, 250, 250, 120, 130, '', 50, 'Dhaka', '2012-11-12', '2012-11-14', 'pending'),
('22dc58a0-45e5-4cbb-9d1e-2782363b38c3', 1002, 1000, 1, 11006, 50, 250, 12500, 120, 6500, '4324', 50, 'Dhaka', '2012-11-13', '2012-11-15', 'pending'),
('22dc58a0-45e5-4cbb-9d1e-2782363b38c3', 1002, 1000, 1, 11010, 30, 7600, 228000, 4000, 108000, '4324', 50, 'Dhaka', '2012-11-13', '2012-11-15', 'pending'),
('22dc58a0-45e5-4cbb-9d1e-2782363b38c3', 1002, 1000, 1, 11017, 25, 5000, 125000, 3500, 37500, '4324', 50, 'Dhaka', '2012-11-13', '2012-11-15', 'pending'),
('22dc58a0-45e5-4cbb-9d1e-2782363b38c3', 1002, 1001, 1, 11000, 70, 5995, 419650, 3000, 209650, '4324', 50, 'Dhaka', '2012-11-13', '2012-11-15', 'pending'),
('22dc58a0-45e5-4cbb-9d1e-2782363b38c3', 1002, 1000, 1, 11018, 50, 450, 22500, 350, 5000, '4324', 50, 'Dhaka', '2012-11-13', '2012-11-15', 'pending'),
('a00c6719-e0d4-43f5-a629-163f3f0da850', 1002, 1001, 1, 11000, 2, 5995, 11990, 3000, 5990, '', 50, 'Dhaka', '2012-11-13', '2012-11-15', 'pending'),
('a00c6719-e0d4-43f5-a629-163f3f0da850', 1020, 1001, 1, 11000, 2, 5995, 11990, 3000, 5990, '', 50, 'Dhaka', '2012-10-13', '2012-10-15', 'pending'),
('a00c6719-e0d4-43f5-a629-163f3f0da850', 1020, 1001, 1, 11000, 2, 5995, 11990, 3000, 5990, '', 50, 'Dhaka', '2012-10-13', '2012-10-15', 'pending'),
('a00c6719-e0d4-43f5-a629-163f3f0da850', 1020, 1001, 1, 11000, 2, 5995, 11990, 3000, 5990, '', 50, 'Dhaka', '2012-10-13', '2012-10-15', 'pending'),
('a00c6719-e0d4-43f5-a629-16333f0da850', 1022, 1001, 1, 11000, 2, 5995, 11990, 3000, 5990, '', 50, 'Dhaka', '2012-10-13', '2012-10-15', 'pending'),
('a00c6719-e0d4-43f5-a629-16353f0da850', 1023, 1001, 1, 11000, 2, 5995, 11990, 3000, 5990, '', 50, 'Dhaka', '2012-10-13', '2012-10-15', 'pending'),
('a00c6719-e0d4-43f5-a629-163339f0da850', 1024, 1001, 1, 11000, 2, 5995, 11990, 3000, 5990, '', 50, 'Dhaka', '2012-10-13', '2012-10-15', 'pending'),
('a00c6719-e0d4-43f5-a629-163f360da850', 10025, 1001, 1, 11000, 2, 5995, 11990, 3000, 5990, '', 50, 'Dhaka', '2012-10-13', '2012-10-15', 'pending'),
('a00c6719-e0d4-43f5-a629-163f3f0da850', 1020, 1001, 1, 11000, 2, 5995, 11990, 3000, 5990, '', 50, 'Dhaka', '2012-09-13', '2012-09-15', 'pending'),
('a00c6719-e0d4-43f5-a629-16333f0da850', 1022, 1001, 1, 11000, 2, 5995, 11990, 3000, 5990, '', 50, 'Dhaka', '2012-09-13', '2012-09-15', 'pending'),
('a00c6719-e0d4-43f5-a629-16353f0da850', 1023, 1001, 1, 11000, 2, 5995, 11990, 3000, 5990, '', 50, 'Dhaka', '2012-09-13', '2012-09-15', 'pending'),
('a00c6719-e0d4-43f5-a629-163339f0da850', 1024, 1001, 1, 11000, 2, 5995, 11990, 3000, 5990, '', 50, 'Dhaka', '2012-09-13', '2012-09-15', 'pending'),
('a00c6719-e0d4-43f5-a629-163f360da850', 10025, 1001, 1, 11000, 2, 5995, 11990, 3000, 5990, '', 50, 'Dhaka', '2012-09-13', '2012-09-15', 'pending'),
('a00c6719-e0d4-43f5-a629-163f3f0da850', 1020, 1001, 1, 11000, 2, 5995, 11990, 3000, 5990, '', 50, 'Dhaka', '2012-08-13', '2012-08-15', 'pending'),
('a00c6719-e0d4-43f5-a629-16333f0da850', 1022, 1001, 1, 11000, 2, 5995, 11990, 3000, 5990, '', 50, 'Dhaka', '2012-08-13', '2012-08-15', 'pending'),
('a00c6719-e0d4-43f5-a629-16353f0da850', 1023, 1001, 1, 11000, 2, 5995, 11990, 3000, 5990, '', 50, 'Dhaka', '2012-08-13', '2012-08-15', 'pending'),
('a00c6719-e0d4-43f5-a629-163339f0da850', 1024, 1001, 1, 11000, 2, 5995, 11990, 3000, 5990, '', 50, 'Dhaka', '2012-08-13', '2012-08-15', 'pending'),
('a00c6719-e0d4-43f5-a629-163f360da850', 1025, 1001, 1, 11000, 2, 5995, 11990, 3000, 5990, '', 50, 'Dhaka', '2012-08-13', '2012-08-15', 'pending');

-- --------------------------------------------------------

--
-- Table structure for table `product_info`
--

CREATE TABLE IF NOT EXISTS `product_info` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_name` varchar(255) NOT NULL,
  `category_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL,
  `discount` double NOT NULL,
  `promotion_from` date DEFAULT NULL,
  `promotion_end` date DEFAULT NULL,
  `active` int(11) NOT NULL,
  PRIMARY KEY (`product_id`),
  KEY `category_id` (`category_id`),
  KEY `store_id` (`store_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11019 ;

--
-- Dumping data for table `product_info`
--

INSERT INTO `product_info` (`product_id`, `product_name`, `category_id`, `store_id`, `description`, `image`, `date_added`, `discount`, `promotion_from`, `promotion_end`, `active`) VALUES
(11000, 'Jamdani Shari', 1001, 1, 'Blue/Red/Yellow Jamdani Shari.\r\n    \r\n    \r\n    \r\n    \r\n    \r\n    \r\n    ', '', '2012-11-09 09:28:17', 0, '2012-10-28', '2012-10-30', 0),
(11004, 'Kamij', 1001, 1, 'Hdfasfas\r\n    ', '', '2012-11-09 09:03:47', 0, '2012-10-31', '2012-11-05', 0),
(11005, 'Fatua', 1001, 1, 'dasdsa', '', '2012-10-26 00:00:00', 0, NULL, NULL, 0),
(11006, 'T-Shirts', 1000, 1, 'blahblahblha....', '', '2012-10-26 00:00:00', 0, NULL, NULL, 0),
(11010, 'Jacket', 1000, 1, '              Blah', './Images/jacket2.jpg', '2012-11-09 06:34:27', 5, '2012-11-01', '2012-11-30', 1),
(11013, 'Sandle', 1002, 1, '              balal', './Images/flat_shoes_in_summer.jpg', '2012-11-09 07:09:53', 0, NULL, NULL, 0),
(11017, 'BooT', 1000, 1, '              dasdasd\r\n    ', './Images/nike-shoes-fl-2.jpg', '2012-11-10 02:19:43', 0, NULL, NULL, 0),
(11018, 'gamcha', 1000, 1, '    asdasd\r\n    \r\n    \r\n    ', './Images/beige_dayDown.gif', '2012-11-10 05:03:56', 10, '2012-11-01', '2012-11-30', 1);

-- --------------------------------------------------------

--
-- Table structure for table `store_info`
--

CREATE TABLE IF NOT EXISTS `store_info` (
  `store_id` int(11) NOT NULL AUTO_INCREMENT,
  `store_name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `admin_id` int(11) NOT NULL,
  `activation_form` datetime DEFAULT NULL,
  `activation_end` datetime DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `active` int(11) NOT NULL,
  PRIMARY KEY (`store_id`),
  KEY `admin_id` (`admin_id`),
  KEY `admin_id_2` (`admin_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `store_info`
--

INSERT INTO `store_info` (`store_id`, `store_name`, `description`, `type`, `admin_id`, `activation_form`, `activation_end`, `image`, `active`) VALUES
(1, 'Arong', 'Arong blah blah', 'Platinum', 1000, '2012-10-01 00:00:00', '2012-10-31 00:00:00', './Images/arong.jpg', 1),
(2, 'Kay Kraft', 'blah blah', 'Platinum', 1001, '2012-10-01 00:00:00', '2012-10-31 00:00:00', './Images/kaykraft.jpg', 1),
(5, 'Apex', 'Blah blah', 'Golden', 1050, NULL, NULL, './Images/nike-shoes-fl-2.jpg', 0);

-- --------------------------------------------------------

--
-- Table structure for table `support`
--

CREATE TABLE IF NOT EXISTS `support` (
  `store_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `comment` varchar(255) NOT NULL,
  `read` int(11) NOT NULL,
  `email` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `support`
--

INSERT INTO `support` (`store_id`, `category_id`, `product_id`, `comment`, `read`, `email`) VALUES
(1, 1001, 11005, 'Fatua Chire Gese', 0, 'shihab.csedu09@gmail.com'),
(1, 1000, 11006, 'T shirt o chire gese', 0, 'shihab.csedu09@gmail.com'),
(1, 1001, 11004, 'Not very Good', 0, 'shihab.csedu09@gmail.com'),
(1, 1002, 11013, 'falty', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `wishlist`
--

CREATE TABLE IF NOT EXISTS `wishlist` (
  `product_name` varchar(255) NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `useremail` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `storeid` int(11) NOT NULL,
  `read` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wishlist`
--

INSERT INTO `wishlist` (`product_name`, `username`, `useremail`, `description`, `storeid`, `read`) VALUES
('Punjabi', 'shihab', 'shihab.csedu09@gmail.com', 'punjabi chai', 1, 0),
('Juta', 'Shovon', 'shihab.csedu09@gmail.com', 'Juta nai ken?', 3, 0),
('Punjabi', 'asd', 'asd', 'asd', 2, 0),
('dddddddddddd', 'dd', 'ddasd', 'ffgghhjj', 2, 0),
('d', 'asd', 'shihab.csedu09@gmail.com', 'dasd', 1, 0),
('Shari', 'shovon', 'sh@yahoo.com', 'Jamdani Shari', 2, 0);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `category`
--
ALTER TABLE `category`
  ADD CONSTRAINT `category_ibfk_1` FOREIGN KEY (`store_id`) REFERENCES `store_info` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `store_info`
--
ALTER TABLE `store_info`
  ADD CONSTRAINT `store_info_ibfk_1` FOREIGN KEY (`admin_id`) REFERENCES `customer_info` (`customer_id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

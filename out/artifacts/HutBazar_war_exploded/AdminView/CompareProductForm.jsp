<%--
  Created by IntelliJ IDEA.
  User: Shovon
  Date: 11/12/12
  Time: 9:27 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title></title>
</head>
<body>
<div id="rightnow">
    <h3 class="reallynow">
        <span>Please Select Two Product</span>

        <br/>
    </h3>

    <form action="<c:url value="/admin/compareProduct"/>" method="post">

        Product One : <select name="firstProduct">
        <c:forEach var="product" items="${products}">
            <option value="${product.productId}">${product.productName}</option>

        </c:forEach>

    </select>
        Product Two: <select name="secondProduct">
        <c:forEach var="product" items="${products}">
            <option value="${product.productId}">${product.productName}</option>

        </c:forEach>

    </select>

        <strong></strong>
        <%--<input type="hidden" value="${p}"/>--%>

        <input type="submit" value="Compare Product"/>

    </form>

</div>
<c:choose>
    <c:when test="${firstProductSales !=null}">

        <h3>Product Compare</h3>
        <table>
            <thead>
            <tr>
                <td>Properties</td>
                <td>${firstProductSales.productName}</td>
                <td>${secondProductSales.productName}</td>

            </tr>
            </thead>
            <tbody>
            <tr>
                <td>Unit Price(taka)</td>
                <td>${firstProductSales.unitPrice}</td>
                <td>${secondProductSales.unitPrice}</td>
            </tr>
            <tr>
                <td>Stock</td>
                <td>${firstProductSales.stock}</td>
                <td>${secondProductSales.stock}</td>
            </tr>
            <tr>
                <td>Threshold</td>
                <td>${firstProductSales.threshold}</td>
                <td>${secondProductSales.threshold}</td>
            </tr>
            <tr>
                <td>Selling Price</td>
                <td>${firstProductSales.sellingPrice}</td>
                <td>${secondProductSales.sellingPrice}</td>
            </tr>
            <tr>
                <td>Total Number of Sell</td>
                <td>${firstProductSales.numberOfSell}</td>
                <td>${secondProductSales.numberOfSell}</td>
            </tr>
            <tr>
                <td>Profit Ratio</td>
                <td>${firstProductSales.profitRatio}</td>
                <td>${secondProductSales.profitRatio}</td>
            </tr>
            <tr>
                <td>PE Ratio Chart</td>
                <td><img
                        src="<c:url value="/admin/firstProductPEChart?firstProductName=${firstProductSales.productName}&firstProfitRatio=${firstProductSales.profitRatio}&secondProductName=${secondProductSales.productName}&secondProfitRatio=${secondProductSales.profitRatio}"/>"/>
                </td>

            </tr>
            </tbody>
        </table>

    </c:when>
    <c:otherwise>

    </c:otherwise>
</c:choose>
</body>
</html>
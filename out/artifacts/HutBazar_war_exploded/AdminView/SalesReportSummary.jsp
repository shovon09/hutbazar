<%--
  Created by IntelliJ IDEA.
  User: shihab
  Date: 11/9/12
  Time: 6:44 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Order Details</title>
    <link rel="shortcut icon"
          href="./Images/favicon.ico"/>
</head>
<body>

<h3>Sales Report Summary</h3>

<div id="infowrap">

    <div id="infobox">

        <table>

            <tbody>

            <c:forEach var="salesReportSummary" items="${salesReportSummary}">
                <tr class="cart_title">

                <tr>
                    <td>Total Products Sold</td>
                    <td>${salesReportSummary.totalProducts}</td>
                </tr>

                <tr>
                    <td>Total Orders Still Now</td>
                    <td>${salesReportSummary.totalOrders}</td>
                </tr>

                <tr>
                    <td>Total Sales</td>
                    <td>${salesReportSummary.totalSales} Taka</td>
                </tr>

                <tr>
                    <td>Total Investment</td>
                    <td>${salesReportSummary.totalInvestment} Taka</td>
                </tr>

                <tr>
                    <td>Total Profit</td>
                    <td>${salesReportSummary.totalProfit} Taka</td>
                </tr>


                <tr>
                    <td>Most Sold Product</td>
                    <td>${salesReportSummary.mostSoldProductName}</td>
                </tr>

                <tr>
                    <td>Quantity</td>
                    <td>${salesReportSummary.mostSoldProductQuantity} </td>
                </tr>

            </c:forEach>


            </tbody>


        </table>

        <table>

            <h3>Not Sold Products</h3>
            <tbody>

            <c:forEach var="notSoldProducts" items="${notSoldProducts}">
                <tr>
                    <td>${notSoldProducts.productName} </td>
                </tr>

            </c:forEach>

            </tbody>
        </table>


    </div>

</div>
<c:forEach var="salesReportSummary" items="${salesReportSummary}">
    <img src="<c:url value="/admin/overAllSalesRatio?totalSales=${salesReportSummary.totalSales}&totalInvestment=${salesReportSummary.totalInvestment}&totalProfit=${salesReportSummary.totalProfit}"/>"/>
    <img src="<c:url value="/admin/soldUnsoldRatio?totalProductSold=${salesReportSummary.totalProducts}&totalProductUnsold=${notSoldProducts.size()}"/>"/>

</c:forEach>


</div>


</body>
</html>
<%--
  Created by IntelliJ IDEA.
  User: shihab
  Date: 11/12/12
  Time: 11:16 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Details</title>
</head>
<body>
<%--
  Created by IntelliJ IDEA.
  User: shihab
  Date: 11/12/12
  Time: 9:02 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Product Wise Sales Report</title>
</head>
<body>

<h3>Orders</h3>
<table>
    <thead>



    </thead>
    <tbody>

        <c:forEach var="products" items="${products}">

            <tr class="cart_title">
                 <td>Product ID</td>
                 <td>${products.productId}</td>
            </tr>


            <tr class="cart_title">
                <td>Product Name</td>
                <td>${products.productName}</td>
            </tr>


            <tr class="cart_title">
                <td>Total Quantity Bought</td>
                <td>${products.quantity}</td>
            </tr>


            <tr class="cart_title">
                <td>Total Investment in this product</td>
                <td>${products.totalInvestment}</td>
            </tr>


            <tr class="cart_title">
                <td>Total Sales in this product</td>
                <td>${products.totalSales}</td>
            </tr>



            <tr class="cart_title">
                <td>Total Profit From this product</td>
                <td>${products.totalProfit}</td>
            </tr>


         </c:forEach>





    </tbody>
</table>

<img src="<c:url value="/admin/productProfitRatioGraph?productProfit=${productProfit}&totalProfit=${totalProfit}"/>">
<img src="<c:url value="/admin/productInvestmentRatioGraph?productInvestment=${productInvestment}&totalInvestment=${totalInvestment}"/>">

</body>
</html>

</body>
</html>
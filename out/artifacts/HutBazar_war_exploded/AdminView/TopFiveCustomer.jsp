<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="hutbazar.model.admin.*" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="hutbazar.model.admin.dao.*" %>
<%@ page import="hutbazar.model.admin.domain.*" %>
<%--
  Created by IntelliJ IDEA.
  UserEntity: Shovon
  Date: 10/19/12
  Time: 11:52 PM
  To change this template use File | Settings | File Templates.
--%>

<%
    HttpSession usersession = request.getSession(true);
    List<TopFiveCustomerEntity> products = TopFiveCustomer.getTopFiveCustomer ((String) usersession.getAttribute("userStoreId"));
    pageContext.setAttribute("products", products);
%>

<div id="infobox">
    <h3>Top Five Customer</h3>
    <table>
        <thead>
        <tr>

            <td>Customer Name</td>
            <td>Quantity Bought</td>


        </tr>
        </thead>
        <tbody>

        <c:forEach var="products" items="${products}">
            <tr class="cart_title">
                <td> <a href= "<c:url value="/admin/customerWiseSalesReportDetails?customerId=${products.customerId}" />"> ${products.customerName} </a>  </td>
                <td>${products.quantity}</td>

               <%-- <form name="customerWiseSalesReport" action="<c:url value="/admin/customerWiseSalesReport"/>" method="post">
                    <input type="hidden" name="customerIdToGetDetails" value="${products.product_id}">
                    <td><input type="submit"  value="Details"/> </td>


                </form>--%>
            </tr>

        </c:forEach>



        </tbody>
    </table>

</div>

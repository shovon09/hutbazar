<%--
  Created by IntelliJ IDEA.
  User: shihab
  Date: 11/9/12
  Time: 2:03 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>New Orders</title>
    <link rel="shortcut icon"
          href="./Images/favicon.ico" />
</head>
<body>

<div id="infowrap">

    <div id="infobox">
        <h3>Orders</h3>
        <table>
            <thead>
            <tr>
                <td>Order_number</td>
                <td>Total Product Bought</td>
                <td>Total Quantity</td>
                <td>Order Date</td>
                <td>Delivary Date</td>
                <td>Status</td>
                <td>Total Investment</td>
                <td>Total Sales</td>
                <td>Total Investment</td>
                <td>Details</td>
                <td>Confirm Delivary</td>
            </tr>
            </thead>
            <tbody>

            <c:forEach var="orderList" items="${orderList}">
                <tr class="cart_title">

                    <td>${orderList.order_number}</td>
                    <td>${orderList.totalNumberOfProducts}</td>
                    <td>${orderList.totalQuantity}</td>
                    <td>${orderList.order_date}</td>
                    <td>${orderList.delivary_date}</td>
                    <td>${orderList.order_status}</td>
                    <td>${orderList.totalInvestment}</td>
                    <td>${orderList.totalSales}</td>
                    <td>${orderList.totalProfit}</td>
                    <form name="orderDetails" action="<c:url value="/admin/orderDetails"/>" method="post">
                        <input type="hidden" name="orderIDToGetDetails" value="${orderList.order_id}">
                        <td><input type="submit" value="Details"/> </td>

                    </form>


                    <form name="confirmDelivary" action= "<c:url value="/admin/confirmDelivary"/>"  method="post">
                        <input type="hidden" name="orderIdToConfirm" value="${orderList.order_id}">
                        <td><input type="submit" value="Confirm Delivary"/> </td>

                    </form>
                </tr>

            </c:forEach>



            </tbody>
        </table>
    </div>



</div>

</body>
</html>
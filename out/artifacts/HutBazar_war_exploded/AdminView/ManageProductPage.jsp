<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%--
  Created by IntelliJ IDEA.
  User: Shovon
  Date: 11/9/12
  Time: 12:02 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
</head>
<body>
<div id="rightnow">
    <h3 class="reallynow">
        <span>Manage Products</span>
        <a href="<c:url value="/admin/addProductForm"/>" class="add">Add New Product</a>
        <br/>
    </h3>

    <p class="youhave">You have ${totalProduct} Products </p>
</div>

<h3>Product On Services</h3>
<table>
    <thead>
    <tr>
        <th>Product Id</th>
        <th>Name</th>
        <th>Bought Price</th>
        <th>Selling Price</th>
        <th>Stock</th>
        <th>Threshold</th>
        <th>Total sale</th>
        <th>Edit</th>
        <th>Add More</th>
        <th>Delete</th>


    </tr>
    </thead>
    <tbody>
    <c:forEach var="product" items="${productDetails}">
        <tr>
            <td> ${product.productId}</td>
            <td>${product.productName}</td>
            <td>${product.avgBoughtPrice}</td>
            <td>${product.sellingPrice}</td>
            <td>${product.stock}</td>

            <td>${product.threshold}</td>
            <td>${product.numberOfSell}</td>
            <td><a href="<c:url value="/admin/addProductForm?productId=${product.productId}"/>"> edit</a></td>
            <td><a href="<c:url value="/admin/addExistingProductForm?productId=${product.productId}"/>"> add more</a>
            <td><a href="<c:url value="/admin/deleteProduct?productId=${product.productId}"/>"> Delete</a>

            </td>


        </tr>
    </c:forEach>


    </tbody>
</table>


</body>
</html>
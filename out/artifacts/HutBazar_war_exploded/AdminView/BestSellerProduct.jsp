<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="hutbazar.model.admin.*" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="hutbazar.model.admin.dao.*" %>
<%@ page import="hutbazar.model.admin.domain.*" %>
<%--
  Created by IntelliJ IDEA.
  UserEntity: Shovon
  Date: 10/19/12
  Time: 11:52 PM
  To change this template use File | Settings | File Templates.
--%>

<%
    HttpSession usersession = request.getSession(true);
    List<ProductSalesListEntity> products = ProductListService.getTopFiveProductList((String) usersession.getAttribute("userStoreId"));
    pageContext.setAttribute("products", products);
%>

<div id="infobox">
    <h3>Top 5 Best Seller Product</h3>
    <table>
        <thead>
        <tr>
            <td>Product Name</td>
            <td>Quantity Sold</td>
        </tr>
        </thead>
        <tbody>

        <c:forEach var="products" items="${products}">
            <tr class="cart_title">
                <td><a href="<c:url value="/admin/productWiseSalesReportDetails?productId=${products.productId}"/>" > ${products.productName}</a> </td>

                <td>${products.quantity}</td>

            </tr>

        </c:forEach>



        </tbody>
    </table>

</div>

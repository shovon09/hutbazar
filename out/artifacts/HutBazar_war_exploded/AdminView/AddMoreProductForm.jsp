<%--
  Created by IntelliJ IDEA.
  User: Shovon
  Date: 11/9/12
  Time: 10:24 PM
  To change this template use File | Settings | File Templates.
--%>
<%--
  Created by IntelliJ IDEA.
  User: Shovon
  Date: 11/9/12
  Time: 2:33 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<html>
<head>
    <title></title>

    <link rel="stylesheet" type="text/css" media="all" href="<c:url value="/Styles/js/jsDatePick_ltr.min.css" />"/>
    <script type="text/javascript" src=" <c:url value="/Styles/js/jsDatePick.min.1.3.js" />"></script>
    <script type="text/javascript" src="http://jzaefferer.github.com/jquery-validation/jquery.validate.js"></script>

    <script type="text/javascript">
        window.onload = function () {
            new JsDatePick({
                useMode:2,
                target:"inputField",
                dateFormat:"%Y-%m-%d"

            });
        };
        onclick = function () {
            new JsDatePick({
                useMode:2,
                target:"inputFieldend",
                dateFormat:"%Y-%m-%d"

            });
        };

        $(document).ready(
                function () {
                    $("#productAddForm").validate({
                    });
                }
        );

    </script>
</head>
<body>

<form action="<c:url value="/admin/addMoreProduct"/>" method="post" name="productAddForm"
      id="productAddForm" onsubmit="return validateForm()">
    <pre>
              Product Name     :<input type="text" name="name" class="required" value="${product.productName} "
                                       readonly="readonly"/>
              Quantity         :<input type="text" name="quantity" class="required"/>

              Bought Price     :<input type="text" name="boughtPrice" class="required"/>

              Selling Price    :<input type="text" name="sellingPrice" class="required"/>

              Threshold        :<input type="text" name="threshold" class="required"/>

                      <input type="hidden" name="productId" class="required" value="${product.productId} "/>
                      <input type="hidden" name="avgBoughtPrice" class="required" value="${product.avgBoughtPrice}"/>
                      <input type="hidden" name="stock" class="required" value="${product.stock}"/>
                   <input type="hidden" name="storeId" class="required" value="${product.storeId}"/>



                    <input type="submit" class="required" value="Add Product To Inventory"/>


    </pre>

</form>

</body>
</html>
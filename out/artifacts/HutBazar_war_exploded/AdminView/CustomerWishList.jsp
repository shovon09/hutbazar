<%--
  Created by IntelliJ IDEA.
  User: Shovon
  Date: 11/13/12
  Time: 4:03 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title></title>
</head>
<body>
<div id="rightnow">
    <h3 class="reallynow">
        <span>Customer Wish List</span>
        <br/>
    </h3>

    <p class="youhave">You have total ${wishList.size()} Customer wish List </p>
</div>

<h3>Customer Wish List </h3>
<table>
    <thead>
    <tr>
        <th>Product Name</th>
        <th>User Name</th>
        <th>User Email</th>
        <th>Description</th>
        <th>Store Id</th>

    </tr>
    </thead>
    <tbody>
    <c:forEach var="wishList" items="${wishList}">
        <tr>
            <td>${wishList.productName}</td>
            <td>${wishList.userName}</td>
            <td>${wishList.userMail}</td>
            <td>${wishList.description}</td>
            <td>${wishList.storeId}</td>


        </tr>
    </c:forEach>


    </tbody>
</table>


</body>
</html>
<%--
  Created by IntelliJ IDEA.
  User: Shovon
  Date: 11/9/12
  Time: 2:33 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<html>
<head>
    <title></title>

    <link rel="stylesheet" type="text/css" media="all" href="<c:url value="/Styles/js/jsDatePick_ltr.min.css" />"/>
    <script type="text/javascript" src=" <c:url value="/Styles/js/jsDatePick.min.1.3.js" />"></script>
    <script type="text/javascript" src="http://jzaefferer.github.com/jquery-validation/jquery.validate.js"></script>

    <script type="text/javascript">
        window.onload = function () {
            new JsDatePick({
                useMode:2,
                target:"inputField",
                dateFormat:"%Y-%m-%d"

            });
        };
        onclick = function () {
            new JsDatePick({
                useMode:2,
                target:"inputFieldend",
                dateFormat:"%Y-%m-%d"

            });
        };

        $(document).ready(
                function () {
                    $("#productAddForm").validate({
                    });
                }
        );

    </script>
</head>
<body>

<form action="<c:url value="/admin/addProduct"/>" method="post" enctype="multipart/form-data" name="productAddForm"
      id="productAddForm" onsubmit="return validateForm()">
    <pre>
              Product Name     :<input type="text" name="name" class="required" value="${product.productName}"/>

              Category Name    :<select name="category" class="required">
        <c:forEach var="category" items="${category}">
            <option value="${category.categoryID}">${category.categoryName}</option>
        </c:forEach>

    </select>

              Description      :<textarea rows="4" cols="10" name="description">${product.description}
    </textarea>
             Image             :<input type="file" name="file" class="required"/>

              Discount         :<input type="text" name="discount" value="0"/> % (percent)

              Quantity         :<input type="text" name="quantity" class="required" value="${product.stock}"/>

              Bought Price     :<input type="text" name="boughtPrice" class="required"
                                       value="${product.avgBoughtPrice}"/>

              Selling Price    :<input type="text" name="sellingPrice" class="required"
                                       value="${product.sellingPrice}"/>

              Threshold        :<input type="text" name="threshold" class="required" value="${product.threshold}"/>

              Promotion Start  :<input type="date" size="12" id="inputField" name="dstart" class="required"
                                       value="${product.promotionFrom}"/>

              Promotion End    :<input type="date" size="12" id="inputFieldend" name="dend" class="required"
                                       value="${product.promotionEnd}"/>

              Promotion Active :  <input type="radio" name="active" class="required" checked="checked" value="0"/>Deactivate
                                  <input type="radio" name="active" class="required" value="1"/>Activate

                        <input type="hidden" value="${product.productId}" name="productId"/>
            <c:choose>
                <c:when test="${product.productId != null}">
                    <input type="submit" class="required" value="Edit Product"/>
                </c:when>
                <c:otherwise>
                    <input type="submit" class="required" value="Add Product"/>
                </c:otherwise>
            </c:choose>

    </pre>

</form>

</body>
</html>
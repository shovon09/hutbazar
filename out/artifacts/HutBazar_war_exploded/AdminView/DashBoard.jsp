<%--
  Created by IntelliJ IDEA.
  User: Shovon
  Date: 11/8/12
  Time: 2:36 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>DashBoard</title>
    <link rel="shortcut icon"
          href="./Images/favicon.ico"/>
</head>
<body>

<jsp:include page="RightNow.jsp"/>
<div id="infobox">
    <img src="<c:url value="/admin/dashboardProfitGraph"/>">
</div>
<jsp:include page="Last5Order.jsp"/>
<jsp:include page="BestSellerProduct.jsp"/>
<jsp:include page="TopFiveCustomer.jsp"/>
<jsp:include page="Threshold.jsp"/>

</body>
</html>
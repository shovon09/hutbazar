<%--
  Created by IntelliJ IDEA.
  User: shihab
  Date: 11/12/12
  Time: 9:02 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Product Wise Sales Report</title>
</head>
<body>

<h3>Orders</h3>
<table>
    <thead>
    <tr>
        <td>Product ID</td>
        <td>Product Name</td>
        <td>Total Quantity Bought</td>
        <td>Total Investment in this product</td>
        <td>Total Sales in this product</td>
        <td>Total Profit from this product</td>


    </tr>
    </thead>
    <tbody>



        <c:forEach var="products" items="${products}">
        <tr class="cart_title">

            <td><a href="<c:url value="/admin/productWiseSalesReportDetails?productId=${products.productId}"/>" > ${products.productId}</a> </td>
            <td>${products.productName}</td>
            <td>${products.quantity}</td>
            <td>${products.totalInvestment} Taka</td>
            <td>${products.totalSales} Taka</td>
            <td>${products.totalProfit} Taka</td>




        </tr>

        </c:forEach>

    </tbody>
</table>








</body>
</html>
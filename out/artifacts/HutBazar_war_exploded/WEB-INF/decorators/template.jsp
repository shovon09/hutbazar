<%--
  Created by IntelliJ IDEA.
  User: Shovon
  Date: 10/21/12
  Time: 10:07 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8"/>
    <%--Adding Page Title--%>
    <title><decorator:title/></title>
    <%--Finished Page Title--%>
    <%--Adding CSS--%>

    <link rel="stylesheet" href="Styles/css/style.css" type="text/css" media="all"/>
    <link rel="stylesheet" href="Styles/css/style2.css" type="text/css" media="all"/>
    <!--[if lte IE 6]>
    <link rel="stylesheet" href="Styles/css/ie6.css" type="text/css" media="all"/><![endif]-->

    <%--Finished CSS--%>

    <!-- Adding JS  -->
    <script src="Styles/js/jquery-1.4.1.min.js" type="text/javascript"></script>
    <script src="Styles/js/jquery.jcarousel.pack.js" type="text/javascript"></script>
    <script src="Styles/js/jquery-func.js" type="text/javascript"></script>
    <!-- End JS -->
    <%--Decorators head for adding Page Head--%>
    <decorator:head/>
    <%--Decorators head finished--%>
</head>
<body>

<!-- Shell -->
<div class="shell">

    <!-- Header -->
    <div id="header">
        <h1 id="logo"><a href="#">HutBazar</a></h1>

        <!-- Cart -->
        <%--<%@include file="/CustomerView/CartView.jsp" %>--%>
        <jsp:include page="/CustomerView/CartView.jsp"/>


        /  <!-- including the cart to the menu--!>
    <!-- End Cart -->

        <!-- Navigation -->
        <%--<%@include file="/CustomerView/NavigationBarView.jsp" %>--%>
        <jsp:include page="/CustomerView/NavigationBarView.jsp"/>

        <!-- End Navigation -->
    </div>
    <!-- End Header -->

    <!-- Main -->
    <div id="main">
        <div class="cl">&nbsp;</div>

        <!-- Sidebar -->
        <div id="sidebar">
            <!-- Search -->
            <%--<%@include file="/CustomerView/ProductSearchBox.jsp" %>--%>
            <jsp:include page="/CustomerView/ProductSearchBox.jsp"/>

            <!-- End Search -->
            <%--<%@include file="/CustomerView/Stores.jsp" %>--%>
            <jsp:include page="/CustomerView/Stores.jsp"/>


        </div>

        <decorator:body/>
        <!-- End Sidebar -->

        <div class="cl">&nbsp;</div>
    </div>
    <!-- End Main -->

    <!-- Side Full -->
    <div class="side-full">

        <!-- More Products -->
        <div class="more-products">

            <%--<%@include file="/CustomerView/SimpleAdView.jsp" %>--%>
            <jsp:include page="/CustomerView/SimpleAdView.jsp"/>


            <div class="more-nav">
                <a href="#" class="prev">previous</a>
                <a href="#" class="next">next</a>
            </div>
        </div>
        <!-- End More Products -->

        <!-- Text Cols -->
        <%--<%@include file="/CustomerView/TextCollumnView.jsp" %>--%>
        <jsp:include page="/CustomerView/TextCollumnView.jsp"/>

        <!-- End Text Cols -->

    </div>
    <!-- End Side Full -->

    <!-- Footer -->
    <%--<%@include file="/CustomerView/FooterView.jsp" %>--%>
    <jsp:include page="/CustomerView/FooterView.jsp"/>

    <!-- End Footer -->

</div>
<!-- End Shell -->


</body>
</html>
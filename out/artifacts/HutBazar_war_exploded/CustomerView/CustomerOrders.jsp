<%--
  Created by IntelliJ IDEA.
  User: shihab
  Date: 11/5/12
  Time: 11:05 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="x_rt" uri="http://java.sun.com/jstl/xml_rt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
</head>
<body>
<div class="center_content">
    <div class="left_content">
        <div class="title">My Orders</div>

        <div class="feat_prod_box_details">

            <table class="cart_table">
                <tr class="cart_title">
                    <td>Order Number</td>
                    <td>Total Product Bought</td>
                    <td>Total Quantity</td>
                    <td>Order Date</td>
                    <td>Delivary Date</td>
                    <td>Status</td>
                    <td>Details</td>
                </tr>

                <c:forEach var="orderList" items="${orderList}">
                    <tr class="cart_title">
                        <td>${orderList.order_number}</td>
                        <td>${orderList.totalNumberOfProducts}</td>
                        <td>${orderList.totalQuantity}</td>
                        <td>${orderList.order_date}</td>
                        <td>${orderList.delivary_date}</td>
                        <c:choose>
                            <c:when test="${orderList.delivered==null}">

                                <form name="orderCancel" action="orderCancel" method="post">
                                    <input type="hidden" name="orderIDToCancel" value="${orderList.order_id}">


                                    <td>Pending <input type="submit" value="Cancel"/></td>

                                </form>
                            </c:when>
                            <c:otherwise>
                                <td>Pending</td>
                            </c:otherwise>

                        </c:choose>

                        <form name="orderDetails" action="orderDetails" method="post">
                            <input type="hidden" name="orderIDToGetDetails" value="${orderList.order_id}">


                            <td><input type="submit" value="Details"/></td>

                        </form>
                    </tr>

                </c:forEach>

            </table>


        </div>


        <div class="clear"></div>
    </div>
    <!--end of left content-->
</div>
</body>
</html>
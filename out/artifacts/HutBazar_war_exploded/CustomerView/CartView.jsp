<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%--
  Created by IntelliJ IDEA.
  UserEntity: Shovon
  Date: 10/19/12
  Time: 3:34 PM
  To change this template use File | Settings | File Templates.
--%>
<%--<%--%>
<%--HttpSession userSession = request.getSession(true);--%>
<%--Cart cart = (Cart)userSession.getAttribute("cart");--%>
<%--List<CartEntry> cartEntries = cart.getCartEntries();--%>
<%--double sumTotal = 0.00;--%>
<%--for (CartEntry cartItem : cartEntries) {--%>
<%--sumTotal = sumTotal + cartItem.getTotal();--%>
<%--}--%>
<%--int totalProduct = cartEntries.size();--%>
<%--pageContext.setAttribute("totalProduct",totalProduct);--%>
<%--pageContext.setAttribute("sumTotal",sumTotal);--%>


<%--%>--%>
<div id="cart">

    <c:choose>
        <c:when test="${sessionScope.cartItem != null}">
            <a href="showCartDetails" class="cart-link">Your Shopping Cart</a>

            <div class="cl">&nbsp;</div>
            <span>Items: <strong>${sessionScope.cartItem}</strong></span>
            &nbsp;&nbsp;
            <span>Cost: <strong>${sessionScope.sumTotal} BDT</strong></span>
        </c:when>
        <c:otherwise>
            <a href="#" class="cart-link">Your Cart is empty!</a>

            <div class="cl">&nbsp;</div>
            <span>Item  : <strong> 0 </strong></span>
            &nbsp;&nbsp;
            <span>Cost :<strong>0.00</strong></span>

        </c:otherwise>
    </c:choose>
</div>
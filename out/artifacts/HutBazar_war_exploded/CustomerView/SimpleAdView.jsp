<%--
  Created by IntelliJ IDEA.
  User: Shovon
  Date: 10/20/12
  Time: 12:38 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ page import="hutbazar.model.customer.dao.CustomerService" %>
<%@ page import="hutbazar.model.customer.domain.ProductEntity" %>
<%@ page import="java.util.List" %>
<%
    List<ProductEntity> products = CustomerService.getAllProducts();
    pageContext.setAttribute("products", products);
%>

<div class="more-products-holder">
    <ul>
        <c:forEach var="product" items="${products}">

            <li><a href="productDetails?product_id=${product.productId}&category_id=${product.categoryId}"><img
                    src="${product.image}" alt="" width="100" height="100"/></a></li>

        </c:forEach>
        <%--<li><a href="#"><img src="Styles/css/images/small3.jpg" alt="" /></a></li>--%>
        <%--<li><a href="#"><img src="Styles/css/images/small4.jpg" alt="" /></a></li>--%>
        <%--<li><a href="#"><img src="Styles/css/images/small5.jpg" alt="" /></a></li>--%>
        <%--<li><a href="#"><img src="Styles/css/images/small6.jpg" alt="" /></a></li>--%>
        <%--<li><a href="#"><img src="Styles/css/images/small7.jpg" alt="" /></a></li>--%>
        <%--<li><a href="#"><img src="Styles/css/images/small1.jpg" alt="" /></a></li>--%>
        <%--<li><a href="#"><img src="Styles/css/images/small2.jpg" alt="" /></a></li>--%>
        <%--<li><a href="#"><img src="Styles/css/images/small3.jpg" alt="" /></a></li>--%>
        <%--<li><a href="#"><img src="Styles/Styles/css/images/small4.jpg" alt="" /></a></li>--%>
        <%--<li><a href="#"><img src="Styles/css/images/small5.jpg" alt="" /></a></li>--%>
        <%--<li><a href="#"><img src="Styles/css/images/small6.jpg" alt="" /></a></li>--%>
        <%--<li><a href="#"><img src="Styles/css/images/small7.jpg" alt="" /></a></li>--%>
        <%--<li><a href="#"><img src="Styles/css/images/small1.jpg" alt="" /></a></li>--%>
        <%--<li><a href="#"><img src="Styles/css/images/small2.jpg" alt="" /></a></li>--%>
        <%--<li><a href="#"><img src="Styles/css/images/small3.jpg" alt="" /></a></li>--%>
        <%--<li><a href="#"><img src="Styles/css/images/small4.jpg" alt="" /></a></li>--%>
        <%--<li><a href="#"><img src="Styles/css/images/small5.jpg" alt="" /></a></li>--%>
        <%--<li><a href="#"><img src="Styles/css/images/small6.jpg" alt="" /></a></li>--%>
        <%--<li class="last"><a href="#"><img src="Styles/css/images/small7.jpg" alt="" /></a></li>--%>
    </ul>
</div>
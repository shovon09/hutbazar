<%--
  Created by IntelliJ IDEA.
  User: shihab
  Date: 10/30/12
  Time: 1:04 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Feedback</title>
    <link rel="shortcut icon"
          href="./Images/favicon.ico"/>

    <script src="http://code.jquery.com/jquery-latest.js"></script>
    <script type="text/javascript" src="http://jzaefferer.github.com/jquery-validation/jquery.validate.js"></script>

    <script>
        $(document).ready(function () {
            $("#feedback").validate();
        });
    </script>
</head>
<body>


<c:forEach var="category" items="${categories}">

    <div class="new_prod_box">
        <a href="listProduct?catId=${category.categoryID}&store_id=${category. storeID}">${category.categoryName}</a>

        <div class="new_prod_bg">
            <span class="new_icon"><img src="" alt="" title=""/></span>
            <a href="listProduct?catId=${category.categoryID}&store_id=${category. storeID}"><img src="" alt="" title=""
                                                                                                  class="thumb"
                                                                                                  border="0"/></a>
        </div>
    </div>

</c:forEach>


<div id="content">

    <div class="feat_prod_box_details">

        <div class="contact_form">

            <div class="form_subtitle">

                <c:choose>
                    <c:when test="${success==null}">
                        Please Give Your Feedback
                    </c:when>
                    <c:otherwise>
                        Your Feedback Received.Another one?
                    </c:otherwise>
                </c:choose>


            </div>


            <form name="feedback" id="feedback" action="giveFeedback" method="post">


                <div class="form_row">
                    <label class="contact"><strong>Your Name:</strong></label>
                    <input type="text" size="48" name="userName" class="required"/>
                </div>


                <div class="form_row">
                    <label class="contact"><strong>Your Email:</strong></label>
                    <input type="text" size="48" name="userEmail" class="required email"/>
                </div>


                <div class="form_row">
                    <label class="contact"><strong>Your Feedback:</strong></label>
                    <textarea rows="10 " cols="47" name="userFeedback" class="required"></textarea>
                </div>


                <div class="form_row">
                    <input type="submit" class="register" value="Send"/>
                </div>

            </form>

        </div>

    </div>
</div>


</body>

</body>
</html>
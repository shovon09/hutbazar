<%--
  Created by IntelliJ IDEA.
  User: shihab
  Date: 11/6/12
  Time: 11:34 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="x_rt" uri="http://java.sun.com/jstl/xml_rt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>My Orders</title>
</head>
<body>
<div class="center_content">
    <div class="left_content">
        <div class="title">Product Information</div>

        <div class="feat_prod_box_details">

            <table class="cart_table">
                <tr class="cart_title">
                    <td>Product Name</td>
                    <td>Store Name</td>
                    <td>Quantity Bought</td>
                    <td>Unit Price</td>
                    <td>Total</td>
                </tr>

                <c:forEach var="orderDetails" items="${orderDetails}">
                <tr class="cart_title">
                    <td>${orderDetails.productName}</td>
                    <td>${orderDetails.storeName}</td>
                    <td>${orderDetails.productQuantity}</td>
                    <td>${orderDetails.productUnitPrice}</td>
                    <td>${orderDetails.total}</td>

                    </c:forEach>


            </table>


        </div>


        <div class="clear"></div>
    </div>
    <!--end of left content-->

    <div class="left_content">
        <div class="title">Other Information</div>

        <div class="feat_prod_box_details">

            <table class="cart_table">


                <tr class="cart_title">
                    <td>Total Amount</td>
                    <td>${total} Taka</td>
                </tr>


                <tr class="cart_title">
                    <td>Shipment Charge</td>
                    <td>${shipmentCharge} Taka</td>
                </tr>


                <tr class="cart_title">
                    <td>Total Paid Amount [with Shipment Charge]</td>
                    <td>${totalAfterShipment} Taka</td>
                </tr>


                <tr class="cart_title">
                    <td>Order Date</td>
                    <td>${orderDate}</td>
                </tr>


                <tr class="cart_title">
                    <td>Delivary Date</td>
                    <td>${delivaryDate}</td>
                </tr>


                <tr class="cart_title">
                    <td>Deliary Address</td>
                    <td>${delivaryAddress}</td>
                </tr>


                <tr class="cart_title">
                    <td>Order Status</td>
                    <td>${orderStatus}</td>
                </tr>


            </table>


        </div>


        <div class="clear"></div>
    </div>
    <!--end of left content-->
</div>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%--<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>--%>
<div id="navigation">
    <ul>
        <li><a href="<c:url value="/" />">Home</a></li>
        <li><a href="serviceAreas">Service Areas</a></li>
        <li><a href="wishlist">Wish List</a></li>

        <c:choose>
            <c:when test="${sessionScope.userEmail != null}">
                <li><a href="userAccount">My Account</a></li>
                <li><a href="userOrder">My Orders</a></li>
                <li><a href="logout">Sign out</a></li>

            </c:when>
            <c:otherwise>
                <li><a href="ownStore">Own a Store</a></li>
                <li><a href="register">Register</a></li>
                <li><a href="login">Sign in</a></li>

            </c:otherwise>
        </c:choose>
    </ul>
</div>

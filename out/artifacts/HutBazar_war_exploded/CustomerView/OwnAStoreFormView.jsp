<%--
  Created by IntelliJ IDEA.
  User: shihab
  Date: 10/29/12
  Time: 11:30 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Get A Store</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="shortcut icon"
          href="./Images/favicon.ico"/>
    <script src="http://code.jquery.com/jquery-latest.js"></script>
    <script type="text/javascript" src="http://jzaefferer.github.com/jquery-validation/jquery.validate.js"></script>


    <script>

        $(document).ready(
                function () {
                    $("#ownAStore").validate({
                    });
                }
        );


    </script>
</head>
<body>
<div id="content">
    <div class="feat_prod_box_details">

        <div class="contact_form">

            <div class="form_subtitle">Own A Store-Store Information</div>

            <form name="ownStore" id="ownAStore" action="ownAStore" enctype="multipart/form-data"
                  onsubmit="return validateForm()" method="post">


                <div class="form_row">
                    <label class="contact"><strong>Store Name:</strong></label>
                    <input type="text" name="storeName" size="48" class="required"/>
                </div>


                <div class="form_row">
                    <label class="contact"><strong>Description:</strong></label>
                    <textarea rows="10 " cols="47" class="required" name="storeDescription"></textarea>
                </div>


                <div class="form_row">
                    <label class="contact"><strong>Store Image:</strong></label>
                    <input type="file" name="file" class="required" id="field"/> <br/>
                </div>

                <div class="form_row">
                    <label class="contact"><strong>Store Type</strong></label>
                    <select name="storeType">
                        <option value="Silver">Silver[2000tk/month]</option>
                        <option value="Golden">Golden[5000tk/month]</option>
                        <option value="Platinum">Platinum[10000tk/motn]</option>
                    </select>
                </div>


                <br/>

                <h3>Admin Information</h3>

                <br/>

                <div class="form_row">
                    <label class="contact"><strong>Choose an Username:</strong></label>
                    <input type="text" name="username" size="48" class="required"/>
                </div>


                <div class="form_row">
                    <label class="contact"><strong>Choose A Password:</strong></label>
                    <input type="password" name="password" size="48" class="required password"/>
                </div>


                <div class="form_row">
                    <label class="contact"><strong>Email:</strong></label>
                    <input type="text" name="email" size="48" class="required email"/>
                </div>


                <div class="form_row">
                    <label class="contact"><strong>Contact No:</strong></label>
                    <input type="text" name="contactNumber" class="contact_input"/>
                </div>


                <div class="form_row">
                    <label class="contact"><strong>Address:</strong></label>
                    <input type="text" name="address" class="contact_input"/>
                </div>


                <div class="form_row">
                    <label class="contact"><strong>Division:</strong></label>
                    <select name="division">
                        <option value="Dhaka">Dhaka</option>
                        <option value="Chittagong">Chittagong</option>
                        <option value="Sylhet">Sylhet</option>
                        <option value="Rajshahi">Rajshahi</option>
                        <option value="Rangpur">Rangpur</option>
                        <option value="Khulna">Khulna</option>
                        <option value="Barishal">Barishal</option>
                    </select>
                </div>


                <div class="form_row">
                    <input type="submit" class="register" value="Own A Store"/>
                </div>

            </form>

        </div>
    </div>
</div>
</body>
</html>
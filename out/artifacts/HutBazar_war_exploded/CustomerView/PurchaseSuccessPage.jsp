<%--
  Created by IntelliJ IDEA.
  User: Shovon
  Date: 11/5/12
  Time: 7:25 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>Purchase Success</title>
    <script type="text/javascript">
        var win = null;
        function printIt(printThis) {
            win = window.open();
            self.focus();
            win.document.open();
            win.document.write('<' + 'html' + '><' + 'head' + '><' + 'style' + '>');
            win.document.write('body, td { font-family: Verdana; font-size: 10pt;}');
            win.document.write('<' + '/' + 'style' + '><' + '/' + 'head' + '><' + 'body' + '>');
            win.document.write(printThis);
            win.document.write('<' + '/' + 'body' + '><' + '/' + 'html' + '>');
            win.document.close();
            win.print();
            win.close();
        }
    </script>

</head>
<body>


<div id="printme">

    <p style="color:GREEN;font-size: 24">HutBazar.com</p>

    <p style="color:#003399;font-size: 18">Customer Name : ${sessionScope.userName} </p>

    <p style="color:#003399;font-size: 18">Customer email : ${sessionScope.userEmail} </p>

    <p style="color:#003399;font-size: 18"> --Product Details -- </p>

    <table class="cart_table">
        <tr class="cart_title">
            <td>Product Id</td>
            <td>Product Name</td>
            <td>Quantity</td>
            <td>Unit Price(BDT)</td>
            <td>Promotion</td>
            <td>Total(BDT)</td>


        </tr>
        <c:forEach var="in" items="${invoice}">

            <tr>
                <td>${in.productId}</td>
                <td>${in.productName}</td>
                <td>${in.quantity}</td>

                <td>${in.unitPrice}</td>
                <td>${in.promotion} %</td>
                <td>${in.total}</td>

            </tr>

        </c:forEach>
        <tr>
            <td colspan="4" class="cart_total"><span class="red">TOTAL: </span></td>
            <td> ${total} BDT</td>
        </tr>

    </table>

    <p style="color:#003399;font-size: 18">Delivery Address :${address} </p>

    <p style="color:#003399;font-size: 18">Purchase Date : ${orderDate}</p>

    <p style="color:#003399;font-size: 18">Delivery Date:${deliveryDate} </p>

    <p style="color:#003399;font-size: 18">Thank you for purchasing.Your product will be delivered within the
        delivery date.
    </p>


</div>
<a href="#" onclick="printIt(document.getElementById('printme').innerHTML); return false">
    Print Invoice
</a>
</body>
</html>
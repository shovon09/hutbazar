v<%--
  Created by IntelliJ IDEA.
  User: shihab
  Date: 10/30/12
  Time: 12:39 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Wishlist</title>
    <link rel="shortcut icon"
          href="./Images/favicon.ico"/>

    <script src="http://code.jquery.com/jquery-latest.js"></script>
    <script type="text/javascript" src="http://jzaefferer.github.com/jquery-validation/jquery.validate.js"></script>

    <script>
        $(document).ready(function () {
            $("#wishlist").validate();
        });


    </script>


</head>
<body>

<div id="content">

    <div class="feat_prod_box_details">

        <div class="contact_form">

            <div class="form_subtitle">

                <c:choose>
                    <c:when test="${success==null}">
                        Please write details of the products you want in future
                    </c:when>
                    <c:otherwise>
                        Your wish sent.Another one?
                    </c:otherwise>
                </c:choose>


            </div>


            <form name="wishlist" id="wishlist" action="wishlist" method="post">


                <div class="form_row">
                    <label class="contact"><strong>Your Name:</strong></label>
                    <input type="text" name="userName" size="48" class="required"/>
                </div>


                <div class="form_row">
                    <label class="contact"><strong>Your Email:</strong></label>
                    <input type="text" name="userEmail" size="48" class="required email"/>
                </div>


                <div class="form_row">
                    <label class="contact"><strong>Store Name:</strong></label>
                    <select name="storeId">

                        <c:forEach var="store" items="${stores}">

                            <option value=${store.store_id}> ${store.store_name}</option>

                        </c:forEach>

                    </select>


                </div>


                <div class="form_row">
                    <label class="contact"><strong>Product You want:</strong></label>
                    <input type="text" name="userProduct" size="48" class="required"/>
                </div>


                <div class="form_row">
                    <label class="contact"><strong>A litlle description:</strong></label>
                    <textarea rows="10 " cols="47" name="userProductDescription"></textarea>
                </div>


                <div class="form_row">
                    <input type="submit" class="register" value="Send"/>
                </div>

            </form>

        </div>

    </div>
</div>


</body>
</html>
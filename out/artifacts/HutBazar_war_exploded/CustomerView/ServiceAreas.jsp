<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  UserEntity: Shovon
  Date: 10/29/12
  Time: 10:51 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Available Service Areas</title>
</head>
<body>
<table id="ver-minimalist" summary="Most Favorite Movies">
    <thead>
    <tr>
        <th scope="col">Areas</th>
        <th scope="col">Charge</th>

    </tr>
    </thead>
    <tbody>
    <c:forEach var="serviceAreas" items="${serviceAreas}">
        <tr>
            <td>${serviceAreas.division}</td>
            <td>${serviceAreas.charge}</td>

        </tr>
    </c:forEach>
    </tbody>
</table>
</body>
</html>
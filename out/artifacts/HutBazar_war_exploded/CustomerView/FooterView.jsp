<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%--
  Created by IntelliJ IDEA.
  User: Shovon
  Date: 10/20/12
  Time: 12:53 AM
  To change this template use File | Settings | File Templates.
--%>
<div id="footer">
    <p class="left">
        <a href="<c:url value="/" />">Home</a>
        <span>|</span>

        <c:choose>

        <c:when test="${sessionScope.userEmail != null}">
        <a href="support">Support</a>
        <span>|</span>
        <a href="feedback">Feedback</a>

        <span>|</span>

        </c:when>

        <c:otherwise>

        <span>|</span>
        <a href="feedback">Feedback</a>

        </c:otherwise>
        </c:choose>

    <p class="right">
        &copy; 2012 Hut Bazar the largest online shopping mall.
        Developed by <a href="#" target="_blank" title="Hut Bazar">Rifatul Islam Shovon & Shihab
        Rahman(TEAM:VOID(CSE_DU))</a>
    </p>
</div>




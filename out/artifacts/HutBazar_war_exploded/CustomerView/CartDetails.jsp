<%--
  Created by IntelliJ IDEA.
  User: Shovon
  Date: 10/30/12
  Time: 12:04 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title></title>
</head>
<body>
<div class="left_content">
    <div class="title"><span class="title_icon"><img src="" alt="" title=""/></span>My cart</div>
    <div class="title"><span class="title_icon"><img src="" alt="" title=""/></span>
        <c:choose>
            <c:when test="${sessionScope.productIdWithInvalidQuantity != null}">
                <p> Purchase Failed !The following products are out of stock.Please edit your product quantity</p>
                <c:forEach var="productItems" items="${sessionScope.productIdWithInvalidQuantity}">
                    <c:out value="Product ID: ${productItems}"/>
                </c:forEach>

            </c:when>
            <c:otherwise>


            </c:otherwise>
        </c:choose>
    </div>


    <div class="feat_prod_box_details">

        <table class="cart_table">
            <tr class="cart_title">
                <td>Product Id</td>
                <td>Product Name</td>
                <td>Quantity</td>
                <td>Unit Price(BDT)</td>
                <td>Promotion</td>
                <td>Total(BDT)</td>
                <td>Delete Item</td>

            </tr>


            <c:forEach var="cart" items="${carts}">

                <tr>
                    <td>${cart.productId}</td>
                    <td>${cart.productName}</td>
                    <td>
                        <form action="editCart" method="post"><input type="text" size="3" name="quantity"
                                                                     value="${cart.quantity}"/>
                            <input type="hidden" name="productId" value="${cart.productId}"/></form>
                    </td>
                    <td>${cart.unitPrice}</td>
                    <td>${cart.promotion} %</td>
                    <td>${cart.total}</td>
                    <td><a href="deleteCart?productId=${cart.productId}"><img src="Styles/css/img/delete.png" alt=""
                                                                              title="delete item"/></a></td>

                </tr>

            </c:forEach>


            <tr>
                <td colspan="4" class="cart_total"><span class="red">TOTAL: </span></td>
                <td> ${sumTotal}BDT</td>
            </tr>

        </table>
        <a href="<c:url value="/" />" class="continue">&lt; Continue</a>
        <c:choose>
            <c:when test="${sessionScope.userEmail != null}">
                <a href="checkOutForm" class="checkout">Checkout &gt;</a>
            </c:when>
            <c:otherwise>
                <a href="login" class="checkout">Login &gt;</a>

            </c:otherwise>
        </c:choose>


    </div>


    <div class="clear"></div>
</div>
<!--end of left content-->
</body>
</html>
<%--
  Created by IntelliJ IDEA.
  User: Shovon
  Date: 10/19/12
  Time: 11:50 PM
  To change this template use File | Settings | File Templates.
--%>
<div class="box search">
    <h2>Search by <span></span></h2>

    <div class="box-content">
        <form action="searchProduct" method="post">

            <label>Keyword</label>
            <input type="text" class="field" name="keyWord"/>


            <div class="inline-field">

                <label>Price (From)</label>
                <input type="text" name="lowerBound" size="5"/>
                <label>to:</label>
                <input type="text" name="upperBound" size="5"/>

            </div>

            <input type="submit" class="search-submit" value="Search"/>

            <p>
                <a href="feedback" class="bul">Contact Customer Support</a>
            </p>

        </form>
    </div>
</div>
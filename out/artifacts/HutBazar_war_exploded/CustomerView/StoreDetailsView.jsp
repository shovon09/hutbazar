<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  UserEntity: Shovon
  Date: 10/23/12
  Time: 1:29 PM
  To change this template use File | Settings | File Templates.
--%>
<%--<%@ page import="hutbazar.model.customer>--%>
<%--<%@ page import="java.util.List" %>--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--<%  List<ProductCategoryEntity> productCategoryEntities = (List<ProductCategoryEntity>) request.getAttribute("categories");%>--%>
<html>
<head>
    <title>Store Details</title>
</head>
<body>

<div class="left_content">
    <div class="crumb_nav">
        <a href="<c:url value="/" />">home</a> &gt;&gt; categories
    </div>
    <div class="title"><span class="title_icon"></span>Product Category
    </div>

    <div class="new_products">


        <%--<jsp:useBean id="categories" scope="request" type="java.util.List"/>--%>
        <c:forEach var="category" items="${categories}">

            <div class="new_prod_box">
                <a href="listProduct?catId=${category.categoryID}&store_id=${category. storeID}">${category.categoryName}</a>

                <div class="new_prod_bg">
                    <span class="new_icon"><img src="" alt="" title=""/></span>
                    <a href="listProduct?catId=${category.categoryID}&store_id=${category. storeID}"><img
                            src="${category.image}" alt="" title="" class="thumb" border="0" height="100" width="120"/></a>
                </div>
            </div>

        </c:forEach>


    </div>


    <div class="clear"></div>
</div>
<!--end of left content-->

</body>
</html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Shovon
  Date: 10/22/12
  Time: 3:07 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Login</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="shortcut icon"
          href="./Images/favicon.ico"/>
    <script src="http://code.jquery.com/jquery-latest.js"></script>
    <script type="text/javascript" src="http://jzaefferer.github.com/jquery-validation/jquery.validate.js"></script>


    <script>

        $(document).ready(
                function () {
                    $("#login").validate({
                    });
                }
        );
    </script>
</head>
<body>
<div id="content">
    <div class="feat_prod_box_details">


        <div class="contact_form">
            <div class="form_subtitle">

                <c:choose>
                    <%-- <c:when test="${sessionScope.loginFailed != null}">
                        Login Failed.Please Enter your Email and Password Correctly
                    </c:when>--%>
                    <c:when test="${failed!=null}">
                        Login Failed.Please Enter your Email and Password Correctly
                    </c:when>

                    <c:when test="${success==null}">
                        Please Login To your Account
                    </c:when>

                    <c:otherwise>
                        Registration Successfull.Please Login To your Account
                    </c:otherwise>
                </c:choose>


            </div>

            <form name="register" id="login" action="logincheck" method="post">
                <div class="form_row">
                    <label class="contact"><strong>Email</strong></label>
                    <input type="text" name="Email" size="58" class="required email"/>

                </div>


                <div class="form_row">
                    <label class="contact"><strong>Password:</strong></label>
                    <input type="password" name="Password" size="58" class="required password"/>
                </div>


                <div class="form_row">
                    <input type="submit" class="register" value="login"/>
                </div>


            </form>

        </div>

    </div>
</div>
<%--<div id="sidebar">--%>
<%--<!-- Categories -->--%>
<%--<%@include file="/CustomerView/Stores.jsp" %>--%>
<%--<!-- End Categories -->--%>
<%--</div>--%>
</body>
</html>